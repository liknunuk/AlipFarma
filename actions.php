<?php
//print_r($_POST);
error_reporting(0);

$obj = $_GET['obj'];
if($obj == 'gaharo'){
  $usets = 'harga_beli, isiPerBox, harga_resep , harga_hv';
  $upkey = 'nomor';
  $udata = array($_POST['harga_beli'],$_POST['isiPerBox'],
            $_POST['harga_resep'],$_POST['harga_hv'],$_POST['nomor']);
  saveData('u','obat',$usets,$udata,$upkey);
}

if($_GET['mod'] =='i'){
  $isets = array(
    'karyawan'=>'nama,alamat,no_telp,ket',
    'dokter'=>'nama,alamat,no_telp,ket',
    'pasien'=>'id,nama,alamat,kota',
    'obat'=> 'kode,nama,indikasi,kategori,satuan,harga_beli,isiPerBox,harga_hv,harga_resep,stock',
    'optKategoriObat'=>'kategori',
    'optSatuanObat'=>'satuan',
    'pbf'=>'pbf_nomor,pbf_nama,pbf_telp'
  );

  $idata = array(
    'karyawan'=>array($_POST['nama'],$_POST['alamat'],$_POST['no_telp'],$_POST['ket']),
    'dokter'=>array($_POST['nama'],$_POST['alamat'],$_POST['no_telp'],$_POST['ket']),
    'pasien'=>array($_POST['id'],$_POST['nama'],$_POST['alamat'],$_POST['kota']),
    'obat'=>array($_POST['kode'],$_POST['nama'],$_POST['indikasi'],
            $_POST['kategori'],$_POST['satuan'],$_POST['harga_beli'],
            $_POST['isiPerBox'],$_POST['harga_hv'],$_POST['harga_resep'],
            $_POST['stock']),
    'optKategoriObat'=>array($_POST['kategori']),
    'optSatuanObat'=>array($_POST['satuan']),
    'pbf'=>array($_POST['pbf_nomor'],$_POST['pbf_nama'],$_POST['pbf_telp'])
  );

  saveData('i',$obj,$isets[$obj],$idata[$obj],null);

}else{
  $usets = array(
    'karyawan'=>'nama,alamat,no_telp,ket',
    'dokter'=>'nama,alamat,no_telp,ket',
    'pasien'=>'nama,alamat,kota',
    'obat'=> 'kode,nama,indikasi,kategori,satuan,harga_beli,isiPerBox,harga_hv,harga_resep,stock',
    'optKategoriObat'=>'kategori',
    'optSatuanObat'=>'satuan',
    'pbf' => 'pbf_nama, pbf_telp'
  );

  $upkey = array('karyawan'=>'kode','dokter'=>'kode','pasien'=>'id','obat'=>'nomor',
          'optKategoriObat'=>'nomor','optSatuanObat'=>'nomor','pbf'=>'pbf_nomor');

  $udata = array(
    'karyawan'=>array($_POST['nama'],$_POST['alamat'],$_POST['no_telp'],$_POST['ket'],$_POST['kode']),
    'dokter'=>array($_POST['nama'],$_POST['alamat'],$_POST['no_telp'],$_POST['ket'],$_POST['kode']),
    'pasien'=>array($_POST['nama'],$_POST['alamat'],$_POST['kota'],$_POST['id']),
    'obat'=>array($_POST['kode'],$_POST['nama'],$_POST['indikasi'],$_POST['kategori'],
            $_POST['satuan'],$_POST['harga_beli'],$_POST['isiPerBox'],
            $_POST['harga_hv'],$_POST['harga_resep'],$_POST['stock'],$_POST['nomor']),
    'optKategoriObat'=>array($_POST['kategori'],$_POST['nomor']),
    'optSatuanObat'=>array($_POST['satuan'],$_POST['nomor']),
    'pbf'=>array($_POST['pbf_nama'],$_POST['pbf_telp'],$_POST['pbf_nomor'])
  );

  saveData('u',$obj,$usets[$obj],$udata[$obj],$upkey[$obj]);
}

function saveData($mod,$obj,$sets,$data,$tkey){
  require_once('lib/class.crud.inc.php');
  $act = new dbcrud();
  if($mod=='i'){
    $act->insert($obj,$sets,$data);
  }else{
    $act->update($obj,$sets,$data,$tkey);
  }
  switch($obj){
    case 'optSatuanObat'  : $obj = 'unito'; break;
    case 'optKategoriObat': $obj = 'katob'; break;
  }
  header("Location:./?show=data&obj=".$obj."&mod=i&nh=1");
}

?>
