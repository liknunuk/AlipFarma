<div class='page-banner'>
  <p>REKAPITULASI PENJUALAN NON RESEP</p>
</div>
<div>
  <?php $jam=date('H');$shift = $jam < 14 ? 'Pagi':'Sore'; ?>
  <a href="exportXls.php?obj=nonresep&shift=<?php echo $shift;?>">Export excel</a>
</div>
<?php
  require_once("./lib/class.crud.inc.php");
  $recap = new dbcrud();
 ?>
 <div class="table-responsive">
   <table class="table table-small">
     <thead>
       <tr>
         <th>No Trx.</th>
         <th>Nama Barang</th>
         <th>Jumlah</th>
         <th>Harga Satuan</th>
         <th>Disk</th>
         <th>Jumlah Harga</th>
         <th>Total Harga</th>
       </tr>
     </thead>
     <tbody>
       <?php
        $hari = date('Y-m-d');
        $cols = "trxNonResep.trxId, obat.nama, trxNonResep.kuantitas qty,
                 trxNonResep.harga_beli harga, trxNonResep.diskon disk";
        $tbls = "trxNonResep, obat";
        $fltr = "trxNonResep.trxDate='".$hari."' && trxShift='".$shift."' &&
                 obat.kode = trxNonResep.kodeObat";
        $ordr = "trxNonResep.trxId, obat.nama";

        $sql = "SELECT $cols FROM $tbls WHERE $fltr ORDER BY $ordr";
        //echo $sql;

        $qry = $recap->transact($sql);
        $grandTotal = 0;
        $i = 0;
        while($r = $qry->fetch()){
          $jmHarga = ($r['qty'] * $r['harga']) * ( 100 - $r['disk'])/100;
          $trx[$i] = $r['trxId'];
          if($i == 0){
            $trxId = sprintf("%06d",$r['trxId']);
            $totalHarga= totalTrx($recap,$r['trxId']);
          }else{
            $j = $i-1;
            if($r['trxId'] == $trx[$j]){
              $trxId = '';
              $totalHarga=0;
            }else{
              $trxId = sprintf("%06d",$r['trxId']);
              $totalHarga= totalTrx($recap,$r['trxId']);
            }
          }
          echo "
          <tr>
            <td>".$trxId."</td>
            <td>".$r['nama']."</td>
            <td>".$r['qty']."</td>
            <td class='ra'>".number_format($r['harga'],0,',','.')."</td>
            <td class='ra'>".$r['disk']."%</td>
            <td class='ra'>".number_format($jmHarga,0,',','.')."</td>";
          if($totalHarga!=0){
            echo "
            <td class='ra'>".number_format($totalHarga,0,',','.')."</td>";
          }else{
            echo "<td class='ra'>&nbsp;</td>";
          }
          echo "
          </tr>
          ";
          $i++;
          $grandTotal+=$totalHarga;
        }
        ?>
        <tr>
          <td colspan='6' class='ra'>Grand Total</td>
          <td class='ra'><?php echo number_format($grandTotal,0,',','.'); ?></td>
        </tr>
     </tbody>
   </table>
 </div>
<?php
  function totalTrx($obj,$trxId){
    $sql = "SELECT SUM((100-diskon)/100 * kuantitas * harga_beli) totalTrx
            FROM trxNonResep WHERE trxId='".$trxId."'";
    $qry = $obj->transact($sql);
    $r = $qry->fetch();
    return($r['totalTrx']);
  }
 ?>
