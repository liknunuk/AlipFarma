<div class='page-banner'>
  <p>FORM PEMBELIAN OBAT</p>
</div>

<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>

<div class="row" id="fakturData">

    <div class="col-sm-2">
      <label>Nama PBF</label>
      <!--input class='form-control' type='text' id='namaPBF' list="pbfList"
      onKeyPress=geserTo(event,'tglFaktur') />
      <datalist id="pbfList" style="background: #000;"></datalist-->
      <select id="namaPBF" class="form-control" onchange=cekpbf(this.value)></select>
    </div>

    <div class="col-sm-2">
      <label>Nomor Faktur</label>
      <input class='form-control' type='text' id='nomorFaktur' onKeyPress=geserTo(event,'namaPBF') />
    </div>

    <div class="col-sm-2">
      <label>Tanggal Faktur</label>
      <input class='form-control comoTanggal' type='text' id='tglFaktur'
      onKeyPress=geserTo(event,'tglOverDue') />
    </div>
    <div class="col-sm-2">
      <label>Jatuh Tempo</label>
      <input class='form-control comoTanggal' type='text' id='tglOverDue'
      onKeyPress=geserTo(event,'fakturDiscount') />
    </div>
    <div class="col-sm-2">
      <label>Total Potongan</label>
      <input class='form-control' type='number' id='fakturDiscount'
      onKeyPress=geserTo(event,'fkbData-save') />
    </div>
    <div class="col-sm-2">
      <br />
      <button class="btn btn-primary" id="fkbData-save">Simpan</button>
    </div>
</div>
<div class="row" id="fakturInfo" style="visibility:hidden;">
  <div class="col-lg-12">

    <table>
      <tbody>
        <tr>
          <td width='250'>Nomor Faktur</td>
          <td id="fi_nofak"></td>
        </tr>
        <tr>
          <td>Nama PBF</td>
          <td id="fi_nmPBF"></td>
        </tr>
        <tr>
          <td>Tanggal Faktur</td>
          <td id="fi_tgfak"></td>
        </tr>
        <tr>
          <td>Tanggal Jatuh Tempo</td>
          <td id="fi_tgdue"></td>
        </tr>
      </tbody>
    </table>

  </div>
</div>

<div class="row fabelList">
  <div class="col-sm-3">
    <label>Kode Obat</label>
    <input class='form-control' type='text' id='kdObat' onkeyup=pilobat() placeholder="Tulis Nama Obat "/>
    <div class='kode-float' id="pilObat" style="display:none;"></div>
  </div>
  <div class="col-sm-3">
    <label>Banyaknya (box)</label>
    <input class='form-control' type='number' id='qty' />
  </div>
  <div class="col-sm-3">
    <label>Isi / box</label>
    <input class='form-control' type='number' id='isibox' />
  </div>
  <div class="col-sm-3">
    <label>Satuan Jual</label>
    <select class="form-control" id="satjual">
      <option>Botol/flez</option>
      <option>Lempeng</option>
      <option>Tablet</option>
      <option>Tube</option>
    </select>
  </div>
</div>
<div class="row fabelList">
  <div class="col-sm-3">
    <label>Harga / Box</label>
    <input class='form-control' type='number' id='hargaBeli' />
  </div>
  <div class="col-sm-3">
    <label>PPn 10%</label>
    <select id="ppnBeli" class='form-control'>
      <option value='1'>Harga Termasuk PPn</option>
      <option value='0'>Harga Belum Termasuk PPn</option>
    </select>
  </div>
  <div class="col-sm-3">
    <label>Diskon (%)</label>
    <input class='form-control' type='number' id='discObat' />
  </div>
  <div class="col-sm-3">
    <label><i>Periksa Data</i></label><br />
    <button class="btn btn-primary" id="fkbList-save">Simpan</button>
    <button class="btn btn-success" id="fkbList-fres">Refresh</button>
  </div>
  <input type="hidden" id="hargaEcer" />
</div>
<div class="row">
  <div style="height:50px;">&nbsp;</div>
  <div id="fakturRefresh"></div>
  <div id="dataFaktur">

  </div>
</div>
<!-- modals -->
<div id="pbfModalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">DATA PBF</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          
          <div class="form-group">
            <label for="pbf_nama" class="col-sm-3">Nama PBF</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="fkb_pbfNama">
            </div>
          </div>
          
          <div class="form-group">
            <label for="pbf_telp" class="col-sm-3">Nomor Telefon</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="fkb_pbfTelp">
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script>

  $(document).ready( function(){
    $('#nomorFaktur').focus();
    $('.fabelList').hide();

    $.ajax({
      url     : "ajax/dataPBF.php",
      success : function(dpbf){
        $("#namaPBF").html(dpbf)
      }
    });

    $("#fkb_pbfTelp").keypress( function(e){

      if( e.which == 13 ){
        e.preventDefault();
        $.post("actions.php?mod=i&obj=pbf",{
          pbf_nomor : '',
          pbf_nama  : $("#fkb_pbfNama").val(),
          pbf_telp  : $("#fkb_pbfTelp").val()
        },function(response){
          location.reload();
        });
      }
    });

  });

  function pilobat(){
    var ko = $("#kdObat").val();

    if(ko.length < 3 ){
      $("#pilObat").hide();
    }else {
      $("#pilObat").show();
      $.ajax({
        url     :"./ajax/obatByKode.php?do="+ko,
        success : function(obats){
          $("#pilObat").html(obats);
        }
      })
    }
  }

  function cekpbf(nm){
    if( nm == "0" || nm == 0 ){
      $("#pbfModalForm").modal('show');
    }
  }
</script>
