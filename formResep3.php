<div class='page-banner'>
  <p>PEMBAYARAN RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class="row">
  <div class="col-lg-4">
    <h3>Antrian Resep</h3>
    <ul class="list-group" id="resepTerproses"></ul>  
  </div>
  <div class="col-lg-8">
  <h3>Uraian Resep #<span id="trxId"></span> </h3>
  <table class='font-small' width="100%" border="0" cellspacing="0">
      <thead>
        <tr>
          <th>No.</th>
          <th>Kode & Nama Obat</th>
          <th>Bny</th>
          <th>Harga</th>
          <th>Diskon</th>
          <th width="92px">Jml Harga</th>
        </tr>
      </thead>
      <tbody id="dataResep">
        
      </tbody>
    </table>
    <br />
    <!-- form tagihan -->
    <div>
      <div class="form-horizontal">

        <div class="form-group">
          <label for="jmTagihan" class="col-sm-3">Jumlah Tagihan</label>
          <div class="col-sm-9">
            <input type="number" id="jmTagihan" class="form-control">
          </div>
        </div>

        <div class="form-group">
          <label for="diskon" class="col-sm-3">Potongan Harga</label>
          <div class="col-sm-9">
            <input type="number" id="diskon" class="form-control">
          </div>
        </div>

        <div class="form-group">
          <label for="dibayar" class="col-sm-3">Dibayar</label>
          <div class="col-sm-9">
            <input type="number" id="dibayar" class="form-control" onBlur=setJujul()>
          </div>
        </div>

        <div class="form-group">
          <label for="kembali" class="col-sm-3">Kembali</label>
          <div class="col-sm-9">
            <input type="number" id="kembali" class="form-control">
          </div>
        </div>

      </div>
    </div>
    <!-- form tagihan -->
    <!-- info pembayaran -->
    <div id="infoBayar"></div>
    <!-- info pembayaran -->
  </div>
</div>


<script>
$(document).ready(function(){
  loadRecipes();

  $("#kembali").keypress( function(e){
    //buka kuitansi php, bawa nomor trx dan jumlah dibayar
    let trxId,dibayar,diskon;
    trxId = $("#trxId").html();
    bayar = $("#dibayar").val();
    diskon= $("#diskon").val();
    window.open("kuitansi.php?id="+trxId+"&jm="+bayar,'kuitansi',"width=1000,height=400px");
    //ubah status recipeRqst jadi Lunas
    $.post("ajax/bayarResep.php",{
      diskon: diskon,
      trxId :trxId
    },function(response){
      $("#infoBayar").html(response);
      loadRecipes();
    })

  });
});

function loadRecipes(){
  // load resep terproses
  $.getJSON("ajax/recipeOutData.php?sr=Terproses", function(recipes){
    $("#resepTerproses li").remove();
    $.each(recipes, function(i,data){
      tgl = data.tanggal.split('-');
      $("#resepTerproses").append(
        "<li class='list-group-item'>"+
        "<p><a href='javascript:void(0)' onClick=showRecipe(this.innerHTML)>"+data.trxId+"</a> - "+
        tgl[2]+"/"+tgl[1]+"/"+tgl[0]+
        "<span class='badge' style='float:right;'>"+data.item+" item obat</span></p>"+
        "<p>"+data.nama+" : <span style='float:right;'>"+data.dokter+"</span></p>"+
        "</li>"
      );
    })
  });
}

function showRecipe(id){
  $("#trxId").html(id);
  $.getJSON("ajax/recipeOutItems.php?id="+id, function(recipes){
    // copy from formresep.php
    $("#dataResep tr").remove();
      let sn = 1;
      let subtotal = 0;
      $.each(recipes.obat, function(i,data){
        let jmHarga = parseFloat(data.banyaknya) * parseFloat(data.harga_resep) * ( 100 - parseInt(data.diskon)) / 100;
        //alert( data.harga_resep +"*"+data.banyaknya+"="+jmHarga );
        $("#dataResep").append(
          "<tr>"+
          "<td>"+sn+"</td>"+
          "<td>["+data.kodeObat+"] "+data.namaObat+"</td>"+
          "<td align='right'>"+data.banyaknya+"</td>"+
          "<td align='right'>"+data.harga_resep.toLocaleString("id-ID",{ minimumFractionDigits: 2 })+"</td>"+
          "<td align='right'>"+data.diskon+"%</td>"+
          "<td align='right'>"+jmHarga.toLocaleString("id-ID",{ minimumFractionDigits: 2 })+"</td>"+
          "</tr>"
        );
        sn+=1;
        subtotal += jmHarga;
        $("#diskon").focus();
      });
      let persen = parseInt(recipes.harga.faktor_p) / 100 * subtotal;
      //let grandtotal = subtotal + parseInt(recipes.harga.faktor_r) + parseInt(recipes.harga.faktor_e) + persen;
      let grandTotal = subtotal + persen + parseInt(recipes.harga.faktor_r) + parseInt(recipes.harga.faktor_e) + parseInt(recipes.harga.konsultasi);
      $("#dataResep").append(
        "<tr>"+
        "  <td align='right' colspan='5'>Sub Total</td>"+
        "  <td align='right' id='subtotal'>"+subtotal.toLocaleString("id-ID",{minimumFractionDigits: 2})+"</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>R</td>"+
        "  <td align='right'>"+parseInt(recipes.harga.faktor_r).toLocaleString("id-ID",{minimumFractionDigits: 2})+"</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>E</td>"+
        "  <td align='right'>"+parseInt(recipes.harga.faktor_e).toLocaleString("id-ID",{minimumFractionDigits: 2})+"</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>%</td>"+
        "  <td align='right'>"+parseFloat(persen).toLocaleString("id-ID",{maximumFractionDigits: 2})+"</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>Konsultasi</td>"+
        "  <td align='right'>"+parseInt(recipes.harga.konsultasi).toLocaleString("id-ID",{minimumFractionDigits: 2})+"</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>Grand Total</td>"+
        "  <td align='right' id='grandtotal'>"+grandTotal.toLocaleString("id-ID",{maximumFractionDigits: 2})+"</td>"+
        "</tr>"
      );
    // copy from formresep.php
      let jmBill = round100(grandTotal);
      $("#jmTagihan").val(jmBill);
  })
}

function setJujul(){
  let jt,ph,by,jj;
  jt = parseInt($("#jmTagihan").val());
  ph = parseInt($("#diskon").val());
  by = parseInt($("#dibayar").val());
  jj = by - (jt - ph);
  $("#kembali").val( jj );
}
</script>