<div class='page-banner'>
  <p>PENJUALAN NON RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class='form-inline'>
  <div class="form-group">
    <label style="width:125px;">Nomor Transaksi</label>
    <input class='form-control' style='width:175px;' id='trxId' type='text' readonly />
    <script>
    $.ajax({
      url : './ajax/nmTrxNRsep.php',
      success : function (trxId){
        $("#trxId").val(trxId);
      }
    });
    </script>
  </div>
  <div class="form-group">
    <label style="width:125px;">Tanggal Transaksi</label>
    <input class='form-control' style='width:175px;' id='trxDate' type='date' readonly value='<?php echo date('Y-m-d'); ?>' />
  </div>
  <div class="form-group">
    <label style="width:125px;">Shift</label>
    <select class='form-control' style='width:175px;' id='trxShift' type='text'>
        <option value='Pagi'>Pagi</option>
        <option value='Sore'>Sore</option>
    </select>
  </div>
  <script>
  var dina = new Date();
  var jame = dina.getHours();
  var jam = parseInt(jame);
  if( jam >= 7 && jam < 14 ){
    $("#trxShift").val('Pagi')
  }else if( jam >= 14 && jam < 21){
    $("#trxShift").val('Sore');
  }
  </script>
  <div class="form-group">
    <label style="width:125px;">Jenis Transaksi</label>
    <select class='form-control' style='width:175px;' id='trxtipe' type='text'>
      <option value='non_resep'> Non Resep </option>
      <option value='dokter'> Dokter </option>
      <option value='karyawan'> Karyawan </option>
    </select>
    <script>
      $.ajax({
        url     : './ajax/dataharga.php',
        success : function(regas){
          rega = JSON.parse(regas);
          localStorage.setItem('nrs',rega[0].non_resep);
          localStorage.setItem('dkt',rega[0].dokter);
          localStorage.setItem('kyw',rega[0].karyawan);
        }
      });
    </script>
  </div>
</div>
<div class="form-inline">
  <div class="form-group">
    <label style="width:125px;">Kode Obat</label>
    <input class="form-control" style="width:175px;" id="med_kodeObat" type="text" readonly />
  </div>
  <div class="form-group">
    <label style="width:125px;">Nama Obat</label>
    <input class='form-control' style='width:175px;' id='med_namaObat' type='text' list="obatList"/>
    <datalist id="obatList"></datalist>
  </div>
  <div class="form-group">
    <label style="width:125px;">Jumlah</label>
    <input class='form-control' style='width:175px;' id='med_quantity' type='text' />
  </div>
  <div class="form-group">
    <label style="width:125px;">Diskon</label>
    <input class='form-control' style='width:175px;' id='diskon' type='text' placeholder="minimal 0" value="0"  />
  </div>
</div>
<div>
  <br />
  <h4>Nota Non Resep</h4>
  <div id="lembarNota">

  </div>
</div>
<script>
$("#med_namaObat").keyup( function(){
  var no = $("#med_namaObat").val();
  if( no.length >=3 ){
    $.getJSON('./ajax/obatResep.php?no='+no, function(obats){
      $("#obatList option").remove();
      $.each(obats,function(i,obat){
        $("#obatList").append("<option value='"+obat.nama+"'>");
      });
    });
  }
});

$("#med_namaObat").keypress( function(e){
  if(e.which == 13){
    e.preventDefault();
    $("#med_quantity").focus();
    $.getJSON('./ajax/dataObat.php?no='+$("#med_namaObat").val(), function(data){
      console.log(data);
      $("#med_kodeObat").val(data.kode);
      $("#med_quantity").prop("placeholder","stok :"+data.stock);
    });
  }
});

$("#med_quantity").keypress(function(e){
  if( e.which == 13 ){
    e.preventDefault();
    $("#diskon").focus();
  }
});

$("#diskon").keypress(function(e){
  if( e.which == 13 ){
    e.preventDefault();
    var jhr = $("#trxtipe").val();
    if( jhr == 'non_resep' ){
      var idxHarga = parseInt(localStorage.getItem('nrs'))/100;
    }else if( jhr == 'dokter' ){
      var idxHarga = parseInt(localStorage.getItem('dkt'))/100;
    }else{
      var idxHarga = parseInt(localStorage.getItem('kyw'))/100;
    }
    var trxId     = $("#trxId").val();
    var trxDate   = $("#trxDate").val();
    var trxShift  = $("#trxShift").val();
    var trxTipe   = $("#trxtipe").val();
    var kodeObat  = $("#med_kodeObat").val();
    var kuantitas = $("#med_quantity").val();
    var diskon    = $("#diskon").val();
    var trxData = {trxId,trxDate,trxShift,trxTipe,idxHarga,kodeObat,kuantitas,diskon}

    $.post('saveTrxNonResep.php',{
      data : trxData
    },function(res){
      console.log(res);
      $("#med_kodeObat").val('');
      $("#med_namaObat").val('');
      $("#med_namaObat").focus();
      $("#med_quantity").val('');
      $("#diskon").val('');
    });
    loadNota();
  }
  loadNota();
});

function loadNota(){
  var trxId     = $("#trxId").val();
  $.ajax({
    url     : './ajax/notaNonResep.php?id='+trxId,
    success : function(nota){
      $("#lembarNota").html(nota);
    }
  });
}
</script>
