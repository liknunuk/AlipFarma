<?php
require "./lib/class.crud.inc.php";
$af = new dbcrud();
$nomorResep = '013001';
$obat = $af->picksome("*","recipeIn","trxId='{$nomorResep}'");
echo "<pre>";
print_r($obat);
echo "</pre>";
echo "<table style='font-family:monospace;'>";
for ( $i = 0 ; $i < COUNT($obat) ; $i++ ){
    echo "
    <tr>
        <td><input type='text' readonly name=trxid[] value='". $obat[$i]['trxId'] ."' size=6 /></td>
        <td><input type='text' readonly name=kodeObat[] value='". $obat[$i]['kodeObat'] ."' size=6 /></td>
        <td>" . $obat[$i]['namaObat'] . "</td>
        <td><input type='text' readonly name=banyaknya[] value='". $obat[$i]['banyaknya'] ."' 
            size=6 style='text-align:right;' /></td>
        <td>
            <input class='form-control' name=ap[] list=dosisList />
                <datalist id=dosisList>
                <option value='1x1, Pagi' >
                <option value='1x1, Siang' >
                <option value='1x1, Sore' >
                <option value='1x1, Malam' >
                <option value='2x1, Pagi - Siang' >
                <option value='2x1, Pagi - Sore' >
                <option value='2x1, Pagi - Malam' >
                <option value='2x1, Siang - Sore' >
                <option value='2x1, Siang - Malam' >
                <option value='2x1, Sore - Malam' >
                <option value='3x1, Pagi-Siang-Malam' >
                <option value='1x2, Pagi' >
                <option value='1x2, Siang' >
                <option value='1x2, Sore' >
                <option value='1x2, Malam' >
                <option value='2x2, Pagi - Siang' >
                <option value='2x2, Pagi - Sore' >
                <option value='2x2, Pagi - Malam' >
                <option value='2x2, Siang - Sore' >
                <option value='2x2, Siang - Malam' >
                <option value='2x2, Sore - Malam' >
                <option value='3x2, Pagi-Siang-Malam' >
                </datalist>
        </td>
    </tr>
    ";
}
echo "</table>";
?>