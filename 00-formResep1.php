<div class='page-banner'>
  <p>LOKET PENERIMAAN RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class="row">
  <div class="form-horizontal col-lg-4">
    <div class="form-group vrapet">
      <label class="col-sm-4">No. Trx</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="trxId" readonly />
      </div>
    </div>
    <script>
    $.ajax({
      url     : "./ajax/nmTrxResep.php",
      success : function(trxid){
        $("#trxId").val(trxid);
      }
    });
    </script>
    <div class="form-group vrapet">
      <label class="col-sm-4">Tanggal</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="date" id="trxDate" value="<?php echo date('Y-m-d'); ?>" />
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Shift</label>
      <div class="col-sm-8">
        <select class="form-control font-small" id="trxShift">
          <option>Pagi</option>
          <option>Siang</option>
        </select>
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Nama Dokter</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="dokter"/>
        <div id="dokterList"></div>
      </div>
    </div>
    <input type="hidden" id="dokterId" />
    <div style='background-color:#DDD; font-weight:bold; margin:2 0; padding:3 5;'>
      <p style="margin:0;">Pasien</p></div>
    <div class="form-group vrapet">
      <label class="col-sm-4">Nama</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienNama" />
        <div id="pasienList" class="kode-float"></div>
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Alamat</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienAlamat" />
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Kota</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienKota" />
      </div>
    </div>
    <input type="hidden" id="pasienId" />
   <div id="newPasBtn" style='visibility:hidden;'>
     <button class="btn btn-primary form-control" id="saveNewpas" onClick=savepasien()>Simpan Pasien</button>
     <br />
   </div>
   <div class="form-group">
     <div id="requestInfo"></div>
   </div>
   <script>
    function savepasien(){

      $.post(
        'actions.php?mod=i&obj=pasien',{
          id      : $("#pasienId").val(),
          nama    : $("#pasienNama").val(),
          alamat  : $("#pasienAlamat").val(),
          kota    : $("#pasienKota").val()
        },function(response){
          //console.log(response);
          $("#newPasBtn").css("visibility","hidden");
        }
      );
    }
   </script>
   <div class="form-group">
     <button class="btn btn-success form-control" id="rqstSubmit" onclick=saveRecipeRqst()>Simpan Data Resep</button>
   </div>
  </div>
  <div class="col-lg-8">
    <div class="form-inline">
      <div class="form-group">
        <label>Kode Obat</label>
        <input class="form-control" type="text" id="med_kodeObat" placeHolder="Kode Obat" readOnly/>
      </div>
      <div class="form-group">
        <label>Nama Obat</label>
        <input class="form-control" type="text" id="med_nama" placeHolder="Nama Obat" />
        <div class="kode-float" id="obatList"></div>
      </div>
      <div class="form-group">
        <label>Banyaknya</label>
        <input class="form-control" type="text" id="qty" placeHolder="Banyaknya Obat"
        onkeypress=saveRecipeIn(event) />
      </div>
      <div class="form-group" id="recipeInAlert">

      </div>
    </div>
    <br />
    <section>
      <div class="row">
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama Obat</th>
                <th>Banyaknya</th>
                <th>Kontrol</th>
              </tr>
            </thead>
            <tbody id="rcpIn_list"></tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>
<script>
  function saveRecipeRqst(){
    $.post("saveResepData.php",{
      mode    : 'ins',
      trxId   : $("#trxId").val(),
      tanggal : $("#trxDate").val(),
      shift   : $("#trxShift").val(),
      idPasien: $("#pasienId").val(),
      idDokter: $("#dokterId").val()
    },function(responses){
      $("#requestInfo").html(responses);
      $("#rqstSubmit").toggle();
      $("#trxId").prop("disabled",true);
      $("#trxDate").prop("disabled",true);
      $("#trxShift").prop("disabled",true);
      $("#dokter").prop("disabled",true);
      $("#pasienNama").prop("disabled",true);
      $("#pasienAlamat").prop("disabled",true);
      $("#pasienKota").prop("disabled",true);
      $("#med_nama").focus();
    });
  }

  function saveRecipeIn(e){
    $("#obatList").hide();
    if(e.which == 13 ){
      e.preventDefault();
      $.post("saveRecipeIn.php",{
        mode      : 'ins',
        trxId     : $("#trxId").val(),
        kodeObat  : $("#med_kodeObat").val(),
        namaObat  : $("#med_nama").val(),
        banyaknya : $("#qty").val()
      },function(responses){
        //console.log(responses);
        $("#recipeInAlert").html(responses);
        $("#med_kodeObat").val('');
        $("#med_nama").val('');
        $("#med_nama").focus();
        $("#qty").val('');
        loadRecipeIn($("#trxId").val());
      })
    }
  }

  function loadRecipeIn(trxId){
    $.getJSON("ajax/recipeInList.php?id="+trxId,function(recipes){
        $("#rcpIn_list tr").remove();
        var nu = 1;
        $.each(recipes, function(i,data){
          $("#rcpIn_list").append(
          "<tr>"+
          "<td>"+nu+"</td>"+
          "<td>"+data.kodeObat+"</td>"+
          "<td>"+data.namaObat+"</td>"+
          "<td>"+data.banyaknya+"</td>"+
          "<td><a href=javascript:void(0) onClick=batalno("+data.idxdata+",'"+trxId+"')>Batal</a></td>"+
          "</tr>"
          );
          nu+=1;
        });
      }
    );
  }

  function batalno(idx,trxId){
    $.post("saveRecipeIn.php",{
      mode : 'rmv',
      id : idx
    },function(response){
      $("#recipeInAlert").html(response);
    });
    loadRecipeIn(trxId);
  }
</script>
