<?php
  /*
  echo "<pre>";
  print_r($_POST);
  echo "</pre>";
  */
  require_once("./lib/class.crud.inc.php");
  $rcp = new dbcrud();
  
  $harga = $rcp->pickone("harga_resep resep","vwHargaObat","kode",$_POST['kodeObat']);

  if($_GET['mod'] == 'ins'){
    $sets = 'trxId,kodeObat,harga_resep,banyaknya,aturanPakai,diskon';
    $data = array($_POST['trxId'],$_POST['kodeObat'],$harga['resep'],
            $_POST['banyaknya'],$_POST['aturanPakai'],$_POST['diskon']);
    $rcp->insert("recipeOut",$sets,$data);
    echo '
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <i>Resep Tersimpan ...</i>
    </div>
    ';
  }

  if($_GET['mod'] == 'rmv'){
    $rcp->delete("recipeIn","idxdata",$_POST['id']);
    echo '
    <div class="alert alert-warning alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Berhasil!</strong> Obat resep terhapus ..
    </div>
    ';
  }
?>
