<div class='page-banner'>
  <p>REKAPITULASI PENJUALAN RESEP</p>
</div>
<?php
  require_once("./lib/class.crud.inc.php");
  $recap = new dbcrud();
 ?>
 <div class="table-responsive">
   <table class="table table-small">
     <thead>
       <tr>
         <th>No.</th>
         <th>Kode<br />Dokter</th>
         <th>Nama Dokter</th>
         <th>Nama Pasien</th>
         <th>Nama Obat</th>
         <th>Jumlah Obat</th>
         <th>Aturan Pakai</th>
         <th>Jumlah Harga</th>
         <th>R/</th>
         <th>+E</th>
         <th>%</th>
         <th>Total Harga</th>
       </tr>
     </thead>
     <tbody>
       <?php
        $cols = "trxResep.trxId, dokter.kode kodeDokter, trxResep.dokter namaDokter,
                trxResep.pasienNama namaPasien, trxResep.pasienAlamat alamatPasien,
                obat.nama obat, trxResep.kuantitas jmlObat, trxResep.aturanPakai,
                (trxResep.harga_beli * trxResep.kuantitas) jmHarga,
                trxResep.e_r, trxResep.e_p persen";
        $tbls = "trxResep, obat, dokter";
        $fltr = "dokter.nama = trxResep.dokter && obat.kode = trxResep.kodeObat";

        $sql = "SELECT ".$cols." FROM ".$tbls." WHERE ".$fltr;

        $qry = $recap->transact($sql);

        while($r = $qry->fetch()){
          echo "
          <tr>
            <td>".$r['trxId']."</td>
            <td>".$r['kodeDokter']."</td>
            <td>".$r['namaDokter']."</td>
            <td>".$r['namaPasien']."<br />".$r['alamatPasien']."</td>
            <td>".$r['obat']."</td>
            <td align='right'>".$r['jmlObat']."</td>
            <td>".$r['aturanPakai']."</td>
            <td align='right'>".number_format($r['jmHarga'],0,',','.')."</td>
            <td>".$r['e_r']."</td>
            <td>E = ?</td>
            <td>".$r['persen']."</td>
            <td></td>
          ";
        //  print_r($r);
        }

        ?>
     </tbody>
   </table>
   <?php //echo $sql; ?>
 </div>
