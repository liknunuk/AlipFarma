<div class='page-banner'>
  <p>REKAPITULASI PENJUALAN RESEP</p>
</div>
<div>
  <a style='margin: 10px 40px;' href="exportXls.php?obj=resep">Export Xls</a>
</div>
<?php
  require_once("./lib/class.crud.inc.php");
  $recap = new dbcrud();
 ?>
 <div class="table-responsive">
   <table class="table table-small">
     <thead>
       <tr>
         <th width="50px">Kode Dokter</th>
         <th>Nama Dokter</th>
         <th>Nama Pasien</th>
         <th>Nama Obat</th>
         <th>Jumlah Obat</th>
         <th>Aturan Pakai</th>
         <th>Jumlah Harga</th>
         <th>R/</th>
         <th>+E</th>
         <th>%</th>
         <th>Total Harga</th>
       </tr>
     </thead>
     <tbody>
       <?php
        $skrg = date('Y-m-d');
        $shft = 'Pagi';
        $rqst = getRequest($recap,$skrg,$shft);
        //print_r($rqst);
        $i = 0;
        while($i < COUNT($rqst)){
          $hargaObat = getTotalHarga($recap,$rqst[$i]['trxId']);
          $totalTagihan=$hargaObat + $rqst[$i]['faktor_r'] + $rqst[$i]['faktor_e'] + $rqst[$i]['faktor_p'];
          $nomor = $i+1;

          $sql2 = "SELECT obat.nama, banyaknya, aturanPakai
                  FROM recipeOut, obat
                  WHERE obat.kode = recipeOut.kodeObat && trxId='".$rqst[$i]['trxId']."'";
          $qry2 = $recap->transact($sql2);
          $j=0;
          while($res2 = $qry2->fetch()){
            if($j == 0){
              echo "
                <tr>
                  <td>".$rqst[$i]['idDokter']."</td>
                  <td>".$rqst[$i]['dokter']."</td>
                  <td>".$rqst[$i]['pasien']."</td>
                  <td>".$res2['nama']."</td>
                  <td>".$res2['banyaknya']."</td>
                  <td>".$res2['aturanPakai']."</td>
                  <td class='ra'>".number_format($hargaObat,0,',','.')."</td>
                  <td class='ra'>".number_format($rqst[$i]['faktor_r'],0,',','.')."</td>
                  <td class='ra'>".number_format($rqst[$i]['faktor_e'],0,',','.')."</td>
                  <td class='ra'>".number_format($rqst[$i]['faktor_p'],0,',','.')."</td>
                  <td class='ra'>".number_format($totalTagihan,0,',','.')."</td>
                </tr>
              ";
            }else{
            echo "
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>".$res2['nama']."</td>
                <td>".$res2['banyaknya']."</td>
                <td>".$res2['aturanPakai']."</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            ";
          }
            $j++;

          }

          $i++;
        }




        function getRequest($recap,$skrg,$shft){
          $sql1 = "SELECT trxId, idDokter, dokter.nama dokter, pasien.nama pasien,
                          faktor_r, faktor_e, faktor_p, konsultasi
                   FROM recipeRqst, dokter, pasien
                   WHERE  tanggal = '".$skrg."' && shift = '".$shft."' &&
                          dokter.kode = recipeRqst.idDokter &&
                          pasien.id = recipeRqst.idPasien";
          $qry1 = $recap->transact($sql1);
          $rqst = array();
          while($res1 = $qry1->fetch()){
            array_push($rqst,$res1);
          }
          return($rqst);
          $qry1 = NULL;
        }
        function getTotalHarga($recap,$trxId){
          $sql3 = "SELECT SUM((100 - diskon )/100 * (harga_resep * banyaknya)) AS totalHarga
                   FROM recipeOut WHERE trxId = '".$trxId."' ";
          $qry3 = $recap->transact($sql3);
          $res3 = $qry3->fetch();
          return(ceil($res3['totalHarga']));
          $qry3 = NULL;
        }
        ?>
     </tbody>
   </table>
 </div>
