<div class='page-banner'>
  <p>PENJUALAN RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class="form-jual"><!-- whole block -->
<div class="col-sm-3 form-horizontal" style="height: 800px;">
  <div class="form-group vrapet">
    <label class="col-sm-4">No. Trx</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="trxId" readonly />
    </div>
  </div>
  <script>
  $.ajax({
    url     : "./ajax/nmTrxResep.php",
    success : function(trxid){
      $("#trxId").val(trxid);
    }
  });
  </script>
  <div class="form-group vrapet">
    <label class="col-sm-4">Tanggal</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="date" id="trxDate" value="<?php echo date('Y-m-d'); ?>" />
    </div>
  </div>

  <div class="form-group vrapet">
    <label class="col-sm-4">Shift</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="trxShift" readonly />
    </div>
  </div>

  <div class="form-group vrapet">
    <label class="col-sm-4">Nama Dokter</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="dokter"/>
      <div id="dokterList"></div>
    </div>
  </div>
  <div style='background-color:#DDD; font-weight:bold; margin:2 0; padding:3 5;'>
    <p style="margin:0;">Pasien</p></div>
  <div class="form-group vrapet">
    <label class="col-sm-4">Nama</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="pasienNama" />
      <div id="pasienList" class="kode-float"></div>
    </div>
  </div>

  <div class="form-group vrapet">
    <label class="col-sm-4">Alamat</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="pasienAlamat" />
    </div>
  </div>

  <div class="form-group vrapet">
    <label class="col-sm-4">Kota</label>
    <div class="col-sm-8">
      <input class="form-control font-small" type="text" id="pasienKota" />
    </div>
  </div>
  <input type="hidden" id="pasienId" />
 <div id="newPasBtn" style='visibility:hidden;'>
   <button class="btn btn-primary" id="saveNewpas" onClick=savepasien()>Simpan Pasien</button>
 </div>
 <script>
  function savepasien(){

    $.post(
      'actions.php?mod=i&obj=pasien',{
        id      : $("#pasienId").val(),
        nama    : $("#pasienNama").val(),
        alamat  : $("#pasienAlamat").val(),
        kota    : $("#pasienKota").val()
      },function(response){
        console.log(response);
        $("#newPasBtn").css("visibility","hidden");
      }
    );
  }
 </script>

</div>
<div class="col-sm-9">
  <table class='table font-small'>
    <thead>
      <tr>
        <th>Nama / Kode Obat</th>
        <th>Banyaknya</th>
        <th>Aturan Pakai</th>
        <th>Diskon</th>
        <th>\R</th>
        <th>Harga</th>
        <th>%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <input class="form-control font-small" id="med_nama" type="text" />
          <div class="kode-float" id="obatList"></div>
          <input class="form-control font-small" id="med_kodeObat" type="text" readonly />
        </td>
        <td><input class="form-control font-small" id="qty" type="text" maxlength="4"/></td>
        <td>
          <input class="form-control" id="med_dosis" list="dosisList" />
          <datalist id="dosisList">
            <option value='1x1, Pagi' >
            <option value='1x1, Siang' >
            <option value='1x1, Sore' >
            <option value='1x1, Malam' >
            <option value='2x1, Pagi - Siang' >
            <option value='2x1, Pagi - Sore' >
            <option value='2x1, Pagi - Malam' >
            <option value='2x1, Siang - Sore' >
            <option value='2x1, Siang - Malam' >
            <option value='2x1, Sore - Malam' >
            <option value='3x1, Pagi-Siang-Malam' >
            <option value='1x2, Pagi' >
            <option value='1x2, Siang' >
            <option value='1x2, Sore' >
            <option value='1x2, Malam' >
            <option value='2x2, Pagi - Siang' >
            <option value='2x2, Pagi - Sore' >
            <option value='2x2, Pagi - Malam' >
            <option value='2x2, Siang - Sore' >
            <option value='2x2, Siang - Malam' >
            <option value='2x2, Sore - Malam' >
            <option value='3x2, Pagi-Siang-Malam' >

          </datalist>
        </td>
        <td><input class="form-control font-small" id="rsp_diskon" type="number" /></td>
        <td><input class="form-control font-small" id="e_r" type="number" /></td>
        <td><input class="form-control" id="med_price" type="number" readonly /></td>
        <td><input class="form-control font-small" id="e_p" type="text" readonly /></td>
      </tr>
    </tbody>
  </table>
  <script>
  $.getJSON(
    './ajax/embalas.php',
    function(emb){
      console.log(emb);
      $("#e_r").val(emb.e_r);
      $("#e_p").val(emb.e_p);
    }
   );
  </script>
  <!-- daftar resep -->
  <table class='font-small' width="100%" border=0>
    <thead>
      <tr>
        <th>No.</th>
        <th>Kode & Nama Obat</th>
        <th>Bny</th>
        <th>Harga</th>
        <th>Jml Harga</th>
        <th>Diskon</th>
        <th>\R</th>
        <th>%</th>
        <th width="100px">Total Harga</th>
        <th width="50px">Edit</th>
      </tr>
    </thead>
    <tbody id="dataResep">

    </tbody>
  </table>
  <div style="text-align:right; padding-right: 30px; margin-top:20px;">
    <a href='javascript:void(0)' class='btn btn-success' onClick=printResep()>Cetak Nota</a>
    <a href='javascript:void(0)' class='btn btn-success' onClick=printLabel()>Cetak Label</a>
    <a href='javascript:void(0)' class='btn btn-danger' onClick=buangResep()>Batal</a>
  </div>
  <!-- daftar resep -->
</div>

</div> <!-- whole block -->
<script>
  var dina = new Date();
  var jame = dina.getHours();
  var jam = parseInt(jame);
  if( jam >= 7 && jam < 14 ){
    $("#trxShift").val('Pagi')
  }else if( jam >= 14 && jam < 21){
    $("#trxShift").val('Sore')
  }else{
    $("#trxShift").val('Lembur');
  }

  function printResep(){
    localStorage.setItem( 'totl', $("#nota-total").html());
    localStorage.setItem( 'disk', $("#nota-diskon").val());
    localStorage.setItem( 'e_l' , $("#nota-l").val());
    localStorage.setItem( 'kons', $("#nota-konsul").val());
    localStorage.setItem( 'gtot', $("#nota-topaid").html());
    localStorage.setItem( 'byar', $("#nota-paid").val());
    localStorage.setItem( 'susk', $("#nota-susuk").html());
    var trxId = $("#trxId").val();
    window.open('print.php?obj=resep&id='+trxId);
  }

  function printLabel(){
    var trxId = $("#trxId").val();
    window.open('print.php?obj=label&id='+trxId);
  }

  function buangResep(){
    var yakin = confirm('Resep akan dihapus permanen !!');
    if(yakin == true ){
      var trxId = $("#trxId").val();
      $.post('./ajax/buangResep.php',{
        tid : trxId
      },function(responses){
        location.reload();
      });
    }

  }
</script>
