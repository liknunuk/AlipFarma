<div class='page-banner'>
  <p>PEMBAYARAN RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class="row">
  <div class="col-lg-4">
    <b>DAFTAR RESEP TERPROSES</b>
    <div class="list-group" id="waitingList">

    </div>
    <button onClick=loadListRecipeIn() class="btn btn-primary">Refresh</button>
  </div>
  <div class="col-lg-4">
    <b>RINCIAN RESEP <span id="recipeId"></span></b>
    <ul class="list-group" id="recipeItems">

    </ul>
  </div>
  <div class="col-lg-4">
    <b>TAGIHAN</b>
    <table class='table'>
      <tr>
        <td>Harga Obat</td>
        <td align="right" id='bill_obat'></td>
      </tr>
      <tr>
        <td>R/</td>
        <td align="right" id='bill_fakr'></td>
      </tr>
      <tr>
        <td>+E</td>
        <td align="right" id='bill_fake'></td>
      </tr>
      <tr>
        <td>%</td>
        <td align="right" id='bill_fakp'></td>
      </tr>
      <tr>
        <td>Konsultasi</td>
        <td align="right" id='bill_knsl'></td>
      </tr>
      <tr>
        <td>Total</td>
        <td align="right"  id='bill_grandTotal'></td>
        <span style="display:none;" id="totalbill"></span>
      </tr>
      <tr>
        <td>Potongan Harga</td>
        <td align="right"><input type="text" style="text-align:right" id="diskon" value="0" onkeypress=tagihanBersih(event,this.value) /></td>
      </tr>
      <tr>
        <td>Tagihan Bersih</td>
        <td align="right"><input type="text" style="text-align:right" id="netbill" readonly /></td>
      </tr>
      <tr>
        <td>Bayar</td>
        <td align="right"><input type="text" style="text-align:right" id="bayar" onKeyPress=kembalian(event,this.value) /></td>
      </tr>
      <tr>
        <td>Kembali</td>
        <td align="right"><input type="text" style="text-align:right" id="kembali" onkeypress=cetakNota() /></td>
      </tr>
      <tr>
        <td colspan=2 id="payAlert"></td>
      </tr>
    </table>
  </div>


</div>
<script>
$(document).ready(function(){
  loadListRecipeIn();
});

function loadListRecipeIn(){
  $("#bill_obat").html('');
  $("#bill_fakr").html('');
  $("#bill_fake").html('');
  $("#bill_fakp").html('');
  $("#bill_knsl").html('');
  $("#bill_grandTotal").html('');
  $("#diskon").val('0');
  $("#netbill").val('');
  $("#bayar").val('');
  $("#kembali").val('');
  $.getJSON("ajax/recipeInData.php?sr=Terproses",function(waitingList){
    $("#waitingList li").remove();
    $.each(waitingList,function(i,data){
      $("#waitingList").append(
        "<li class='list-group-item'>"+
          "<a href='javascript:void(0)' onclick=showItems('"+data.trxId+"')>"+data.trxId+"</a>"+
          "<span class='badge'>"+data.item+"</span>"+
          "<br />"+data.nama+
        "</li>"
      )
    });
  });
}

function showItems(trxId){
  $("#recipeId").html(trxId);
  $.getJSON("ajax/recipeOutItems.php?id="+trxId,function(recipes){

    $("#recipeItems li").remove();
    console.log(recipes);
    $.each(recipes.obat,function(i,data){
      $("#recipeItems").append(
        "<li class='list-group-item'>"+
        data.kodeObat+"<br />"+
        data.namaObat+"<span class='badge'>"+data.banyaknya+"</span>"+
        "</li>"
      );
    });
    $("#bill_obat").html(parseInt(recipes.harga.harga).toLocaleString('id-ID'));
    $("#bill_fakr").html(parseInt(recipes.harga.faktor_r).toLocaleString('id-ID'));
    $("#bill_fake").html(parseInt(recipes.harga.faktor_e).toLocaleString('id-ID'));
    $("#bill_fakp").html(parseInt(recipes.harga.faktor_p).toLocaleString('id-ID'));
    $("#bill_knsl").html(parseInt(recipes.harga.konsultasi).toLocaleString('id-ID'));
    $("#bill_grandTotal").html(parseInt(recipes.total).toLocaleString('id-ID'));
    $("#totalbill").html(recipes.total);
    $("#diskon").focus();
  });
}

  function tagihanBersih(e,tb){
    if( e.which == 13 ){
      var totalbill = parseInt( $("#totalbill").html() );
      var diskon    = parseInt(tb);
      var netbill   = totalbill - diskon;
      $("#netbill").val(netbill);
      $("#bayar").focus();
    }
  }

  function kembalian(e,by){
    if( e.which == 13 ){
      var tagihanBesih  = parseInt( $("#netbill").val());
      var dibayar       = parseInt(by);

      var kembali = dibayar - tagihanBesih;
      $("#kembali").val(kembali);
      $("#kembali").focus();
    }

  }
  function cetakNota(){
    $.post("ajax/bayarResep.php",{
      trxId  : $('#recipeId').html(),
      diskon : $('#diskon').val()
    },function(response){
      $('#payAlert').html(response);
    });

    var cetak = confirm(" Cetak Kuitansi ? ");
    if( cetak == true ){
      var trxId=$('#recipeId').html();
      var jmlah=$('#netbill').val();
      window.open("kuitansi.php?id="+trxId+"&jm="+jmlah,'kuitansi',"width=800px,height=300px,location=0");
    }
  }
</script>
