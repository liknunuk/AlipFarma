<html>
  <head>
    <title>Kuitansi</title>
    <style>
    table {font-family: serif;}
    h2,p{margin:0; padding:0;}
    td {padding: 2px 10px; }
    </style>
  </head>
  <body onload=window.print()>
    <?php
    require_once('./lib/class.crud.inc.php');
    $posa = new dbcrud();
    $pasienId = $posa->pickone("idPasien,tanggal","recipeRqst","trxId",$_GET['id']);
    $pasienDt = $posa->pickone(" * ","pasien","id",$pasienId['idPasien']);
    ?>
    <table style="width: 100%;" border=0 cellspacing=0; >
      <tr>
        <td colspan="2">
          <h2>APOTEK "ALIP FARMA"</h2>
          <p>Jl. Jend. Sudirman No. 3 Banjarnegara</p>
          <p>Apoteker: Nimas Kina Wardani, S.Farm, Apt</p>
          <p><b>19891221/SIPA/ 33.04/2016/20142</b></p>
        </td>
      </tr>
      <tr><td width=200px;>Nomor</td><td>:&nbsp;<?php echo $_GET['id']; ?></tr>
      <tr><td>Telah terima dari</td><td>:&nbsp;<?php echo $pasienDt['nama']; ?></tr>
      <tr><td>Uang sebesar</td><td>:&nbsp;<?php echo "Rp ".number_format($_GET['jm'],0,',','.'); ?> </tr>
      <tr><td>Untuk pembayaran</td><td>:&nbsp; Obat-obatan / Resep / Konsultasi</td></tr>
      <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
      <tr><td>&nbsp;</td>
        <td align="right">&nbsp<?php echo "Banjarnegara, ".$posa->tanggalTerbaca($pasienId['tanggal']); ?></td>
      </tr>
      <tr><td>Terbilang</td>
        <td>:&nbsp; <?php $nilaiUang = terbilang($_GET['jm']); echo"<i>".ucfirst($nilaiUang)." rupiah </i>"; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="right">
          <div style="width: 200px; float:right; margin-right: 30px; text-align: center;">
            <br />
            <i>Petugas</i>
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
<?php
function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}
		return $hasil;
	}

  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai < 20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }
    return $temp;
  }

 ?>
