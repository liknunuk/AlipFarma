DROP TABLE IF EXISTS `formIface`;
CREATE TABLE `formIface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file','number') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO formIface (fgroup,pertanyaan,formName,formId,formType,fparam) VALUES
-- tabel profil
('profil','Nama Apotik','nama','nama','text',''),
('profil','Alamat','alamat','alamat','text',''),
('profil','Nomor Telephone','telephone','telephone','text',''),
('profil','Gambar Logo','logo','logo','file',''),
-- tabel karyawan
('karyawan','Kode Karyawan','kode','kyw_kode','text','Readonly'),
('karyawan','Nama Karyawan','nama','kyw_nama','text',''),
('karyawan','Alamat Rumah','alamat','kyw_alamat','textarea',"rows=3"),
('karyawan','Nomor HP','no_telp','kyw_no_telp','text','maxlength=12'),
('karyawan','Keterangan','ket','kyw_ket','textarea',"rows=3"),
-- tabel dokter
('dokter','Kode Dokter','kode','dok_kode','text','Readonly'),
('dokter','Nama Dokter','nama','dok_nama','text',''),
('dokter','Alamat Rumah','alamat','dok_alamat','textarea',"rows=3"),
('dokter','Nomor HP','no_telp','dok_no_telp','text','maxlength=12'),
('dokter','Keterangan','ket','dok_ket','textarea',"rows=3"),
-- tabel obat
('obat','Nomor Urut','nomor','med_nomor','text',"readonly"),
('obat','Kode Obat','kode','med_kode','text',""),
('obat','Nama Obat','nama','med_nama','text',""),
('obat','Indikasi','indikasi','med_indikasi','text',""),
('obat','Kategori','kategori','med_kategori','select',"kategori/kategori/optKategoriObat"),
('obat','Satuan Jual','satuan','med_satuan','select',"satuan/satuan/optSatuanObat"),
('obat','Harga Pembelian','harga_beli','med_harga_beli','number',""),
('obat','Isi / Box','ipb','med_ipb','number',"value='0'"),
('obat','Harga HV','harga_hv','med_harga_hv','number',""),
('obat','Harga Resep','harga_resep','med_harga_resep','number',""),
('obat','Stock Obat','stock','med_stock','number',""),
-- tabel pasien
-- xxyyy xx=tahun input yyy=urut
('pasien','ID Pasien','id','pas_id','number',""),
('pasien','Nama Pasien','nama','pas_nama','text',""),
('pasien','Alamat Rumah','alamat','pas_alamat','textarea',"rows=3"),
-- tabel optJenisHarga
('optJenisHarga','Harga Resep','resep','resep','number',""),
('optJenisHarga','Harga Non Resep','non_resep','non_resep','number',""),
('optJenisHarga','Harga Dokter','dokter','dokter','number',""),
('optJenisHarga','Harga Karyawan','karyawan','karyawan','number',""),
-- tabel optEmbalase
('optEmbalase','R/','e_r','e_r','number',""),
('optEmbalase','L/','e_l','e_l','number',""),
('optEmbalase','%','e_p','e_p','text',""),
-- tabel optKategoriObat
('optKategoriObat','Nomor Urut','nomor','kat_nomor','number',"Readonly"),
('optKategoriObat','Kategori','kategori','kategori','text',""),
-- tabel optSatuanObat
('optSatuanObat','Nomor urut','nomor','sat_nomor','number',"Readonly"),
('optSatuanObat','Satuan','satuan','satuan','text',""),
-- tabel fakturPembelian-data
('fabelData','Nomor Faktur','nomorFaktur','nomorFaktur','text',''),
('fabelData','Nama PBF / Distributor','namaPBF','namaPBF','text',''),
('fabelData','Tanggal Faktur','tanggalFaktur','tanggalFaktur','date',''),
('fabelData','Tanggal Jatuh Tempo','tanggalJatuhTempo','tanggalJatuhTempo','date',''),
('fabelData','Total Potongan','totalDiskon','totalDiskon','number','value="0"');
