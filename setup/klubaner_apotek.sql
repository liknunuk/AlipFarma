
-- Struktur dari tabel `dokter`
--
DROP TABLE IF EXISTS dokter; 
CREATE TABLE `dokter` (
  `kode` int(2) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokter`
--

INSERT INTO `dokter` (`kode`, `nama`, `alamat`, `no_telp`, `ket`) VALUES
(03, 'Dr. Budi L Sp. PD (RMH)', '', '', ''),
(04, 'Dr. Budi L Sp. PD (RSU)', '', '', ''),
(05, 'Dr. Eko Pudji Antono Sp. S (RM', '', '', ''),
(06, 'Dr. Eko Pudji Antono Sp. S (RS', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fabelData`
--

DROP TABLE IF EXISTS fabelData; 
CREATE TABLE `fabelData` (
  `nomorFaktur` varchar(20) DEFAULT NULL,
  `namaPBF` varchar(30) DEFAULT NULL,
  `tanggalFaktur` date DEFAULT NULL,
  `tanggalJatuhTempo` date DEFAULT NULL,
  `totalDiskon` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fabelData`
--

INSERT INTO `fabelData` (`nomorFaktur`, `namaPBF`, `tanggalFaktur`, `tanggalJatuhTempo`, `totalDiskon`) VALUES
('0000', 'alipFarma', '2018-01-01', '2018-01-14', 0);

--
-- Trigger `fabelData`
--
DELIMITER $$
CREATE TRIGGER `batalBeliObat` BEFORE DELETE ON `fabelData` FOR EACH ROW BEGIN DELETE FROM fabelList WHERE nomorFaktur = OLD.nomorFaktur ;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `fabelList`
--

DROP TABLE IF EXISTS fabelList; 
CREATE TABLE `fabelList` (
  `itemIndex` int(6) NOT NULL,
  `nomorFaktur` varchar(30) DEFAULT NULL,
  `kodeObat` varchar(30) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT '0',
  `ppn10` enum('1','0') DEFAULT '1',
  `disc` float DEFAULT '0',
  `kuantitas` int(4) DEFAULT '0',
  `isiPerBox` int(3) DEFAULT '1',
  `satuanJual` varchar(20) DEFAULT 'Tablet'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Trigger `fabelList`
--
DELIMITER $$
CREATE TRIGGER `batalTambahObat` BEFORE DELETE ON `fabelList` FOR EACH ROW BEGIN UPDATE obat SET stock = stock - (OLD.kuantitas * OLD.isiPerBox) WHERE kode = OLD.kodeObat; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `stokObatTambah` AFTER INSERT ON `fabelList` FOR EACH ROW BEGIN
  UPDATE obat SET stock = stock + ( NEW.kuantitas * NEW.isiPerBox ) WHERE kode = NEW.kodeObat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `formIface`
--

DROP TABLE IF EXISTS formIface; 
CREATE TABLE `formIface` (
  `urut` int(3) NOT NULL,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file','number') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `formIface`
--

INSERT INTO `formIface` (`urut`, `fgroup`, `pertanyaan`, `formName`, `formType`, `formId`, `fparam`) VALUES
(1, 'profil', 'Nama Apotik', 'nama', 'text', 'nama', ''),
(2, 'profil', 'Alamat', 'alamat', 'text', 'alamat', ''),
(3, 'profil', 'Nomor Telephone', 'telephone', 'text', 'telephone', ''),
(4, 'profil', 'Gambar Logo', 'logo', 'file', 'logo', ''),
(5, 'karyawan', 'Kode Karyawan', 'kode', 'text', 'kyw_kode', 'Readonly'),
(6, 'karyawan', 'Nama Karyawan', 'nama', 'text', 'kyw_nama', ''),
(7, 'karyawan', 'Alamat Rumah', 'alamat', 'textarea', 'kyw_alamat', 'rows=3'),
(8, 'karyawan', 'Nomor HP', 'no_telp', 'text', 'kyw_no_telp', 'maxlength=12'),
(9, 'karyawan', 'Keterangan', 'ket', 'textarea', 'kyw_ket', 'rows=3'),
(10, 'dokter', 'Kode Dokter', 'kode', 'text', 'dok_kode', 'Readonly'),
(11, 'dokter', 'Nama Dokter', 'nama', 'text', 'dok_nama', ''),
(12, 'dokter', 'Alamat Rumah', 'alamat', 'textarea', 'dok_alamat', 'rows=3'),
(13, 'dokter', 'Nomor HP', 'no_telp', 'text', 'dok_no_telp', 'maxlength=12'),
(14, 'dokter', 'Keterangan', 'ket', 'textarea', 'dok_ket', 'rows=3'),
(15, 'obat', 'Nomor Urut', 'nomor', 'text', 'med_nomor', 'readonly'),
(16, 'obat', 'Kode Obat', 'kode', 'text', 'med_kode', ''),
(17, 'obat', 'Nama Obat', 'nama', 'text', 'med_nama', ''),
(18, 'obat', 'Indikasi', 'indikasi', 'text', 'med_indikasi', ''),
(19, 'obat', 'Kategori', 'kategori', 'select', 'med_kategori', 'kategori/kategori/optKategoriObat'),
(20, 'obat', 'Satuan Jual', 'satuan', 'select', 'med_satuan', 'satuan/satuan/optSatuanObat'),
(21, 'obat', 'Harga Pembelian', 'harga_beli', 'number', 'med_harga_beli', ''),
(22, 'obat', 'Isi / Box', 'isiPerBox', 'number', 'med_ipb', 'value=\'0\''),
(23, 'obat', 'Harga HV', 'harga_hv', 'number', 'med_harga_hv', ''),
(24, 'obat', 'Harga Resep', 'harga_resep', 'number', 'med_harga_resep', ''),
(25, 'obat', 'Stock Obat', 'stock', 'number', 'med_stock', ''),
(26, 'pasien', 'ID Pasien', 'id', 'number', 'pas_id', ''),
(27, 'pasien', 'Nama Pasien', 'nama', 'text', 'pas_nama', ''),
(28, 'pasien', 'Alamat Rumah', 'alamat', 'text', 'pas_alamat', ''),
(29, 'optJenisHarga', 'Harga Resep', 'resep', 'number', 'resep', ''),
(30, 'optJenisHarga', 'Harga Non Resep', 'non_resep', 'number', 'non_resep', ''),
(31, 'optJenisHarga', 'Harga Dokter', 'dokter', 'number', 'dokter', ''),
(32, 'optJenisHarga', 'Harga Karyawan', 'karyawan', 'number', 'karyawan', ''),
(33, 'optEmbalase', 'R/', 'e_r', 'number', 'e_r', ''),
(34, 'optEmbalase', 'L/', 'e_l', 'number', 'e_l', ''),
(35, 'optEmbalase', '%', 'e_p', 'text', 'e_p', ''),
(36, 'optKategoriObat', 'Nomor Urut', 'nomor', 'number', 'kat_nomor', 'Readonly'),
(37, 'optKategoriObat', 'Kategori', 'kategori', 'text', 'kategori', ''),
(38, 'optSatuanObat', 'Nomor urut', 'nomor', 'number', 'sat_nomor', 'Readonly'),
(39, 'optSatuanObat', 'Satuan', 'satuan', 'text', 'satuan', ''),
(40, 'fabelData', 'Nomor Faktur', 'nomorFaktur', 'text', 'nomorFaktur', ''),
(41, 'fabelData', 'Nama PBF / Distributor', 'namaPBF', 'text', 'namaPBF', ''),
(42, 'fabelData', 'Tanggal Faktur', 'tanggalFaktur', 'date', 'tanggalFaktur', ''),
(43, 'fabelData', 'Tanggal Jatuh Tempo', 'tanggalJatuhTempo', 'date', 'tanggalJatuhTempo', ''),
(44, 'fabelData', 'Total Potongan', 'totalDiskon', 'number', 'totalDiskon', 'value=\"0\"'),
(45, 'pasien', 'Kota', 'kota', 'text', 'pas_kota', ''),
(46, 'pbf', 'Nomor PBF', 'pbf_nomor', 'text', 'pbf_nomor', ''),
(47, 'pbf', 'Nama PBF', 'pbf_nama', 'text', 'pbf_nama', ''),
(48, 'pbf', 'Telephone', 'pbf_telp', 'text', 'pbf_telp', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

DROP TABLE IF EXISTS karyawan; 
CREATE TABLE `karyawan` (
  `kode` int(2) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`kode`, `nama`, `alamat`, `no_telp`, `ket`) VALUES
(01, 'Sri Giyanti', 'Kalibenada ', '08132772700', 'Apoteker'),
(02, 'Kinta', 'Kutabanjar', '', 'Apoteker'),
(03, 'Lita', 'Parakancanggah', '', 'Kasir 1'),
(04, 'Een', 'Perum Kalisemi', '', 'Kasir 2'),
(06, 'Rudy', '', '', ''),
(07, 'Nadia', 'Banjarnegara\r\n', '', ''),
(08, 'Anto', 'Banjarnegara\r\n', '', ''),
(09, 'Yamti', '', '', ''),
(10, 'Endah', 'Banjarnegara', '', ''),
(12, 'Yuan', 'Banjarnegara', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat`
--

DROP TABLE IF EXISTS obat; 
CREATE TABLE `obat` (
  `nomor` int(4) UNSIGNED NOT NULL,
  `kode` varchar(30) DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `indikasi` tinytext,
  `kategori` varchar(12) DEFAULT NULL,
  `satuan` varchar(6) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT '0',
  `isiPerBox` int(3) DEFAULT '0',
  `harga_hv` int(6) DEFAULT '0',
  `harga_resep` int(6) DEFAULT '0',
  `stock` int(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Struktur dari tabel `optEmbalase`
--

DROP TABLE IF EXISTS optEmbalase; 
CREATE TABLE `optEmbalase` (
  `e_r` int(5) DEFAULT NULL,
  `e_l` int(5) DEFAULT NULL,
  `e_p` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `optEmbalase`
--

INSERT INTO `optEmbalase` (`e_r`, `e_l`, `e_p`) VALUES
(1000, 2500, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `optJenisHarga`
--

DROP TABLE IF EXISTS optJenisHarga; 
CREATE TABLE `optJenisHarga` (
  `resep` float DEFAULT NULL,
  `non_resep` float DEFAULT NULL,
  `dokter` float DEFAULT NULL,
  `karyawan` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `optJenisHarga`
--

INSERT INTO `optJenisHarga` (`resep`, `non_resep`, `dokter`, `karyawan`) VALUES
(30, 15, 0, 0);

--
-- Trigger `optJenisHarga`
--
DELIMITER $$
CREATE TRIGGER `updateHvResep` AFTER UPDATE ON `optJenisHarga` FOR EACH ROW BEGIN UPDATE obat SET harga_resep = (( NEW.resep + 100 ) / 100 * harga_beli ), harga_hv = (( NEW.non_resep + 100 ) / 100 * harga_beli ); END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `optKategoriObat`
--

DROP TABLE IF EXISTS optKategoriObat; 
CREATE TABLE `optKategoriObat` (
  `nomor` int(2) NOT NULL,
  `kategori` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `optKategoriObat`
--

INSERT INTO `optKategoriObat` (`nomor`, `kategori`) VALUES
(1, 'Obat Reguler'),
(2, 'HV'),
(3, 'Narkotik'),
(4, 'Psikotropika'),
(5, 'OOT'),
(6, 'Sirup'),
(7, 'Salep '),
(8, 'Injeksi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `optSatuanObat`
--

DROP TABLE IF EXISTS optSatuanObat; 
CREATE TABLE `optSatuanObat` (
  `nomor` int(2) NOT NULL,
  `satuan` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `optSatuanObat`
--

INSERT INTO `optSatuanObat` (`nomor`, `satuan`) VALUES
(1, 'Tablet'),
(2, 'Capsul'),
(3, 'Box'),
(4, 'Pcs'),
(5, 'Strip');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

DROP TABLE IF EXISTS pasien; 
CREATE TABLE `pasien` (
  `id` int(5) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext,
  `kota` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id`, `nama`, `alamat`, `kota`) VALUES
(18001, 'Triyono', 'Kalisemi ', 'Banjarnegara'),
(18002, 'Pasien Tes', 'Krandegan', 'Banjarnegara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pbf`
--
DROP TABLE IF EXISTS pbf; 
CREATE TABLE `pbf` (
  `pbf_nomor` int(3) UNSIGNED ZEROFILL NOT NULL,
  `pbf_nama` varchar(40) DEFAULT NULL,
  `pbf_telp` varchar(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

DROP TABLE IF EXISTS profil; 
CREATE TABLE `profil` (
  `id` int(1) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext,
  `telephone` varchar(16) DEFAULT NULL,
  `logo` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil`
--

INSERT INTO `profil` (`id`, `nama`, `alamat`, `telephone`, `logo`) VALUES
(1, 'Alip Farma', 'Jl. Jend. Sudirman Banjarnegara No. 3 Banjarnegara', '0286 594118', 'img/logo-mini.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `recipeIn`
--

DROP TABLE IF EXISTS recipeIn; 
CREATE TABLE `recipeIn` (
  `idxdata` int(6) UNSIGNED NOT NULL,
  `trxId` varchar(6) DEFAULT NULL,
  `kodeObat` varchar(30) DEFAULT NULL,
  `namaObat` varchar(40) DEFAULT NULL,
  `banyaknya` int(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Struktur dari tabel `recipeOut`
--

DROP TABLE IF EXISTS recipeOut; 
CREATE TABLE `recipeOut` (
  `idxdata` int(6) UNSIGNED NOT NULL,
  `trxId` varchar(6) DEFAULT NULL,
  `kodeObat` varchar(30) NOT NULL,
  `harga_resep` int(5) DEFAULT '0',
  `banyaknya` int(2) DEFAULT '1',
  `aturanPakai` varchar(40) DEFAULT NULL,
  `diskon` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Trigger `recipeOut`
--
DELIMITER $$
CREATE TRIGGER `resepWurung` BEFORE DELETE ON `recipeOut` FOR EACH ROW BEGIN
UPDATE obat SET stock = stock + OLD.banyaknya WHERE trxId = OLD.kodeObat;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `stokObatKurangRs` AFTER INSERT ON `recipeOut` FOR EACH ROW BEGIN   UPDATE obat SET stock = stock - NEW.banyaknya WHERE kode = NEW.kodeObat; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `recipeRqst`
--

DROP TABLE IF EXISTS recipeRqst; 
CREATE TABLE `recipeRqst` (
  `trxId` varchar(6) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `shift` enum('Pagi','Siang') DEFAULT 'Pagi',
  `idPasien` int(5) DEFAULT NULL,
  `idDokter` int(2) DEFAULT NULL,
  `faktor_r` int(5) DEFAULT '0',
  `faktor_e` int(5) DEFAULT '0',
  `faktor_p` int(5) DEFAULT '0',
  `konsultasi` int(6) DEFAULT '0',
  `diskon` int(6) DEFAULT '0',
  `statusBayar` enum('Terhutang','Terproses','Lunas') DEFAULT 'Terhutang'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Struktur dari tabel `trxNonResep`
--

DROP TABLE IF EXISTS trxNonResep; 
CREATE TABLE `trxNonResep` (
  `itemIndex` int(6) NOT NULL,
  `trxId` int(6) UNSIGNED ZEROFILL NOT NULL,
  `trxDate` date DEFAULT NULL,
  `trxShift` enum('Pagi','Sore','Lembur') DEFAULT 'Pagi',
  `jenisHarga` enum('non_resep','dokter','karyawan') DEFAULT 'non_resep',
  `idxHarga` float DEFAULT '1',
  `kodeObat` varchar(30) NOT NULL,
  `kuantitas` int(3) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT '0',
  `diskon` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Trigger `trxNonResep`
--
DELIMITER $$
CREATE TRIGGER `nonrsWurung` BEFORE DELETE ON `trxNonResep` FOR EACH ROW BEGIN UPDATE obat SET stock = stock + OLD.kuantitas WHERE kode = OLD.kodeObat; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `stokObatKurangHv` AFTER INSERT ON `trxNonResep` FOR EACH ROW BEGIN
  UPDATE obat SET stock = stock - NEW.kuantitas WHERE kode = NEW.kodeObat;
END
$$
DELIMITER ;

-- --------------------------------------------------------


--
-- Struktur untuk view `vwRecipeOut`
--

DROP TABLE IF EXISTS vwRecipeOut; 
CREATE ALGORITHM=UNDEFINED DEFINER=`farmacolog`@`localhost` SQL SECURITY DEFINER VIEW `vwRecipeOut`  AS  select `recipeOut`.`idxdata` AS `idxdata`,`recipeOut`.`trxId` AS `trxId`,`recipeOut`.`kodeObat` AS `kodeObat`,`recipeOut`.`harga_resep` AS `harga_resep`,`recipeOut`.`banyaknya` AS `banyaknya`,`recipeOut`.`diskon` AS `diskon`,(((100 - `recipeOut`.`diskon`) / 100) * (`recipeOut`.`harga_resep` * `recipeOut`.`banyaknya`)) AS `jumlahHarga` from `recipeOut` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `vwRecipeRqst`
--

DROP TABLE IF EXISTS vwRecipeRqst; 
CREATE ALGORITHM=UNDEFINED DEFINER=`farmacolog`@`localhost` SQL SECURITY DEFINER VIEW `vwRecipeRqst`  AS  select `recipeOut`.`idxdata` AS `idxdata`,`recipeOut`.`trxId` AS `trxId`,`recipeOut`.`kodeObat` AS `kodeObat`,`recipeOut`.`harga_resep` AS `harga_resep`,`recipeOut`.`banyaknya` AS `banyaknya`,`recipeOut`.`aturanPakai` AS `aturanPakai`,`recipeOut`.`diskon` AS `diskon`,ceiling((((100 - `recipeOut`.`diskon`) / 100) * (`recipeOut`.`harga_resep` * `recipeOut`.`banyaknya`))) AS `jumlahHarga` from `recipeOut` ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`kode`);

--
-- Indeks untuk tabel `fabelData`
--
ALTER TABLE `fabelData`
  ADD UNIQUE KEY `nomorFaktur` (`nomorFaktur`);

--
-- Indeks untuk tabel `fabelList`
--
ALTER TABLE `fabelList`
  ADD PRIMARY KEY (`itemIndex`);

--
-- Indeks untuk tabel `formIface`
--
ALTER TABLE `formIface`
  ADD PRIMARY KEY (`urut`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`kode`);

--
-- Indeks untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`nomor`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indeks untuk tabel `optKategoriObat`
--
ALTER TABLE `optKategoriObat`
  ADD PRIMARY KEY (`nomor`);

--
-- Indeks untuk tabel `optSatuanObat`
--
ALTER TABLE `optSatuanObat`
  ADD PRIMARY KEY (`nomor`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pbf`
--
ALTER TABLE `pbf`
  ADD PRIMARY KEY (`pbf_nomor`);

--
-- Indeks untuk tabel `recipeIn`
--
ALTER TABLE `recipeIn`
  ADD PRIMARY KEY (`idxdata`);

--
-- Indeks untuk tabel `recipeOut`
--
ALTER TABLE `recipeOut`
  ADD PRIMARY KEY (`idxdata`);

--
-- Indeks untuk tabel `recipeRqst`
--
ALTER TABLE `recipeRqst`
  ADD PRIMARY KEY (`trxId`);

--
-- Indeks untuk tabel `trxNonResep`
--
ALTER TABLE `trxNonResep`
  ADD PRIMARY KEY (`itemIndex`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dokter`
--
ALTER TABLE `dokter`
  MODIFY `kode` int(2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `fabelList`
--
ALTER TABLE `fabelList`
  MODIFY `itemIndex` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4216;

--
-- AUTO_INCREMENT untuk tabel `formIface`
--
ALTER TABLE `formIface`
  MODIFY `urut` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `kode` int(2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `obat`
--
ALTER TABLE `obat`
  MODIFY `nomor` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4212;

--
-- AUTO_INCREMENT untuk tabel `optKategoriObat`
--
ALTER TABLE `optKategoriObat`
  MODIFY `nomor` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `optSatuanObat`
--
ALTER TABLE `optSatuanObat`
  MODIFY `nomor` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `pbf`
--
ALTER TABLE `pbf`
  MODIFY `pbf_nomor` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `recipeIn`
--
ALTER TABLE `recipeIn`
  MODIFY `idxdata` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `recipeOut`
--
ALTER TABLE `recipeOut`
  MODIFY `idxdata` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `trxNonResep`
--
ALTER TABLE `trxNonResep`
  MODIFY `itemIndex` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
