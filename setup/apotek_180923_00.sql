--
-- Table structure for table `dokter`
--

DROP TABLE IF EXISTS `dokter`;
CREATE TABLE `dokter` (
  `kode` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Table structure for table `fabelData`
--

DROP TABLE IF EXISTS `fabelData`;
CREATE TABLE `fabelData` (
  `nomorFaktur` varchar(20) DEFAULT NULL,
  `namaPBF` varchar(30) DEFAULT NULL,
  `tanggalFaktur` date DEFAULT NULL,
  `tanggalJatuhTempo` date DEFAULT NULL,
  `totalDiskon` int(6) DEFAULT 0,
  UNIQUE KEY `nomorFaktur` (`nomorFaktur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `fabelList`
--

DROP TABLE IF EXISTS `fabelList`;
CREATE TABLE `fabelList` (
  `itemIndex` int(6) NOT NULL AUTO_INCREMENT,
  `nomorFaktur` varchar(30) DEFAULT NULL,
  `kodeObat` varchar(30) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT 0,
  `ppn10` enum('1','0') DEFAULT '1',
  `disc` float DEFAULT 0,
  `kuantitas` int(4) DEFAULT 0,
  `isiPerBox` int(3) DEFAULT 1,
  `satuanJual` varchar(20) DEFAULT 'Tablet',
  PRIMARY KEY (`itemIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DELIMITER ;;
CREATE TRIGGER stokObatTambah AFTER INSERT ON fabelList
FOR EACH ROW
BEGIN
  UPDATE obat SET stock = stock + ( NEW.kuantitas * NEW.isiPerBox ) WHERE kode = NEW.kodeObat;
END; ;;
DELIMITER ;

--
-- Table structure for table `formIface`
--

DROP TABLE IF EXISTS `formIface`;
CREATE TABLE `formIface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file','number') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;


INSERT INTO `formIface` VALUES (1,'profil','Nama Apotik','nama','text','nama',''),(2,'profil','Alamat','alamat','text','alamat',''),(3,'profil','Nomor Telephone','telephone','text','telephone',''),(4,'profil','Gambar Logo','logo','file','logo',''),(5,'karyawan','Kode Karyawan','kode','text','kyw_kode','Readonly'),(6,'karyawan','Nama Karyawan','nama','text','kyw_nama',''),(7,'karyawan','Alamat Rumah','alamat','textarea','kyw_alamat','rows=3'),(8,'karyawan','Nomor HP','no_telp','text','kyw_no_telp','maxlength=12'),(9,'karyawan','Keterangan','ket','textarea','kyw_ket','rows=3'),(10,'dokter','Kode Dokter','kode','text','dok_kode','Readonly'),(11,'dokter','Nama Dokter','nama','text','dok_nama',''),(12,'dokter','Alamat Rumah','alamat','textarea','dok_alamat','rows=3'),(13,'dokter','Nomor HP','no_telp','text','dok_no_telp','maxlength=12'),(14,'dokter','Keterangan','ket','textarea','dok_ket','rows=3'),(15,'obat','Nomor Urut','nomor','text','med_nomor','readonly'),(16,'obat','Kode Obat','kode','text','med_kode',''),(17,'obat','Nama Obat','nama','text','med_nama',''),(18,'obat','Indikasi','indikasi','text','med_indikasi',''),(19,'obat','Kategori','kategori','select','med_kategori','kategori/kategori/optKategoriObat'),(20,'obat','Satuan Jual','satuan','select','med_satuan','satuan/satuan/optSatuanObat'),(21,'obat','Harga Pembelian','harga_beli','number','med_harga_beli',''),(22,'obat','Isi / Box','isiPerBox','number','med_ipb','value=\'0\''),(23,'obat','Harga HV','harga_hv','number','med_harga_hv',''),(24,'obat','Harga Resep','harga_resep','number','med_harga_resep',''),(25,'obat','Stock Obat','stock','number','med_stock',''),(26,'pasien','ID Pasien','id','number','pas_id',''),(27,'pasien','Nama Pasien','nama','text','pas_nama',''),(28,'pasien','Alamat Rumah','alamat','text','pas_alamat',''),(29,'optJenisHarga','Harga Resep','resep','number','resep',''),(30,'optJenisHarga','Harga Non Resep','non_resep','number','non_resep',''),(31,'optJenisHarga','Harga Dokter','dokter','number','dokter',''),(32,'optJenisHarga','Harga Karyawan','karyawan','number','karyawan',''),(33,'optEmbalase','R/','e_r','number','e_r',''),(34,'optEmbalase','L/','e_l','number','e_l',''),(35,'optEmbalase','%','e_p','text','e_p',''),(36,'optKategoriObat','Nomor Urut','nomor','number','kat_nomor','Readonly'),(37,'optKategoriObat','Kategori','kategori','text','kategori',''),(38,'optSatuanObat','Nomor urut','nomor','number','sat_nomor','Readonly'),(39,'optSatuanObat','Satuan','satuan','text','satuan',''),(40,'fabelData','Nomor Faktur','nomorFaktur','text','nomorFaktur',''),(41,'fabelData','Nama PBF / Distributor','namaPBF','text','namaPBF',''),(42,'fabelData','Tanggal Faktur','tanggalFaktur','date','tanggalFaktur',''),(43,'fabelData','Tanggal Jatuh Tempo','tanggalJatuhTempo','date','tanggalJatuhTempo',''),(44,'fabelData','Total Potongan','totalDiskon','number','totalDiskon','value=\"0\"'),(45,'pasien','Kota','kota','text','pas_kota','');


--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan` (
  `kode` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Table structure for table `obat`
--

DROP TABLE IF EXISTS `obat`;
CREATE TABLE `obat` (
  `nomor` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `indikasi` tinytext DEFAULT NULL,
  `kategori` varchar(12) DEFAULT NULL,
  `satuan` varchar(6) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT 0,
  `isiPerBox` int(3) DEFAULT 0,
  `harga_hv` int(6) DEFAULT 0,
  `harga_resep` int(6) DEFAULT 0,
  `stock` int(4) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  UNIQUE KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Table structure for table `optEmbalase`
--

DROP TABLE IF EXISTS `optEmbalase`;
CREATE TABLE `optEmbalase` (
  `e_r` int(5) DEFAULT NULL,
  `e_l` int(5) DEFAULT NULL,
  `e_p` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optEmbalase`
--
INSERT INTO `optEmbalase` VALUES (500,1000,10);


--
-- Table structure for table `optJenisHarga`
--

DROP TABLE IF EXISTS `optJenisHarga`;
CREATE TABLE `optJenisHarga` (
  `resep` float DEFAULT NULL,
  `non_resep` float DEFAULT NULL,
  `dokter` float DEFAULT NULL,
  `karyawan` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optJenisHarga`
--

INSERT INTO `optJenisHarga` VALUES (20,15,0,0);

--
-- Table structure for table `optKategoriObat`
--

DROP TABLE IF EXISTS `optKategoriObat`;
CREATE TABLE `optKategoriObat` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optKategoriObat`
--

INSERT INTO `optKategoriObat` VALUES (1,'Obat Reguler'),(2,'HV'),(3,'Narkotik'),(4,'Psikotropika'),(5,'OOT'),(6,'Sirup'),(7,'Salep '),(8,'Injeksi');

--
-- Table structure for table `optSatuanObat`
--

DROP TABLE IF EXISTS `optSatuanObat`;
CREATE TABLE `optSatuanObat` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optSatuanObat`
--

INSERT INTO `optSatuanObat` VALUES (1,'Tablet'),(2,'Capsul'),(3,'Box'),(4,'Pcs'),(5,'Strip');

--
-- Table structure for table `pasien`
--

DROP TABLE IF EXISTS `pasien`;
CREATE TABLE `pasien` (
  `id` int(5) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `kota` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `profil`
--

DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id` int(1) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `logo` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--
INSERT INTO `profil` VALUES (1,'Alip Farma','Jl. Jend. Sudirman Banjarnegara No. 3 Banjarnegara','0286 594118','img/logo-mini.jpg');


--
-- Table structure for table `recipeIn`
--

DROP TABLE IF EXISTS `recipeIn`;
CREATE TABLE `recipeIn` (
  `idxdata` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `trxId` varchar(6) DEFAULT NULL,
  `kodeObat` varchar(30) DEFAULT NULL,
  `namaObat` varchar(40) DEFAULT NULL,
  `banyaknya` int(2) DEFAULT 1,
  PRIMARY KEY (`idxdata`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Table structure for table `recipeOut`
--

DROP TABLE IF EXISTS `recipeOut`;
CREATE TABLE `recipeOut` (
  `idxdata` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `trxId` varchar(6) DEFAULT NULL,
  `kodeObat` varchar(30) NOT NULL,
  `harga_resep` int(5) DEFAULT 0,
  `banyaknya` int(2) DEFAULT 1,
  `aturanPakai` varchar(40) DEFAULT NULL,
  `diskon` int(2) DEFAULT 0,
  PRIMARY KEY (`idxdata`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DELIMITER ;;
CREATE TRIGGER stokObatKurangRs AFTER INSERT ON recipeOut 
FOR EACH ROW 
BEGIN
   UPDATE obat SET stock = stock - NEW.banyaknya WHERE kode = NEW.kodeObat; 
  END;
;;

CREATE TRIGGER resepWurung BEFORE DELETE ON recipeOut
FOR EACH ROW
BEGIN
	UPDATE obat SET stock = stock + OLD.banyaknya WHERE trxId = OLD.kodeObat;
END ;
;;

DELIMITER ;


--
-- Table structure for table `recipeRqst`
--

DROP TABLE IF EXISTS `recipeRqst`;
CREATE TABLE `recipeRqst` (
  `trxId` varchar(6) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `shift` enum('Pagi','Siang') DEFAULT 'Pagi',
  `idPasien` int(5) DEFAULT NULL,
  `idDokter` int(2) DEFAULT NULL,
  `faktor_r` int(5) DEFAULT 0,
  `faktor_e` int(5) DEFAULT 0,
  `faktor_p` int(5) DEFAULT 0,
  `konsultasi` int(6) DEFAULT 0,
  `diskon` int(6) DEFAULT 0,
  `statusBayar` enum('Terhutang','Terproses','Lunas') DEFAULT 'Terhutang',
  PRIMARY KEY (`trxId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `trxNonResep`
--

DROP TABLE IF EXISTS `trxNonResep`;
CREATE TABLE `trxNonResep` (
  `itemIndex` int(6) NOT NULL AUTO_INCREMENT,
  `trxId` int(6) unsigned zerofill NOT NULL,
  `trxDate` date DEFAULT NULL,
  `trxShift` enum('Pagi','Sore','Lembur') DEFAULT 'Pagi',
  `jenisHarga` enum('non_resep','dokter','karyawan') DEFAULT 'non_resep',
  `idxHarga` float DEFAULT 1,
  `kodeObat` varchar(30) NOT NULL,
  `kuantitas` int(3) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT 0,
  `diskon` int(6) DEFAULT NULL,
  PRIMARY KEY (`itemIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DELIMITER ;;
CREATE TRIGGER stokObatKurangHv AFTER INSERT ON trxNonResep
FOR EACH ROW
BEGIN
  UPDATE obat SET stock = stock - NEW.kuantitas WHERE kode = NEW.kodeObat;
END; ;;

CREATE TRIGGER nonrsWurung BEFORE DELETE ON trxNonResep 
FOR EACH ROW 
BEGIN 
	UPDATE obat SET stock = stock + OLD.kuantitas WHERE kode = OLD.kodeObat; 
END ; ;;
DELIMITER ;

CREATE OR REPLACE VIEW `vwRecipeOut` AS
SELECT `recipeOut`.`idxdata` AS `idxdata`,`recipeOut`.`trxId` AS `trxId`,
`recipeOut`.`kodeObat` AS `kodeObat`,`recipeOut`.`harga_resep` AS `harga_resep`,
`recipeOut`.`banyaknya` AS `banyaknya`,`recipeOut`.`diskon` AS `diskon`,
ceiling((100 - `recipeOut`.`diskon`) / 100 * (`recipeOut`.`harga_resep` * `recipeOut`.`banyaknya`)) AS `jumlahHarga` 
FROM `recipeOut`;
