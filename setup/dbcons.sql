DROP TABLE IF EXISTS profil;
CREATE TABLE profil (
  id int(1) primary key auto_increment,
  nama	varchar	(30),
  alamat	tinytext,
  telephone	varchar	(16),
  logo	tinytext
);

DROP TABLE IF EXISTS karyawan;
CREATE TABLE karyawan(
  kode	int	(2) unsigned zerofill auto_increment,
  nama	varchar	(30),
  alamat	tinytext	,
  no_telp	varchar(12),
  ket	tinytext,
  primary key (kode)
);

DROP TABLE IF EXISTS dokter;
CREATE TABLE dokter(
  kode	int	(2) unsigned zerofill auto_increment,
  nama	varchar	(30),
  alamat	tinytext	,
  no_telp	varchar(12),
  ket	tinytext,
  primary key (kode)
);

DROP TABLE IF EXISTS obat;
CREATE TABLE obat (
  nomor int(4) unsigned auto_increment,
  kode varchar(30) unique,
  nama varchar(40),
  indikasi tinytext,
  kategori varchar(12),
  satuan varchar(6),
  harga_beli int(6) default 0,
  isiPerBox int(3) default 0,
  harga_hv int(6) default 0,
  harga_resep int(6) default 0,
  stock int(4) default 0,
  primary key (nomor)
);

DROP TABLE IF EXISTS pasien;
CREATE TABLE pasien(
  -- xxyyy xx=tahun input yyy=urut
  id int(5) not null,
  nama varchar(30),
  alamat tinytext,
  primary key(id)
);

DROP TABLE IF EXISTS optJenisHarga;
CREATE TABLE optJenisHarga(
  resep float(5),
  non_resep float(5),
  dokter float(5),
  karyawan float(5)
);

-- dummy data default optJenisHarga
INSERT INTO optJenisHarga VALUES (0,0,0,0);

DROP TABLE IF EXISTS optEmbalase;
CREATE TABLE optEmbalase(
  e_r int(5),
  e_l int(5),
  e_p float(5)
);
-- dummy data default optEmbalase
INSERT INTO optEmbalase VALUES (0,0,0);

DROP TABLE IF EXISTS optKategoriObat;
CREATE TABLE optKategoriObat(
  nomor int(2) auto_increment,
  kategori varchar(12),
  primary key (nomor)
);

INSERT INTO optKategoriObat VALUES
(1,'Obat Reguler'),
(2,'HV'),
(3,'Narkotik'),
(4,'Psikotropika'),
(5,'OOT'),
(6,'Sirup'),
(7,'Salep '),
(8,'Injeksi');

DROP TABLE IF EXISTS optSatuanObat;
CREATE TABLE optSatuanObat(
  nomor int(2) auto_increment,
  satuan varchar(6),
  primary key (nomor)
);

INSERT INTO optSatuanObat VALUES
(1,'Tablet'),
(2,'Capsul'),
(3,'Box'),
(4,'Pcs'),
(5,'Strip');

-- pengembanan tabel pembelian
DROP TABLE IF EXISTS fabelData;
CREATE TABLE fabelData(
   nomorFaktur varchar(20) unique,
   namaPBF varchar(30),
   tanggalFaktur date,
   tanggalJatuhTempo date,
   totalDiskon int(6) default 0
 );

DROP TABLE IF EXISTS fabelList;
CREATE TABLE fabelList(
   itemIndex int(6) auto_increment primary key,
   nomorFaktur varchar(20),
   kodeObat varchar(30),
   harga_beli int(6) default 0,
   ppn10 enum('1','0') default '1',
   disc float(5) default 0,
   kuantitas int(4) default 0,
   isiPerBox int(3),
   satuanJual varchar(20) default 'Tablet'
);

DROP TABLE IF EXISTS pbf;
CREATE TABLE pbf(
  pbf_nomor int(3) unsigned zerofill auto_increment,
  pbf_nama varchar(40),
  pbf_telp varchar(12),
  primary key(nomor)
)
CREATE TABLE `formIface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file','number') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
);

INSERT INTO formIface (fgroup,pertanyaan,formName,formId,formType,fparam) VALUES
-- tabel profil
('profil','Nama Apotik','nama','nama','text',''),
('profil','Alamat','alamat','alamat','text',''),
('profil','Nomor Telephone','telephone','telephone','text',''),
('profil','Gambar Logo','logo','logo','file',''),
-- tabel karyawan
('karyawan','Kode Karyawan','kode','kyw_kode','text','Readonly'),
('karyawan','Nama Karyawan','nama','kyw_nama','text',''),
('karyawan','Alamat Rumah','alamat','kyw_alamat','textarea',"rows=3"),
('karyawan','Nomor HP','no_telp','kyw_no_telp','text','maxlength=12'),
('karyawan','Keterangan','ket','kyw_ket','textarea',"rows=3"),
-- tabel dokter
('dokter','Kode Dokter','kode','dok_kode','text','Readonly'),
('dokter','Nama Dokter','nama','dok_nama','text',''),
('dokter','Alamat Rumah','alamat','dok_alamat','textarea',"rows=3"),
('dokter','Nomor HP','no_telp','dok_no_telp','text','maxlength=12'),
('dokter','Keterangan','ket','dok_ket','textarea',"rows=3"),
-- tabel obat
('obat','Nomor Urut','nomor','med_nomor','text',"readonly"),
('obat','Kode Obat','kode','med_kode','text',""),
('obat','Nama Obat','nama','med_nama','text',""),
('obat','Indikasi','indikasi','med_indikasi','text',""),
('obat','Kategori','kategori','med_kategori','select',"kategori/kategori/optKategoriObat"),
('obat','Satuan Jual','satuan','med_satuan','select',"satuan/satuan/optSatuanObat"),
('obat','Harga Pembelian','harga_beli','med_harga_beli','number',""),
('obat','Isi / Box','ipb','med_ipb','number',"value='0'"),
('obat','Harga HV','harga_hv','med_harga_hv','number',""),
('obat','Harga Resep','harga_resep','med_harga_resep','number',""),
('obat','Stock Obat','stock','med_stock','number',""),
-- tabel pasien
-- xxyyy xx=tahun input yyy=urut
('pasien','ID Pasien','id','pas_id','number',""),
('pasien','Nama Pasien','nama','pas_nama','text',""),
('pasien','Alamat Rumah','alamat','pas_alamat','textarea',"rows=3"),
-- tabel optJenisHarga
('optJenisHarga','Harga Resep','resep','resep','number',""),
('optJenisHarga','Harga Non Resep','non_resep','non_resep','number',""),
('optJenisHarga','Harga Dokter','dokter','dokter','number',""),
('optJenisHarga','Harga Karyawan','karyawan','karyawan','number',""),
-- tabel optEmbalase
('optEmbalase','R/','e_r','e_r','number',""),
('optEmbalase','L/','e_l','e_l','number',""),
('optEmbalase','%','e_p','e_p','text',""),
-- tabel optKategoriObat
('optKategoriObat','Nomor Urut','nomor','kat_nomor','number',"Readonly"),
('optKategoriObat','Kategori','kategori','kategori','text',""),
-- tabel optSatuanObat
('optSatuanObat','Nomor urut','nomor','sat_nomor','number',"Readonly"),
('optSatuanObat','Satuan','satuan','satuan','text',""),
-- tabel fakturPembelian-data
('fabelData','Nomor Faktur','nomorFaktur','nomorFaktur','text',''),
('fabelData','Nama PBF / Distributor','namaPBF','namaPBF','text',''),
('fabelData','Tanggal Faktur','tanggalFaktur','tanggalFaktur','date',''),
('fabelData','Tanggal Jatuh Tempo','tanggalJatuhTempo','tanggalJatuhTempo','date',''),
('fabelData','Total Potongan','totalDiskon','totalDiskon','number','value="0"'),
-- tabel PBF
('pbf','Nomor PBF','pbf_nomor','text,','pbf_nomor',''),
('pbf','Nama PBF','pbf_nama','text,','pbf_nama',''),
('pbf','Telephone','pbf_telp','text,','pbf_telp','')
;

DROP TABLE IF EXISTS trxNonResep;
CREATE TABLE trxNonResep(
  itemIndex int(6) auto_increment primary key,
  trxId int(6) unsigned zerofill not null,
  trxDate date,
  trxShift enum('Pagi','Sore','Lembur') default 'pagi',
  jenisHarga enum ('non_resep','dokter','karyawan') default 'non_resep',
  idxHarga float(5) default 1,
  kodeObat varchar(30) not null,
  kuantitas int(3),
  diskon int(6)
);

-- pengembangan trx resep
DROP TABLE IF EXISTS recipeRqst;
CREATE TABLE recipeRqst(
  trxId varchar(6),
  tanggal date,
  shift enum('Pagi','Siang') default 'Pagi',
  idPasien int(5),
  idDokter int(2),
  faktor_r int(5) default 0,
  faktor_e int(5) default 0,
  faktor_p int(5) default 0,
  konsultasi int(6) default 0,
  diskon int(6) default 0,
  statusBayar enum('Terhutang','Terproses','Lunas') default 'Terhutang',
  PRIMARY KEY(trxId)
);

DROP TABLE IF EXISTS recipeIn;
CREATE TABLE recipeIn(
  idxdata int(6) unsigned auto_increment,
  trxId varchar(6),
  kodeObat varchar(30) default null,
  namaObat varchar(40),
  banyaknya int(2) default 1,
  PRIMARY KEY (idxdata)
);

DROP TABLE IF EXISTS recipeOut;
CREATE TABLE recipeOut(
  idxdata int(6) unsigned auto_increment,
  trxId varchar(6),
  kodeObat varchar(30) not null,
  harga_resep int(6) default 0,
  banyaknya int(2) default 1,
  aturanPakai varchar(40),
  diskon int(2) default 0,
  PRIMARY KEY (idxdata)
);

-- views
CREATE OR REPLACE VIEW vwRecipeRqst AS
SELECT recipeOut.*, ceil((100 - diskon)/100 * (harga_resep * banyaknya)) AS jumlahHarga
FROM recipeOut;

-- triggers --
-- trigger tambah stock
delimiter ;;
CREATE TRIGGER stokObatTambah AFTER INSERT ON fabelList
FOR EACH ROW
BEGIN
  UPDATE obat SET stock = stock + ( NEW.kuantitas * NEW.isiPerBox ) WHERE kode = NEW.kodeObat;
END;;

-- trigger kurang stock: resep
CREATE TRIGGER stokObatKurangRs AFTER INSERT ON recipeOut
FOR EACH ROW
BEGIN
  UPDATE obat SET stock = stock - NEW.banyaknya WHERE kode = NEW.kodeObat;
END;;

-- trigger kurang stock: resep
CREATE TRIGGER stokObatKurangHv AFTER INSERT ON trxNonResep
FOR EACH ROW
BEGIN
  UPDATE obat SET stock = stock - NEW.kuantitas WHERE kode = NEW.kodeObat;
END;;

-- trigger tambah stok: resep Batal
CREATE TRIGGER resepWurung BEFORE DELETE ON recipeOut
FOR EACH ROW
BEGIN
UPDATE obat SET stock = stock + OLD.banyaknya WHERE trxId = OLD.kodeObat;
END;;

-- trigger tambah stok: nonResep Batal
CREATE TRIGGER nonrsWurung BEFORE DELETE ON trxNonResep
FOR EACH ROW
BEGIN
UPDATE obat SET stock = stock + OLD.kuantitas WHERE kode = OLD.kodeObat;
END;;

-- triger hapus data faktur pembelian
CREATE TRIGGER batalBeliObat BEFORE DELETE ON fabelData
FOR EACH ROW
BEGIN
DELETE FROM fabelList WHERE nomorFaktur = OLD.nomorFaktur ;
END ;;

-- trigger pengurangan stok batal pembelian
CREATE TRIGGER batalTambahObat BEFORE DELETE ON fabelList
FOR EACH ROW
BEGIN
UPDATE obat SET stock = stock - (OLD.kuantitas * OLD.isiPerBox) WHERE kode = OLD.kodeObat;
END;;

-- trigger update jenis harga obat
CREATE TRIGGER updateHvResep AFTER UPDATE ON optJenisHarga
FOR EACH ROW
BEGIN
UPDATE obat SET harga_resep = (( NEW.resep + 100 ) / 100 * harga_beli ),
harga_hv = (( NEW.non_resep + 100 ) / 100 * harga_beli ) ;
END; ;;
delimiter ;
