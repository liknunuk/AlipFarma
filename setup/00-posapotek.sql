-- MySQL dump 10.16  Distrib 10.2.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: posapotek
-- ------------------------------------------------------
-- Server version	10.2.17-MariaDB-10.2.17+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dokter`
--

DROP TABLE IF EXISTS `dokter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dokter` (
  `kode` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `formIface`
--

DROP TABLE IF EXISTS `formIface`;
CREATE TABLE `formIface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file','number') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `kode` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `ket` tinytext DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obat`
--

DROP TABLE IF EXISTS `obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obat` (
  `nomor` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `indikasi` tinytext DEFAULT NULL,
  `kategori` varchar(12) DEFAULT NULL,
  `satuan` varchar(6) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT 0,
  `isiPerBox` int(3) DEFAULT 0,
  `harga_hv` int(6) DEFAULT 0,
  `harga_resep` int(6) DEFAULT 0,
  `stock` int(4) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  UNIQUE KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `optEmbalase`
--

DROP TABLE IF EXISTS `optEmbalase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optEmbalase` (
  `e_r` int(5) DEFAULT NULL,
  `e_l` int(5) DEFAULT NULL,
  `e_p` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optEmbalase`
--

LOCK TABLES `optEmbalase` WRITE;
/*!40000 ALTER TABLE `optEmbalase` DISABLE KEYS */;
INSERT INTO `optEmbalase` VALUES (500,1000,10);
/*!40000 ALTER TABLE `optEmbalase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optJenisHarga`
--

DROP TABLE IF EXISTS `optJenisHarga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optJenisHarga` (
  `resep` float DEFAULT NULL,
  `non_resep` float DEFAULT NULL,
  `dokter` float DEFAULT NULL,
  `karyawan` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optJenisHarga`
--

LOCK TABLES `optJenisHarga` WRITE;
/*!40000 ALTER TABLE `optJenisHarga` DISABLE KEYS */;
INSERT INTO `optJenisHarga` VALUES (20,15,0,0);
/*!40000 ALTER TABLE `optJenisHarga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optKategoriObat`
--

DROP TABLE IF EXISTS `optKategoriObat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optKategoriObat` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optKategoriObat`
--

LOCK TABLES `optKategoriObat` WRITE;
/*!40000 ALTER TABLE `optKategoriObat` DISABLE KEYS */;
INSERT INTO `optKategoriObat` VALUES (1,'Obat Reguler'),(2,'HV'),(3,'Narkotik'),(4,'Psikotropika'),(5,'OOT'),(6,'Sirup'),(7,'Salep '),(8,'Injeksi');
/*!40000 ALTER TABLE `optKategoriObat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optSatuanObat`
--

DROP TABLE IF EXISTS `optSatuanObat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optSatuanObat` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optSatuanObat`
--

LOCK TABLES `optSatuanObat` WRITE;
/*!40000 ALTER TABLE `optSatuanObat` DISABLE KEYS */;
INSERT INTO `optSatuanObat` VALUES (1,'Tablet'),(2,'Capsul'),(3,'Box'),(4,'Pcs'),(5,'Strip');
/*!40000 ALTER TABLE `optSatuanObat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pasien`
--

DROP TABLE IF EXISTS `pasien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pasien` (
  `id` int(5) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `kota` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pembelian`
--

DROP TABLE IF EXISTS `pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembelian` (
  `itemIndex` int(6) NOT NULL AUTO_INCREMENT,
  `nomorFaktur` varchar(20) DEFAULT NULL,
  `namaPBF` varchar(30) DEFAULT NULL,
  `tanggalFaktur` date DEFAULT NULL,
  `tanggalJatuhTempo` date DEFAULT NULL,
  `kodeObat` varchar(30) DEFAULT NULL,
  `harga_beli` int(6) DEFAULT NULL,
  `kuantitas` int(4) DEFAULT NULL,
  PRIMARY KEY (`itemIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profil`
--

DROP TABLE IF EXISTS `profil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profil` (
  `id` int(1) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `logo` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profil`
--

LOCK TABLES `profil` WRITE;
/*!40000 ALTER TABLE `profil` DISABLE KEYS */;
INSERT INTO `profil` VALUES (1,'Apotik Alip','Jl. Jend. Sudirman Banjarnegara','02865980777','img/logo-mini.jpg');
/*!40000 ALTER TABLE `profil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trxNonResep`
--

DROP TABLE IF EXISTS `trxNonResep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trxNonResep` (
  `itemIndex` int(6) NOT NULL AUTO_INCREMENT,
  `trxId` int(8) NOT NULL,
  `trxDate` date DEFAULT NULL,
  `trxShift` enum('Pagi','Sore','Lembur') DEFAULT 'Pagi',
  `jenisHarga` enum('non_resep','dokter','karyawan') DEFAULT 'non_resep',
  `idxHarga` float DEFAULT 1,
  `kodeObat` varchar(30) NOT NULL,
  `kuantitas` int(3) DEFAULT NULL,
  `diskon` int(6) DEFAULT NULL,
  PRIMARY KEY (`itemIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `trxResep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trxResep` (
  `itemIndex` int(6) NOT NULL AUTO_INCREMENT,
  `trxId` int(8) NOT NULL,
  `trxDate` date DEFAULT NULL,
  `trxShift` enum('Pagi','Sore','Lembur') DEFAULT 'Pagi',
  `dokter` varchar(30) DEFAULT NULL,
  `pasienNama` varchar(30) DEFAULT NULL,
  `pasienAlamat` tinytext DEFAULT NULL,
  `pasienKota` varchar(30) DEFAULT 'Banjarnegara',
  `kodeObat` varchar(30) DEFAULT NULL,
  `kuantitas` float DEFAULT 1,
  `aturanPakai` varchar(40) DEFAULT NULL,
  `diskon` int(6) DEFAULT NULL,
  `e_r` int(5) DEFAULT NULL,
  `e_p` int(5) DEFAULT NULL,
  PRIMARY KEY (`itemIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-27 11:59:37
