<div class='page-banner'>
  <p>PEMROSESAN RESEP</p>
</div>
<div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
</div>
<div class="row">
  <div class="col-lg-3">
    <b>DAFTAR RESEP DITERIMA</b>
    <div class="list-group" id="waitingList">

    </div>
    <button onClick=loadListRecipeIn() class="btn btn-primary btn-sm">Refresh</button>
  </div>
  <div class="col-lg-3">
    <b>RINCIAN RESEP <span id="recipeId"></span></b>
    <ul class="list-group" id="recipeItems">

    </ul>
  </div>
  <div class="col-lg-3">
    <b>PROSES RESEP</b>
    <div class="form-group">
      <label>Nama Barang/Obat</label>
      <input class="form-control" type="text" id="med_nama" />
      <div class="kode-float" id="obatList"></div>
    </div>
    <div class="form-group" style="display:none;">
      <label>Kode Barang/Obat</label>
      <input class="form-control" id="med_kodeObat" type="text" />
    </div>
    <div class="form-group">
      <label>Bayaknya</label>
      <input class="form-control" type="number" id="qty" />
    </div>
    <div class="form-group">
      <label>Aturan Pakai</label>
      <input class="form-control" id="med_dosis" list="dosisList" />
      <datalist id="dosisList">
        <option value='1x1, Pagi' >
        <option value='1x1, Siang' >
        <option value='1x1, Sore' >
        <option value='1x1, Malam' >
        <option value='2x1, Pagi - Siang' >
        <option value='2x1, Pagi - Sore' >
        <option value='2x1, Pagi - Malam' >
        <option value='2x1, Siang - Sore' >
        <option value='2x1, Siang - Malam' >
        <option value='2x1, Sore - Malam' >
        <option value='3x1, Pagi-Siang-Malam' >
        <option value='1x2, Pagi' >
        <option value='1x2, Siang' >
        <option value='1x2, Sore' >
        <option value='1x2, Malam' >
        <option value='2x2, Pagi - Siang' >
        <option value='2x2, Pagi - Sore' >
        <option value='2x2, Pagi - Malam' >
        <option value='2x2, Siang - Sore' >
        <option value='2x2, Siang - Malam' >
        <option value='2x2, Sore - Malam' >
        <option value='3x2, Pagi-Siang-Malam' >

      </datalist>
    </div>
    <div class="form-group">
      <label>Harga</label>
      <input class="form-control" id="med_price" type="number" />
    </div>
    <div class="form-group">
      <label>Diskon (%)</label>
      <input class="form-control font-small" id="rsp_diskon" type="number"
      value="0" onkeypress=saveRecipeOut(event) />
    </div>

  </div>

  <div class="col-lg-3">
    <b>TAGIHAN RESEP</b>

    <div class="list-group" id="recipeBill">

    </div>
    <div id="rcpResponse"></div>
    <div>
      <table class="table">
        <tr>
          <td>Jumlah Harga</td>
          <td>
            <input class="form-control" id="rcpTotalPrice" type="number" style="text-align:right;"/>
          </td>
        </tr>
        <tr>
          <td>R/</td>
          <td>
            <input class="form-control" id="rcp_r" type="number" style="text-align:right;" />
          </td>
        </tr>
        <tr>
          <td>+E</td>
          <td>
            <input class="form-control" id="rcp_e" type="number" style="text-align:right;" />
          </td>
        </tr>
        <tr>
          <td>%</td>
          <td>
            <input class="form-control" id="rcp_p" type="number" style="text-align:right;" />
          </td>
        </tr>
        <tr>
          <td>Konsultasi</td>
          <td>
            <input class="form-control" id="rcp_konsultasi" type="number"
            style="text-align:right;" onkeypress=updateRqst(event) />
          </td>
        </tr>
      </table>
    </div>
    <li class='list-group-item'>
      <button class='btn btn-info btn-sm' onclick=refreshTagihan()>Refresh</button>
      <button class='btn btn-primary btn-sm' onclick=cetakLabel()>Label Obat</button>
    </li>
  </div>

</div>
<script>
$(document).ready(function(){
  loadListRecipeIn();
});

function loadListRecipeIn(){
  $.getJSON("ajax/recipeInData.php?sr=Terhutang",function(waitingList){
    $("#waitingList li").remove();
    $.each(waitingList,function(i,data){
      $("#waitingList").append(
        "<li class='list-group-item'>"+
          "<a href='javascript:void(0)' onclick=showItems('"+data.trxId+"')>"+data.trxId+"</a>"+
          " - "+data.namaDokter +
          "<span class='badge'>"+data.item+"</span>"+
          "<br />"+data.nama+
        "</li>"
      )
    });
  });
}

function showItems(trxId){
  $("#recipeId").html(trxId);
  $.getJSON("ajax/recipeInItems.php?id="+trxId,function(items){
    $("#recipeItems li").remove();
    $.each(items,function(i,data){
      $("#recipeItems").append(
        "<li class='list-group-item'>"+
        data.kodeObat+"<br />"+
        data.namaObat+"<span class='badge'>"+data.banyaknya+"</span>"+
        "</li>"
      );
    });
  });
}

function saveRecipeOut(e){
  if(e.which == 13){
    $.post("saveRecipeOut.php?mod=ins",{
      trxId         : $("#recipeId").html(),
      kodeObat      : $("#med_kodeObat").val(),
      harga_resep   : $("#med_price").val(),
      banyaknya     : $("#qty").val(),
      aturanPakai   : $("#med_dosis").val(),
      diskon        : $("#rsp_diskon").val()
    },function(response){
      $("#rcpResponse").html(response);
      $("#med_kodeObat").val('');
      $("#med_price").val('');
      $("#qty").val('');
      $("#rsp_diskon").val('0');
      $("#med_nama").val('');
      $("#med_nama").focus();
    })
    loadTagihan($("#recipeId").html());
  }
}

function loadTagihan(trxId){
  $.getJSON("ajax/recipeBill.php?id="+trxId, function(bill){
    //console.log(bill);
    $("#recipeBill li").remove();
    $.each(bill.rincian,function(i,data){
      console.log(data);
      $("#recipeBill").append(
        "<li class='list-group-item' style='padding: 0px 5px;'>"+
        "<p style='margin:0; padding:0;'>"+data.nama+"</p>"+
        "<p style='margin:0; padding:0;'>"+data.banyaknya+" x "+data.harga+" - disk. "+data.diskon+"%"+
        "<span style='float:right'>"+data.jumlah+"</span></p>"+
        "</li>"
      )
    });
    var fak_p = parseInt(bill.fak_p)/100 * parseInt(bill.totalTagihan);
    fak_p=Math.ceil(fak_p/100)*100;
    $("#rcpTotalPrice").val(bill.totalTagihan);
    $("#rcp_r").val(bill.fak_r);
    $("#rcp_e").val(bill.fak_l);
    $("#rcp_p").val(fak_p);
  });
  $("#rcpResponse").fadeOut('3000');
}

function refreshTagihan(){
  var trxId = $("#recipeId").html();
  loadTagihan(trxId);
}

function updateRqst(e){
  if(e.which == 13){
    $.post("saveResepData.php",{
      mode : 'chg',
      trxId: $("#recipeId").html(),
      fakr : $("#rcp_r").val(),
      fake : $("#rcp_e").val(),
      fakp : $("#rcp_p").val(),
      cnsl : $("#rcp_konsultasi").val(),
      stby : 'Terproses'
    },function(response){
      $("#rcpResponse").html(response);
      location.reload();
    });
    cetakLabel();
  }
}

function cetakLabel(){
  var rid=$("#recipeId").html();
  window.open("print.php?obj=label&id="+rid);
}
</script>
