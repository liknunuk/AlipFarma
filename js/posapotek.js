
$(document).ready(function(){
  //baca profil
  $.getJSON("profil.php",function(pdata){
    //console.log(pdata);
    $("#namaApotik").html(pdata.nama);
    $("#brandApotik").html(pdata.nama);
    $("#alamatApotik").html(pdata.alamat);
    $("#telpApotik").html(pdata.telephone);
    $("#logoApotik").html("<img src='"+pdata.logo+"' height='60px' />");
  });

  $("#pas_id").click( function(){
    $("#pas_id").val('');
      $.ajax({
        url     : 'reference.php?key=pas_id',
        success : function(pas_id){
          $("#pas_id").val(pas_id);
        }
      });
  });

  $("#kyw_kode").click( function(){
    $("#kyw_kode").val('');
      $.ajax({
        url     : 'reference.php?key=kar_id',
        success : function(pas_id){
          $("#kyw_kode").val(pas_id);
        }
      });
  });

  $("#dok_kode").click( function(){
    $("#dok_kode").val('');
      $.ajax({
        url     : 'reference.php?key=dok_id',
        success : function(pas_id){
          $("#dok_kode").val(pas_id);
        }
      });
  });

  /* form pembelian obat */

  $("#namaPBF").keyup( function(){
    var pbf = $("#namaPBF").val();
    if( pbf.length >=5 ){
      $.ajax({
        url     : "ajax/daftarPBF.php?npbf="+pbf,
        success : function(dpbf){
          $("#pbfList").html(dpbf)
        }
      });
    }
  });

  /// penanganan taggal
  $( ".comoTanggal" ).datepicker({
           dateFormat : 'dd/mm/yy',
           changeMonth : true,
           changeYear : true,
           yearRange: '-100y:c+nn',
           maxDate: '+20d'
       });

  $("#tglFaktur").change( function(){
    let bd,sd,tgl,t,b,h,dpl;
    bd  = $(this).val();
    tgl = bd.split('/');
    t   = tgl[2];
    b   = parseInt( tgl[1] - 1);
    h   = tgl[0];
    sd = new Date(t,b,h);
    //alert( sd );
    dpl= duapekanlagi(sd,14);
    $("#tglOverDue").val( dpl );
  });

  $("#hargaBeli").focus( function(){
    let kdObat = $("#kdObat").val();
    $.getJSON('ajax/tukukeri.php?id='+kdObat,function(data){
        $("#hargaBeli").val(data.hargaNett)
      });
  });

  $("#fkbData-save").click( function(){
    $.post("ajax/simpanFakturObat.php?modus=baru",{
      target  : 'fbData',
      nomor   : $("#nomorFaktur").val(),
      namapbf : $("#namaPBF").val(),
      tanggal : $("#tglFaktur").val(),
      jatempo : $("#tglOverDue").val(),
      diskon  : $("#fakturDiscount").val()
    },function(response){
      //console.log(response);

      $("#fkbData-save").prop("disabled",true);
      $("#fakturData").css("display","none");
      $("#fakturInfo").css("visibility","visible");
      $(".fabelList").show();

    });
    //pindah data faktur info

    $("#fi_nofak").html( $("#nomorFaktur").val() );
    $("#fi_nmPBF").html( $("#namaPBF option:selected").text() );
    $("#fi_tgfak").html( $("#tglFaktur").val() );
    $("#fi_tgdue").html( $("#tglOverDue").val() );
    $("#kdObat").focus();

  });

  $("#fkbData-update").click( function(){
    $.post("ajax/simpanFakturObat.php?modus=ubah",{
      target  : 'fbData',
      nomor   : $("#nomorFaktur").val(),
      namapbf : $("#namaPBF").val(),
      tanggal : $("#tglFaktur").val(),
      jatempo : $("#tglOverDue").val(),
      diskon  : $("#fakturDiscount").val()
    },function(response){
      console.log(response);
      $("#fabelData-modal").modal('toggle');
      location.reload();
    });
  });

  $("#fkbList-save").click( function(){
    $.post("ajax/simpanFakturObat.php?modus=baru",{
      target  : 'fbList',
      faktur  : $("#nomorFaktur").val(),
      kdObat  : $("#kdObat").val(),
      banyak  : $("#qty").val(),
      regane  : $("#hargaBeli").val(),
      pajake  : $("#ppnBeli").val(),
      diskon  : $("#discObat").val(),
      perbox  : $("#isibox").val(),
      sajual  : $("#satjual").val()
    },function(response){
      // console.log(response);
      showFaktur($("#nomorFaktur").val(),'1');
      $('#kdObat').val('');
      $('#qty').val('');
      $('#hargaBeli').val('');
      $('#discObat').val('');
      $('#kdObat').focus();
    });
  });


  $("#fkbList-fres").click( function(){
      showFaktur($("#nomorFaktur").val(),'1');
  });

  $("#fkbList-update").click( function(){
    $.post("ajax/simpanFakturObat.php?modus=ubah",{
      target  : 'fbList',
      itmidx  : $("#itemIndex").val(),
      faktur  : $("#nomorFaktur").val(),
      kdObat  : $("#kdObat").val(),
      banyak  : $("#qty").val(),
      isibox  : $("#ipb").val(),
      sajual  : $("#satjual").val(),
      regane  : $("#hargaBeli").val(),
      pajake  : $("#ppnBeli").val(),
      diskon  : $("#discObat").val()
    },function(response){
      console.log(response);
      location.reload();
    });
  });

  /* form katalog obat */

  $("#med_kode").keypress( function(e){
    geserTo(e,'med_nama');
  });

  $("#med_nama").keypress( function( e ){
    geserTo(e,'med_indikasi');
  });

  $("#med_indikasi").keypress( function( e ){
    geserTo(e,'med_kategori');
  });

  $("#med_kategori").keypress( function( e ){
    geserTo(e,'med_satuan');
  });

  $("#med_satuan").keypress( function( e ){
    geserTo(e,'med_harga_beli');
  });

  $("#med_harga_beli").keypress( function( e ){
    geserTo(e,'med_ipb');
  });

  $("#med_ipb").keypress( function( e ){
    geserTo(e,'med_harga_hv');
  });

  $("#med_harga_hv").keypress( function( e ){
    geserTo(e,'med_harga_resep');
  });

  $("#med_harga_resep").keypress( function( e ){
    geserTo(e,'med_stock');
  });

  /* form katalog obat */

  /* pencaria data */

  $("#medSrcBox").keyup( function(){
    $.ajax({
      url : "./ajax/allDataSearch.php?obj=obat&nama="+$(this).val(),
      success : function( data ){
        $("#dataHolder").html( data );
      }
    });
  });

  /* pencaria data */

  /* form pembelian obat */

  $("#med_harga_beli").blur( function(){
    $.getJSON(
      './ajax/dataharga.php',
      function(harga){
        console.log("harga",harga);
        var hv = ( parseInt(harga[0].non_resep) + 100 ) / 100 * parseInt($("#med_harga_beli").val());
        var hr = ( parseInt(harga[0].resep) + 100 ) / 100 * parseInt($("#med_harga_beli").val());

        var chv = round50(hv);
        var chr = round50(hr);
        var r50 = chv+','+chr;
        console.log('ceil 50:',r50);
        $("#med_harga_hv").val(Math.ceil(hv));
        $("#med_harga_resep").val(Math.ceil(hr));
      }
    );
    $("#med_ipg").focus();
  });

  $("#med_ipg").blur( function(){
    $("#med_stock").focus();
  });

  $("#fakturseek").click(function(){
    $.post('./ajax/fakturByTglPbf.php?mod=report',
    {
      tg0 : $("#tg0").val(),
      tg1 : $("#tg1").val(),
      pbf : $("#pbf").val()
    },function(faktur){
      //console.log(faktur);
      $("#fakRecap").html(faktur);
    });
  });

  /*
  $("#dokter").keyup( function(){
    var nd = $("#dokter").val();
    if (nd.length < 3){
      $("#dokterList").hide();
      $("#dokterList").css({"display":"none"});
    }else{
      $.ajax({
        url     : './ajax/dokterList.php?nd='+nd,
        success : function(dokters){
          $("#dokterList").show();
          $("#dokterList").html(dokters);
        }
      });
    }

  }); */

  $("#pasienNama").keyup( function(){
    var np = $("#pasienNama").val();
    if (np.length < 3 ){
      $("#pasienList").hide();
      $("#pasienList").css({"display":"none"});
    }else{
      $.ajax({
        url     : './ajax/pasienList.php?np='+np,
        success : function(pasiens){
          $("#pasienList").show();
          $("#pasienList").html(pasiens);
        }
      });
    }

  });

  $("#rqstSubmit").click( function(){
    $("#rcp2_trxId").html( $("#trxId").val() );
    let tg = $("#trxDate").val().split('-');
    $("#rcp2_trxTg").html( tg[2]+'/'+tg[1]+'/'+tg[0] );
    $("#rcp2_shift").html( $("#trxShift").val() );
    $("#rcp2_doktr").html( $("#dokter option:selected").text() );
    $("#rcp2_psien").html( $("#pasienNama").val() );
    $("#rcp2_almat").html( $("#pasienAlamat").val() );
    $("#rcp2_pkota").html( $("#pasienKota").val() );
    $("#rcp_sc1").hide();
    $("#rcp_sc2").show();
    
    $.post("saveResepData.php",{
      mode    : 'ins',
      trxId   : $("#trxId").val(),
      tanggal : $("#trxDate").val(),
      shift   : $("#trxShift").val(),
      idPasien: $("#pasienId").val(),
      idDokter: $("#dokter").val()
    },function(responses){
      $("#rcpResponse").html(responses);
    });
    
  });

  $("#med_nama").keyup( function(){
    var no = $("#med_nama").val();
    if( no.length < 3 ){
      $("#obatList").hide();
      $("#obatList").html('');
    }else{
      $("#obatList").show();
      $.getJSON('./ajax/obatResep.php?no='+no,
        function(reseps){
          //console.log(reseps);
          $("#obatList").html('');
          $("#obatList").append("<p onClick=zeroCode()>Tanpa Kode</p>");
          $.each(reseps,function(i,data){
            $("#obatList").append(
              "<p onClick=setResep(this.innerHTML,'"+data.kode+"',"+data.harga+
              ","+data.stock+")>"+data.nama+"</p>");
          });
      });
    }
  });

  /* simpan recipeOut */
  $("#med_diskon").keypress( function(e){
    if( e.which == 13 ){
      e.preventDefault();
      $.post("saveRecipeOut.php?mod=ins",{
        trxId         : $("#trxId").val(),
        kodeObat      : $("#med_kodeObat").val(),
        banyaknya     : $("#qty").val(),
        aturanPakai   : $("#med_dosis").val(),
        diskon        : $("#med_diskon").val()
      },function(response){
        $("#rcpResponse").html(response);
        $("#med_nama").val('');
        $("#qty").val('');
        $("#med_dosis").val('');
        $("#med_diskon").val(0);
        showRecipeOut( $("#rcp2_trxId").html() );
      });
    }
  })
  /* simpan recipeOut */

  $("#rsp_diskon").keypress( function(event){
    if( event.which == 13 ){
      event.preventDefault();
      var trxId = $("#trxId").val();
      var trxDate = $("#trxDate").val();
      var trxShift = $("#trxShift").val();
      var dokter = $("#dokter").val();
      var pasienNama = $("#pasienNama").val();
      var pasienAlamat = $("#pasienAlamat").val();
      var pasienKota = $("#pasienKota").val();
      var kodeObat = $("#med_kodeObat").val();
      var kuantitas = $("#qty").val();
      var med_price = $("#med_price").val();
      var aturanPakai = $("#med_dosis").val();
      var diskon = $("#rsp_diskon").val();
      var e_r = $("#e_r").val();
      var e_p = $("#e_p").val();

      var trxData = {trxId,trxDate,trxShift,dokter,pasienNama,pasienKota,pasienAlamat,
                    kodeObat,kuantitas,med_price,aturanPakai,diskon,e_r,e_p};
      $.post('saveTrxResep.php',{ data: trxData }, function(responses){
        //console.log(responses);
        $("#med_nama").val('');
        $("#qty").val('');
        $("#rsp_diskon").val('');
        $("#med_dosis").val('');
        $("#med_kodeObat").val('');
        showNota(trxId);
        $("#med_nama").focus();
      });

    }
  });

  $("#recipeDone").click( function(){
    let f_r, f_e, kon,trxid;
    f_r = $("#f_r").val();
    f_e = $("#f_e").val();
    kon = $("#konsultasi").val();
    trx = $("#trxId").val();
    $.post("ajax/updateRecipeRqst.php",{
      fakr : f_r,
      fake : f_e,
      kons : kon,
      trxid: trx
    },function(response){
      console.log(response);
      location.reload();
    })
  });
});

  function delco(tabel,id){
    var yakin = confirm('Data Akan Dihapus Permanen');
    if(yakin == true ){
      $.post('trash.php',{
        obj : tabel,
        oid : id
      },function(responses){
        //console.log('respon',responses);
        location.reload();
      });
    }
  }

  function showFaktur(nf,em){
    $.ajax({
      url     : './ajax/obatByFaktur.php?nf='+nf+"&em="+em,
      success : function(faktur){
        $("#dataFaktur").html(faktur);
      }
    });
  }

  function setResep(nama,kode,harga,stok){
    $("#med_nama").val(nama);
    $("#med_kodeObat").val(kode);
    $("#med_price").val(harga);
    $("#qty").attr('placeholder','stok :'+stok);
    $("#obatList").html('');
    $("#obatList").css({"display":"none"});
    $("#qty").focus();
  }

  function showNota(trxId){
    $.ajax({
      url     : './ajax/notaResep.php?id='+trxId,
      success : function(nota){
        $("#dataResep").html(nota);
      }
    });
  }

  function round50(x){
    return Math.ceil(x/50)*50;
  }

  function round100(x){
    return Math.ceil(x/100)*100;
  }

  function zeroCode(){
    $("#med_kodeObat").val('000');
    $("#qty").focus();
  }

  function geserTo(e,nxel){
    if( e.which == 13 ){
      e.preventDefault();
      $("#"+nxel).focus();
    }
  }

  function duapekanlagi(startDate,numberOfDays=14)
	{
		var returnDate = new Date(
								startDate.getFullYear(),
								startDate.getMonth(),
								startDate.getDate()+numberOfDays,
								startDate.getHours(),
								startDate.getMinutes(),
								startDate.getSeconds());
		/* return returnDate; */
    let ny = returnDate.getFullYear();
    let nm = returnDate.getMonth();
    let nd = returnDate.getDate();
    nm+=1;
    return nd.pad(2)+'/'+nm.pad(2)+'/'+ny;

	}

  Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

  function editFabelList(idx,nf){
    $.post('ajax/obatBatal.php',{ itemIndex: idx },function(response){
      console.log(response)
    });
    showFaktur(nf);
  }
