<div class='table-banner'>
  <span class='table-title'>
    Pembelian Bulan Ini
  </span>
  <span class=' btn-right'>
    <a class='btn btn-primary' href='./?show=trxf&obj=beli'>
      <i class='fa fa-plus'></i>&nbsp;Data Pembelian
    </a>
  </span>

</div>
<div class="table-responsive">
  <table class="table table-sm">
     <thead class="thead-dark">
      <tr>
        <th>Nomor</th>
        <th>No. Faktur</th>
        <th>Nama PBF</th>
        <th>Tanggal Faktur</th>
        <th>Jatuh Tempo</th>
        <th>Total Diskon</th>
        <th>Kontrol</th>
      </tr>
    </thead>
    <tbody id="dafak">

    </tbody>

  </table>
</div>
<!-- modal form fabelData -->
<!-- Modal -->
<div id="fabelData-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Data Faktur</h4>
      </div>
      <div class="modal-body">
        <!-- form fabeldata-->
        <div class="form-group">
          <label>Nomor Faktur</label>
          <input class='form-control' type='text' id='nomorFaktur' readonly />
        </div>
        <div class="form-group">
          <label>Nama PBF</label>
          <input class='form-control' type='text' id='namaPBF' />
          <div id="pbfList"></div>
        </div>
        <div class="form-group">
          <label>Tanggal Faktur</label>
          <input class='form-control' type='date' id='tglFaktur'/>
        </div>
        <div class="form-group">
          <label>Jatuh Tempo</label>
          <input class='form-control' type='date' id='tglOverDue' />
        </div>
        <div class="form-group">
          <label>Total Potongan</label>
          <input class='form-control' type='number' id='fakturDiscount' />
        </div>
        <div class="form-group">
          <br />
          <button class="btn btn-primary" id="fkbData-update">Simpan</button>
        </div>
        <!-- form fabeldata-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
$.ajax({
  url     : "ajax/pembelianBulanIni.php",
  success : function(faktur){
    $("#dafak").html(faktur);
  }
});

function editFabelData(nf){
  $("#fabelData-modal").modal('show');
  $.getJSON("ajax/dataFaktur.php?nf="+nf, function(df){
    console.log(df);
    $("#nomorFaktur").val(df.nomorFaktur);
    $("#namaPBF").val(df.namaPBF);
    $("#tglFaktur").val(df.tanggalFaktur);
    $("#tglOverDue").val(df.tanggalJatuhTempo);
    $("#fakturDiscount").val(df.totalDiskon);
  });
}
</script>
