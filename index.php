<?php
//error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('Asia/Jakarta');
?>
<html>
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script-->
        <!-- bootstrap cdn -->
          <!-- Latest compiled and minified CSS -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <!-- jQuery library -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <!-- Latest compiled JavaScript -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- bootstrap cdn -->
        <!-- bootstrap ui -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- bootstrap ui -->

        <!--
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        -->

        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/posapotek.js"></script>
        <!--script type="text/javascript" src="js/datapokok.js"></script-->
        <!--link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css"-->
        <style>
        .page-header h1{margin:0; font-size: 20px;}
        .page-header h3{margin:0; font-size: 16px;}
        .page-header p{margin:0;}
        #mainContent {min-height: 470px;}
        .table-banner { height: 45px; padding: 5px; background-color: #BBB;
        margin-bottom: 10px;}
        .table-title {font-size: 20px; font-weight: bold;}
        .table-small > tr > td{ padding: 2px 15px;}
        .btn-right { float: right;}
        .kode-float {position: absolute; display: none;  width: 100%;
            min-height: 200px; background-color: rgba(255,255,255,0.9); z-index: 2;
            cursor: pointer; }
        .ra{ text-align: right; padding-right: 20px;}
        label.resep {width: 120px;}
        .vrapet { margin: 1px 0px;}
        p.docName { font-size: 12pt; cursor: pointer; margin:0}
        p.docAddr { font-size: 10pt; font-family: monospace; text-align: right; margin:0;}
        div.form-jual > div.col-sm-3 { font-size: 10pt;}
        .font-small { font-size: 10pt;}
        table.table-sm > tbody > tr > td { font-size: 12pt; padding: 5px 10px;}
        input.ra {width: 75px;}
        .page-banner {text-align: center; font-size: 20px; background-color: #BBB;
          padding: 5px; margin-bottom: 10px;}
        .thead-dark > tr { background: #555; color: #DDD; text-align: center;}
        .thead-dark > tr >th { vertical-align: middle;}
        th { text-align: center; }
        table>tbody>tr>td>input { margin: 0; width: 90px; text-align: right; padding: 0;}
        input[type=number]{ text-align: right; }
        </style>
        <title>Apotek POS</title>
    </head>
    <body>
      <!--div class="row page-header" style="padding: 0px 50px; margin-top: 10px;">
        <div class='col-sm-2' style="text-align:center;" id='logoApotik'>
          <img src="img/asclepius-rod.jpeg" height='60px' />
        </div>
        <di class='col-sm-10'>
          <h1 id='namaApotik'>Apotik ...</h1>
          <h3 id="alamatApotik">Jl ...</h3>
          <p id="telpApotik">12345</p>
        </di>
      </div-->
      <div class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">
              <span id="brandApotik"></span>
            </a>
          </div>
          <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">DATA <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="./?show=form&obj=profil&mod=u&nh=1">Profil</a></li>
                  <li><a href="./?show=data&obj=karyawan&mod=i&nh=1">Karyawan</a></li>
                  <li><a href="./?show=data&obj=dokter&mod=i&nh=1">Dokter</a></li>
                  <li><a href="./?show=data&obj=pasien&mod=i&nh=1">Pasien</a></li>
                  <li><a href="./?show=data&obj=pbf&mod=i&nh=1">P B F</a></li>
                  <li class='divider'></li>
                  <li><a href="./?show=data&obj=obat&mod=i&nh=1">Obat</a></li>
                  <li><a href="./?show=opdt&obj=jobat&mod=u&nh=1">Jenis Harga</a></li>
                  <li><a href="./?show=opdt&obj=embal&mod=u&nh=1">Embalase</a></li>
                  <li><a href="./?show=data&obj=katob&mod=i&nh=1">Kategori</a></li>
                  <li><a href="./?show=data&obj=unito&mod=i&nh=1">Satuan</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  PEMBELIAN <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="./?show=trxd&obj=fbeli">Data Pembelian</a></li>
                  <li><a href="./?show=trxd&obj=rbeli">Rekap Pembelian</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  PENJUALAN <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <!--li><a href="./?show=trxf&obj=resep1">Terima Resep</a></li>
                  <li><a href="./?show=trxf&obj=resep2">Proses Resep</a></li-->
                  <li><a href="./?show=trxf&obj=resep">Terima Resep</a></li>
                  <li><a href="./?show=trxf&obj=resep3">Pembayaran Resep</a></li>
                  <li><a href="./?show=trxf&obj=nonresep">Non Resep</a></li>
                  <li><a href="./?show=trxd&obj=resep">Rekap Resep</a></li>
                  <li><a href="./?show=trxd&obj=nonresep">Rekap Non Resep</a></li>
                </ul>
              </li>

            </ul>
          </div>
        </div>
      </div>


        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" id="mainContent">
              <?php
              include("content.php");
              ?>
            </div>
        </div>
      </div>
      <footer class="section" style="border: 0px solid blue;">
        <div class="row page-header" style="padding: 0px 50px; margin-top: 10px;">
          <div class='col-sm-2' style="text-align:center;" id='logoApotik'>
            <img src="img/asclepius-rod.jpeg" height='60px' />
          </div>
          <di class='col-sm-10'>
            <h1 id='namaApotik'>Apotik ...</h1>
            <h3 id="alamatApotik">Jl ...</h3>
            <p id="telpApotik">12345</p>
          </di>
        </div>
      </footer>

     </body>
     </html>
