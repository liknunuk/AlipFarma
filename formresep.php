<div class="row">
  <div class="col-sm-12">
    <div class='page-banner'>
      <p>PENJUALAN RESEP</p>
    </div>
    <div class='bg-danger' style='padding: 15px; font-weight: bold;'>
    Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-4 form-horizontal">
  
    <div style='background-color:#DDD; font-weight:bold; margin:2 0; padding:3 5;'>
      <p style="margin:0;">Data Permintaan</p>
    </div>
  <div id="rcp_sc1">  
    <div class="form-group vrapet">
      <label class="col-sm-4">No. Trx</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="trxId" readonly />
      </div>
    </div>
    <script>
    $.ajax({
      url     : "./ajax/nmTrxResep.php",
      success : function(trxid){
        $("#trxId").val(trxid);
      }
    });
    </script>
    <div class="form-group vrapet">
      <label class="col-sm-4">Tanggal</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="date" id="trxDate" value="<?php echo date('Y-m-d'); ?>" />
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Shift</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="trxShift" readonly />
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Nama Dokter</label>
      <div class="col-sm-8">
        <select class="form-control font-small" type="text" id="dokter">
        </select>
      </div>
    </div>

    <div style='background-color:#DDD; font-weight:bold; margin:2 0; padding:3 5;'>
      <p style="margin:0;">Pasien</p>
    </div>
    <div class="form-group vrapet">
      <label class="col-sm-4">Nama</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienNama" />
        <div id="pasienList" class="kode-float"></div>
        <input type=hidden id="pasienId" /> 
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Alamat</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienAlamat" />
      </div>
    </div>

    <div class="form-group vrapet">
      <label class="col-sm-4">Kota</label>
      <div class="col-sm-8">
        <input class="form-control font-small" type="text" id="pasienKota" />
      </div>
    </div>
    <input type="hidden" id="pasienId" />
    <div id="newPasBtn" style='visibility:hidden;'>
      <button class="btn btn-primary form-control" id="saveNewpas" onClick=savepasien()>Simpan Pasien Barus</button>
    </div>
    <div>
    <button class="btn btn-success form-control" id="rqstSubmit">Simpan Permintaan Resep</button>
    </div>
    <script>
      function savepasien(){
        $("#rcp2_psien").html( $("#pasienNama").val() );
        $("#rcp2_almat").html( $("#pasienAlamat").val() );
        $("#rcp2_pkota").html( $("#pasienKota").val() );
        $.post(
          'actions.php?mod=i&obj=pasien',{
            id      : $("#pasienId").val(),
            nama    : $("#pasienNama").val(),
            alamat  : $("#pasienAlamat").val(),
            kota    : $("#pasienKota").val()
          },function(response){
            console.log(response);
            $("#newPasBtn").css("visibility","hidden");
          }
        );
      }
    </script>
    </div> <!-- seksi 1 -->
    <div id="rcp_sc2" style='display:none;'>
      <ul class="list-group">
        <li class="list-group-item" id="rcp2_trxId"></li>
        <li class="list-group-item" id="rcp2_trxTg"></li>
        <li class="list-group-item" id="rcp2_shift"></li>
        <li class="list-group-item" id="rcp2_doktr"></li>
        <li class="list-group-item" id="rcp2_psien"></li>
        <li class="list-group-item" id="rcp2_almat"></li>
        <li class="list-group-item" id="rcp2_pkota"></li>
      </ul>
    </div>
    <div id="rcpResponse"></div>
  </div>
  <div class='col-sm-8'>
    <div style='background-color:#DDD; font-weight:bold; margin:2 0; padding:3 5;'>
      <p style="margin:0;">Data Obat</p>
    </div>
    <div class="form-inline">
      <div class="form-group vrapet col-sm-3">
        <label>Nama obat</label>
          <input class="form-control font-small" id="med_nama" type="text" />
          <div class="kode-float" id="obatList"></div>
          <input class="form-control font-small" id="med_kodeObat" type="hidden" readonly />
      </div>

      <div class="form-group vrapet col-sm-3">
        <label>Banyaknya</label>
          <input class="form-control font-small" id="qty" type="text" maxlength="4"/>
      </div>

      <div class="form-group vrapet col-sm-3">
        <label>Aturan Pakai</label>
        <input class="form-control" id="med_dosis" list="dosisList" />
            <datalist id="dosisList">
              <option value='1x1, Pagi' >
              <option value='1x1, Siang' >
              <option value='1x1, Sore' >
              <option value='1x1, Malam' >
              <option value='2x1, Pagi - Siang' >
              <option value='2x1, Pagi - Sore' >
              <option value='2x1, Pagi - Malam' >
              <option value='2x1, Siang - Sore' >
              <option value='2x1, Siang - Malam' >
              <option value='2x1, Sore - Malam' >
              <option value='3x1, Pagi-Siang-Malam' >
              <option value='1x2, Pagi' >
              <option value='1x2, Siang' >
              <option value='1x2, Sore' >
              <option value='1x2, Malam' >
              <option value='2x2, Pagi - Siang' >
              <option value='2x2, Pagi - Sore' >
              <option value='2x2, Pagi - Malam' >
              <option value='2x2, Siang - Sore' >
              <option value='2x2, Siang - Malam' >
              <option value='2x2, Sore - Malam' >
              <option value='3x2, Pagi-Siang-Malam' >
            </datalist>
      </div>

      <div class="form-group vrapet col-sm-3">
        <label>Diskon</label>
          <input type="number" id="med_diskon" class="form-control" value="0">
      </div>
    </div>

    <!-- daftar resep -->
    <div id="rcpResponse"></div>
    <div>&nbsp;</div>
    <table class='font-small' width="100%" border=1 cellspacing=0>
      <thead>
        <tr>
          <th>No.</th>
          <th>Kode & Nama Obat</th>
          <th>Bny</th>
          <th>Harga</th>
          <th>Diskon</th>
          <th width="92px">Jml Harga</th>
          <th width="50px">Edit</th>
        </tr>
      </thead>
      <tbody id="dataResep">
        
      </tbody>
    </table>
    <div style="text-align:right; padding-right: 30px; margin-top:20px;">
      <a href='javascript:void(0)' class='btn btn-success' onClick=refreshRecipeOut()>Refresh</a>
      <!--a href='javascript:void(0)' class='btn btn-success' onClick=printResep()>Cetak Nota</a-->
      <a href='javascript:void(0)' class='btn btn-success' onClick=printLabel()>Cetak Label</a>
      <a href='javascript:void(0)' class='btn btn-danger' onClick=buangResep()>Batal</a>
      <a href='javascript:void(0)' class='btn btn-primary' id='recipeDone'>Selesai</a>
    </div>
    <!-- daftar resep -->
  </div>
  <!-- kolom obat -->
</div>


<div class="col-sm-3 form-horizontal" style="height: 800px;">
  
  

</div>
 
  <!--script>
  $.getJSON(
    './ajax/embalas.php',
    function(emb){
      console.log(emb);
      $("#e_r").val(emb.e_r);
      $("#e_p").val(emb.e_p);
    }
   );
  </script-->
  
<script>
  $(document).ready( function(){
    $.ajax({
      url     : './ajax/dokters.php',
      success : function(dokters){
        $("#dokter").html(dokters);
      }
    });    
  });

  var dina = new Date();
  var jame = dina.getHours();
  var jam = parseInt(jame);
  if( jam >= 7 && jam < 14 ){
    $("#trxShift").val('Pagi')
  }else if( jam >= 14 && jam < 21){
    $("#trxShift").val('Sore')
  }else{
    $("#trxShift").val('Lembur');
  }

  function printResep(){
  
  }

  function printLabel(){
    var trxId = $("#trxId").val();
    window.open('print.php?obj=label&id='+trxId);
  }

  function buangResep(){
    var yakin = confirm('Resep akan dihapus permanen !!');
    if(yakin == true ){
      var trxId = $("#trxId").val();
      $.post('./ajax/buangResep.php',{
        tid : trxId
      },function(responses){
        location.reload();
      });
    }

  }

  function refreshRecipeOut(){

    let trxId = $("#rcp2_trxId").html();
    showRecipeOut(trxId);
  }


  function grato(subtotal,persen){
    let f_r = $("#f_r").val();
    let f_e = $("#f_e").val();
    let kon = $("#konsultasi").val();
    let grandtotal = parseInt(f_r) + parseInt(f_e) + parseInt(kon) + subtotal + persen;
    $("#grandtotal").html( grandtotal.toLocaleString("id-ID",{minimumFractionDigits: 2}) );
  }

  function showRecipeOut(trxid){
    $.getJSON("ajax/recipeOutItems.php?id="+trxid,function(recipes){
    //$.getJSON("ajax/recipeOutItems.php?id=013005",function(recipes){
    
      //console.log(recipes.obat);
      
      $("#dataResep tr").remove();
      let sn = 1;
      let subtotal = 0;
      $.each(recipes.obat, function(i,data){
        let jmHarga = parseFloat(data.banyaknya) * parseFloat(data.harga_resep) * ( 100 - parseInt(data.diskon)) / 100;
        //alert( data.harga_resep +"*"+data.banyaknya+"="+jmHarga );
        $("#dataResep").append(
          "<tr>"+
          "<td>"+sn+"</td>"+
          "<td>["+data.kodeObat+"] "+data.namaObat+"</td>"+
          "<td align='right'>"+data.banyaknya+"</td>"+
          "<td align='right'>"+data.harga_resep.toLocaleString("id-ID",{ minimumFractionDigits: 2 })+"</td>"+
          "<td align='right'>"+data.diskon+"%</td>"+
          "<td align='right'>"+jmHarga.toLocaleString("id-ID",{ minimumFractionDigits: 2 })+"</td>"+
          "<td align='right'>hapus</td>"+
          "</tr>"
        );
        sn+=1;
        subtotal += jmHarga;
      });
      let persen = parseInt(recipes.harga.faktor_p) / 100 * subtotal;
      //let grandtotal = subtotal + parseInt(recipes.harga.faktor_r) + parseInt(recipes.harga.faktor_e) + persen;
      
      $("#dataResep").append(
        "<tr>"+
        "  <td align='right' colspan='5'>Sub Total</td>"+
        "  <td align='right' id='subtotal'>"+subtotal.toLocaleString("id-ID",{minimumFractionDigits: 2})+"</td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>R</td>"+
        "  <td align='right'><input type='number' id='f_r' size='10' value="+recipes.harga.faktor_r+" onblur=grato("+subtotal+","+persen+") /></td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>E</td>"+
        "  <td align='right'><input type='number' id='f_e' size='10' value="+recipes.harga.faktor_e+" onblur=grato("+subtotal+","+persen+") /></td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>%</td>"+
        "  <td align='right'><input type='text' id='f_p' size='10' value="+parseFloat(persen).toLocaleString("id-ID",{minimumFractionDigits: 2})+" /></td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>Konsultasi</td>"+
        "  <td align='right'><input type='text' id='konsultasi' size='10' value='0' onblur=grato("+subtotal+","+persen+") /></td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"+
        "<tr>"+
        "  <td align='right' colspan='5'>Grand Total</td>"+
        "  <td align='right' id='grandtotal'></td>"+
        "  <td>&nbsp;</td>"+
        "</tr>"
      );
      
    });

    
  }
</script>
