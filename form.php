<?php
  echo "
  <h2 style='text-align:center;'>Formullir Data ".ucfirst($_GET['obj'])."</h2>
  <div class='bg-danger' style='padding: 15px; font-weight: bold;'>
  Gunakan tombol &quot;<b>TAB</b>&quot; untuk berpindah kolom formulir .. !
  </div>
  ";
  require_once('lib/class.forms.inc.php');
  $frm = new forms();
  if($_GET['obj'] == 'profil'){
    echo "
    <div class='page-banner'>
      <p>DATA PROFIL</p>
    </div>";
    $data = $frm->select("*","profil","nama",0);
    echo "<form action='act-profil.php' method='post' enctype='multipart/form-data'>";
    $frm->getForm('profil',$_GET['mod'],$data[0],1);
    echo "</form>";
  }

  $obkey = array('karyawan'=>'kode','dokter'=>'kode','pasien'=>'id','obat'=>'kode',
          'optKategoriObat'=>'nomor','optSatuanObat'=>'nomor','pbf'=>'pbf_nomor');


  if($_GET['obj']=='unito'){
    $obj='optSatuanObat';
  }elseif( $_GET['obj'] == 'katob' ){
    $obj = 'optKategoriObat';
  }else{
    $obj = $_GET['obj'];
  }
  $mod=$_GET['mod'];
  if($mod == 'i'){
    formMode($obj,$mod,$frm);
  }else{
    formMode($obj,$mod,$frm,$obkey[$obj],$_GET['id']);
  }



function formMode($obj,$mod,$frm,$key=null,$id=null){
  if($mod=='i'){
    echo "<form action='actions.php?obj=".$obj."&mod=i' method='post' enctype='multipart/form-data'>";
    $frm->getForm($obj,'i',null,1);
    echo "</form>";
  }else{
    $data=$frm->pickone("*",$obj,$key,$id);
    echo "<form action='actions.php?obj=".$obj."&mod=u' method='post' enctype='multipart/form-data'>";
    $frm->getForm($obj,'u',$data,1);
    echo "</form>";
  }
}

if($_GET['obj'] == 'obat'){
  echo " <script> ";
  if($_GET['mod']=='i'){
    echo "
    $.ajax({
      url: './ajax/obatLastCode.php',
      success : function(ko){
        $('#med_nomor').val(ko);
        $('#med_kode').val(ko);
      }
    });
    ";
  }
  echo "
  $('#med_kode').focus();
  $('#med_kode').blur( function(){
    var kdo = $('#med_kode').val();
    $.getJSON('ajax/tukukeri.php?id='+kdo,function(tuker){
      $('#med_harga_beli').val(tuker.hargaNett);
      $('#med_ipb').val(tuker.isiPerBox);
      $('#med_harga_resep').val(tuker.hargaResep);
      $('#med_harga_hv').val(tuker.hargaBebas);
    });
  });

  </script>
  ";
}

?>
