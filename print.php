<?php
/*
  if( $_GET['obj'] == 'fakturtgl' ){
    $filename ="excelreport.xls";
    $contents = "testdata1 \t testdata2 \t testdata3 \t \n";
    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename='.$filename);
  }
*/
  switch($_GET['obj']){
    case 'fakturtgl'  : $title = 'Faktur'; break;
    case 'resep'      : $title = 'Nota Resep'; break;
    case 'label'      : $title = 'Label Resep'; break;
    case 'nonresep'   : $title = 'Non Resep'; break;
  }
?>
<html>
  <head>
    <title>Cetak <?php echo $title; ?> </title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./js/posapotek.js"></script>
    <style>
    .table-print {
      font-family: monospace; font-size: 8pt; width:96%; margin: 2%;
    }
    .table-print>thead>tr>th { font-weight: bold; text-align: center; padding: 4px; }
    .table-print>tbody>tr>td { padding: 0px 4px; }
    .ra { text-align: right;}
    p {padding: 0; margin: 0;}
    </style>
  </head>
  <body onLoad = window.print()>
  <!--body-->
    <?php
      if( $_GET['obj'] == 'fakturtgl' ){
        echo "
        <script>
          var tg0 = localStorage.getItem('tg0');
          var tg1 = localStorage.getItem('tg1');
          var pbf = localStorage.getItem('pbf');
          $.post('./ajax/fakturByTglPbf.php?mod=cetak',
          {
            tg0 : tg0,
            tg1 : tg1,
            pbf : pbf
          },function(faktur){
            //console.log(faktur);
            $('#fakRecap').html(faktur);
          });
        </script>";

        echo "<H3>LAPORAN FAKTUR</H3>";
        echo '
          <div id="fakRecap" class="table-print">

          </div>
        ';
      }

      if($_GET['obj'] == 'resep'){
        require("priceCalc.php");
        echo "<p>
        Nota Resep ".$_GET['id']."
        </p>";

        echo "
        <table width='100%' border=0 class='table-print'>
          <thead>
            <tr>
              <th>No.</th>
              <th>Kode & Nama Obat</th>
              <th>Bny</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Jml Harga</th>
              <th>Diskon</th>
              <th>\R</th>
              <th>%</th>
              <th width=100px>Total Harga</th>
            </tr>
          </thead>
          <tbody>
          ";
          require_once('./lib/class.crud.inc.php');
          $resep = new dbCrud();
          $data  = $resep->picksome("trxResep.*,obat.nama, obat.satuan, obat.harga_resep",
                  "trxResep,obat","trxId='".$_GET['id']."' && obat.kode = trxResep.kodeObat");

          $toha  = 0;
          for($i = 0 ; $i < COUNT($data) ; $i++ ){
            $nomor = $i+1;

            $harga = hargaNotaResep($data[$i]['kuantitas'],$data[$i]['harga_beli'],
                    $data[$i]['diskon'],$data[$i]['e_p'],$data[$i]['e_r']);
            /*
            $hargaJual = ($data[$i]['kuantitas'] * $data[$i]['harga_beli'] );
            $hardis = (100 - $data[$i]['diskon'])/100 * $hargaJual;
            $hargin = (100 + $data[$i]['e_p']) / 100 * $hardis;
            $hargaSubt = $hargin +  $data[$i]['e_r'];*/

            echo
            "
            <tr>
              <td class='ra'>".$nomor.".</td>
              <td>".$data[$i]['kodeObat']."-".$data[$i]['nama']."</td>
              <td class='ra'>".$data[$i]['kuantitas']."</td>
              <td>".$data[$i]['satuan']."</td>
              <td class='ra'>".number_format($data[$i]['harga_beli'],1,",",".")."</td>
              <td class='ra'>".number_format($harga[0],1,",",".")."</td>
              <td class='ra'>".$data[$i]['diskon']."%</td>
              <td class='ra'>".number_format($data[$i]['e_r'],1,',','.')."</td>
              <td class='ra'>".$data[$i]['e_p']."% </td>
              <td class='ra'>".number_format($harga[1],1,",",".")."</td>
            </tr>
            ";
            $toha += $harga[1];
          }
        echo "

          <tr>
            <td colspan='9' class='ra'>Total harga</td>
            <td class='ra'>".number_format($toha,1,',','.')."</td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>Diskon</td>
            <td class='ra' id='diskon'></td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>L/</td>
            <td class='ra' id='e_l'>
            </td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>Konsultasi</td>
            <td class='ra' id='konsul'>
            </td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>Jumlah Dibayar</td>
            <td class='ra' id='topaid'></td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>Dibayar</td>
            <td class='ra' id='paid'></td>
          </tr>
          <tr>
            <td colspan='9' class='ra'>Kembali</td>
            <td class='ra' id='susuk'></td>
          </tr>
          </tbody>
          <script>
          var diskon = parseInt(localStorage.getItem( 'disk'));
          var e_l    = parseInt(localStorage.getItem( 'e_l'));
          var konsul = parseInt(localStorage.getItem( 'kons'));
          var granto = parseInt(localStorage.getItem( 'gtot'));
          var mbayar = parseInt(localStorage.getItem( 'byar'));
          var susuke = parseInt(localStorage.getItem( 'susk'));

          $('#diskon').html(diskon.toLocaleString('id'));
          $('#e_l').html(e_l.toLocaleString('id'));
          $('#konsul').html(konsul.toLocaleString('id'));
          $('#topaid').html(granto.toLocaleString('id'));
          $('#paid').html(mbayar.toLocaleString('id'));
          $('#susuk').html(susuke.toLocaleString('id'));
          </script>
        ";

      }

        if($_GET['obj'] == 'label'){
          require_once('./lib/class.crud.inc.php');
          $label = new dbCrud();
          /*$pasien = $label->pickone('pasienNama nama,pasienAlamat alamat, pasienKota kota',
                "trxResep","trxId",$_GET['id']);*/
          //select pasien.* from pasien,recipeRqst where trxId='009001' && pasien.id=recipeRqst.idPasien;
          $pasien = $label->picksome("pasien.*","pasien, recipeRqst",
                    " trxId = '".$_GET['id']."' && pasien.id = recipeRqst.idPasien");
          $obat   = $label->picksome('kodeObat,aturanPakai,nama','recipeOut,obat',"
                    trxId = '".$_GET['id']."' && obat.kode = recipeOut.kodeObat");
          $i = 0;
          while( $i < COUNT($obat)){
            echo "
            <table style='border:1px solid black; font-size:8pt; width: 48mm;
            height: 28mm; margin-bottom: 2mm;'>
              <tr>
                <td style='padding: 2px' align='center'>
                <p>Apotek Alip Farma Banjarnegara</p>
                  <b>".$pasien[0]['nama']."</b><br />
                  ".$pasien[0]['alamat']."
                </td>
              </tr>
              <tr>
                <td style='padding: 5px' align='center'>
                  ".$obat[$i]['nama']."<br />( ".$obat[$i]['aturanPakai']." )
                </td>
              </tr>
            </table>
            ";
            $i++;
          }

        }

        if($_GET['obj'] == 'nonresep'){
          require_once('./lib/class.crud.inc.php');
          $nnr = new dbCrud();
          kopnota($nnr);
          // nomor, tanggal, tipe
          $hdr = $nnr->pickone('trxDate,trxShift,JenisHarga','trxNonResep','trxId',$_GET['id']);
          switch($hdr['JenisHarga']){
            case 'non_resep'  : $nt = 'Non Resep'; break;
            case 'dokter'     : $nt = 'Nota Dokter'; break;
            case 'karyawan'   : $nt = 'Nota Karyawan'; break;
          }
          echo "
          <table class='table-print' style='width:145mm;'>
            <tr>
              <td colspan='2'>NOTA PENJUALAN</td>
              <td width='100px'>Nomor</td>
              <td width='100px'>: ".sprintf("%06d",$_GET['id'])."</td>
            </tr>
            <tr>
              <td width='100px;'>Tanggal </td>
              <td>: ".$nnr->tanggalTerbaca($hdr['trxDate'])." [".$hdr['trxShift']."]</td>
              <td>Jenis Nota </td>
              <td>:   ".strtoupper($nt)."</td>
            </tr>
          </table>
          ";

          //Nama/Kode Obat	Harga Satuan	Banyaknya	Jumlah Jual	Diskon	Jumlah Harga
          echo "
          <table class='table-print' style='width:145mm;'>
            <thead>
              <tr>
                <th>Nomor</th>
                <th>Nama Barang</th>
                <th>Banyaknya</th>
                <th>Harga Satuan</th>
                <th>Jumlah Harga</th>
                <th>Diskon</th>
                <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
            ";
          $stuff = $nnr->picksome('trxNonResep.*,obat.nama, obat.satuan',"trxNonResep,obat","trxId='".$_GET['id']."' && obat.kode = trxNonResep.kodeObat");
          //print_r($stuff);
          $i = 0;
          $gt = 0;
          while($i < COUNT($stuff)){
            $nomor = $i + 1;
            $hrgUnit = $stuff[$i]['harga_beli'];
            $JmHarga = $stuff[$i]['kuantitas'] * $hrgUnit;
            $diskon  = $JmHarga * $stuff[$i]['diskon'] / 100;
            $subTotal= $JmHarga - $diskon;
            echo "
            <tr>
              <td class='ra'>".$nomor.".</td>
              <td>".$stuff[$i]['nama']."</td>
              <td class='ra'>".$stuff[$i]['kuantitas']." ".$stuff[$i]['satuan']."</td>
              <td class='ra'>".number_format($hrgUnit,0,',','.')."</td>
              <td class='ra'>".number_format($JmHarga,0,',','.')."</td>
              <td class='ra'>".number_format($diskon,0,',','.')."</td>
              <td class='ra'>".number_format($subTotal,0,',','.')."</td>
            </tr>
            ";
            $gt+=$subTotal;
            $i++;
          }
          echo "
            <tr>
              <td colspan='6' class='ra'>Jumlah Keseluruhan</td>
              <td id='grandTotal' class='ra'>".number_format($gt,0,',','.')."</td>
            </tr>
            <tr>
              <td colspan='6' class='ra'>Diskon</td>
              <td id='diskon' class='ra'></td>
            </tr>
            <tr>
              <td colspan='6' class='ra'>Jumlah Dibayar</td>
              <td id='jmDibayar' class='ra'></td>
            </tr>
            <tr>
              <td colspan='6' class='ra'>Dibayar</td>
              <td id='jmUange' class='ra'></td>
            </tr>
            <tr>
              <td colspan='6' class='ra'>Kembali</td>
              <td id='jujul' class='ra'></td>
            </tr>
            </tbody>
          </table>

          <script>
            $('#diskon').html(parseInt(localStorage.getItem('nnrDiscount')).toLocaleString('id-ID'));
            $('#jmDibayar').html(parseInt(localStorage.getItem('nnrJmDbayar')).toLocaleString('id-ID'));
            $('#jmUange').html(parseInt(localStorage.getItem('nnrJmDuwite')).toLocaleString('id-ID'));
            $('#jujul').html(parseInt(localStorage.getItem('nnrUKembali')).toLocaleString('id-ID'));
          </script>
          ";
        }

    ?>
  </body>
</html>
<?php
  function kopnota($obj){
    $profil = $obj->pickone("*","profil","id",1);
    echo "
    <table>
      <tbody width='100%'>
        <tr>
          <td width='60px'>
            <img src='".$profil['logo']."' width='100%'/>
          </td>
          <td style='padding-left: 20px;'>
            <p style='font-size: 14pt;'>Apotek ".$profil['nama']."</p>
            <p style='font-size: 12pt;'>".$profil['alamat']."</p>
            <p style='font-size: 12pt;'>".$profil['telephone']."</p>
          </td>
        </tr>
      </tbody>
    </table>
    ";
  }
 ?>
