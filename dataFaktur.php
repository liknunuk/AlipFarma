<div class='table-banner'>
  <span class='table-title'>
    URAIAN FAKTUR NOMOR <?php echo $_GET['id']; ?>
  </span>
  <span style="float: right; margin-right: 15px">
  <a style="pointer:cursor;" onclick=showDataFaktur('<?php echo $_GET['id']; ?>','1')>
    Edit
  </a>
  </span>
</div>
<div class="table-responsive" id="dataFaktur">

</div>
<!-- modal form fabelList -->
<!-- Modal -->
<div id="fabelList-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detil Item Faktur</h4>
      </div>
      <div class="modal-body form-horizontal">
        <!-- form fabeldata-->
        <div class="form-group">
          <label class='col-sm-4'>Nomor Index</label>
          <div class='col-sm-8'>
            <input class='form-control' type='text' id='itemIndex' readonly />
          </div>
        </div>

        <div class="form-group form-horizontal">
          <label class='col-sm-4'>Kode Obat</label>
          <div class='col-sm-8'>
            <input class='form-control' type='text' id='kdObat' onkeyup=pilobat() placeholder="Tulis Nama Obat "/>
            <div class='kode-float' id="pilObat" style="display:none;"></div>
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'>Banyaknya (box)</label>
          <div class='col-sm-8'>
            <input class='form-control' type='number' id='qty' />
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'>Isi Per Box</label>
          <div class='col-sm-8'>
            <input class='form-control' type='number' id='ipb' />
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'>Harga / Box</label>
          <div class='col-sm-8'>
            <input class='form-control' type='number' id='hargaBeli' />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4">Satuan Jual</label>
          <div class="col-sm-8">
          <select class="form-control" id="satjual">
            <option>Botol/flez</option>
            <option>Lempeng</option>
            <option>Tablet</option>
            <option>Tube</option>
          </select>
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'>PPn 10%</label>
          <div class='col-sm-8'>
            <select id="ppnBeli" class='form-control'>
              <option value='1'>Harga Termasuk PPn</option>
              <option value='0'>Harga Belum Termasuk PPn</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'>Diskon (%)</label>
          <div class='col-sm-8'>
            <input class='form-control' type='number' id='discObat' />
          </div>
        </div>
        <div class="form-group">
          <label class='col-sm-4'><i>Periksa Data</i></label><br />
          <div class='col-sm-8'>
            <button class="btn btn-primary" id="fkbList-update">Simpan</button>
          </div>
        </div>

        <!-- form fabelList-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php
/* 
  echo "
  <script>
  $.ajax({
    url     : 'ajax/obatByFaktur.php?nf=".$_GET['id']."&em=1',
    success : function(faktur){
      $('#dataFaktur').html(faktur);
    }
  });
  </script>"; */
 ?>
<script>
  function showDataFaktur(nf,em){
    $.ajax({
    url     : 'ajax/obatByFaktur.php?nf='+nf+'&em='+em,
    success : function(faktur){
      $('#dataFaktur').html(faktur);
    }
  });
  }

  function editFabelList(fbi){
    $("#fabelList-modal").modal('show');
    $.getJSON("ajax/fakturList.php?id="+fbi, function(fl){
      $("#itemIndex").val(fbi),
      $("#kdObat").val(fl.kodeObat),
      $("#qty").val(fl.kuantitas),
      $("#hargaBeli").val(fl.harga_beli),
      $("#ppnBeli").val(fl.ppn10),
      $("#discObat").val(fl.disc)
    });
  }

  function pilobat(){
    var ko = $("#kdObat").val();

    if(ko.length < 3 ){
      $("#pilObat").hide();
    }else {
      $("#pilObat").show();
      $.ajax({
        url     :"./ajax/obatByKode.php?do="+ko,
        success : function(obats){
          $("#pilObat").html(obats);
        }
      })
    }
  }
</script>
<?php echo "<script> showDataFaktur('".$_GET['id']."','0') </script>"; ?>