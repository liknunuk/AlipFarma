<?php
  switch($_GET['obj']){
    case 'karyawan' :
      $title = "karyawan";
      $obj = "karyawan" ;
      $plh = "Cari Nama Karyawan";
      $sbx = "kwySrcBox";
      break;
    case 'dokter'   :
      $title = "dokter";
      $obj = "dokter";
      $plh = "Cari Nama Dokter";
      $sbx = "docSrcBox";
      break;
    case 'pasien'   :
      $title = "pasien";
      $obj = "pasien";
      $plh = "Cari Nama Pasien";
      $sbx = "pasSrcBox";
      break;
    case 'obat'     :
      $title = "obat";
      $obj = "obat";
      $plh = "Cari Nama Obat";
      $sbx = "medSrcBox";
      break;
    case 'katob'    :
      $title = "kategori obat";
      $obj = "optKategoriObat";
      $plh = "Cari Nama Kategori Obat";
      $sbx = "katSrcBox";
      break;
    case 'unito'    :
      $title = "satuan obat";
      $obj = "optSatuanObat";
      $plh = "Cari Nama Satuan";
      $sbx = "satSrcBox";
      break;
    case 'pbf'      :
      $title = "PBF";
      $obj  = 'pbf';
      $plh  = 'Cari Nama PBF';
      $sbx  = "pbfSrcBox";
  }
echo "
<div class='table-banner'>
  <span class='table-title'>
    DAFTAR ".strtoupper($title)."
  </span>
  <span class=' btn-right'>
    <a class='btn btn-primary' href='./?show=form&obj=".$_GET['obj']."&mod=i'>
      <i class='fa fa-plus'></i>&nbsp;".$title."
    </a>
  </span>

</div>";

require_once("lib/class.crud.inc.php");
$data = new dbcrud();
$thdata = $data->picksome("pertanyaan,formName","formIface","fgroup='".$obj."'");
$colorder = array(
  'karyawan'=>'kode', 'dokter'=>'kode','obat'=>'kode','pasien'=>'id',
  'optKategoriObat'=>'nomor','optSatuanObat'=>'nomor', 'pbf'=>'pbf_nama'
);

$colkey = array(
  'karyawan'=>'kode', 'dokter'=>'kode','obat'=>'kode','pasien'=>'id',
  'optKategoriObat'=>'nomor','optSatuanObat'=>'nomor','pbf'=>'pbf_nomor'
);
?>
<div class='row'>
  <div class='col-sm-3'>
  <?php
    $data->pageNumber($obj,$_GET['obj'],$_GET['nh']);
    echo "
    <script>
     function chref(obj,nh){
       var href='./?show=data&obj='+obj+'&mod=i&nh='+nh;
       //alert(href);
       window.location=href;
     }
    </script>
    ";
  ?>
  </div>
  <div class='col-sm-9'>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="<?=$sbx; ?>" placeholder="<?=$plh; ?>" />
    </div>
    <div class="col-sm-6">
      &nbsp;
    </div>
  </div>
</div>
<div class='table-responsive'>
  <table class='table table-striped table-sm'>
  <thead>
    <tr>
      <?php
        $i = 0;
        while($i < COUNT($thdata)){
          if($_GET['obj']=='obat' && $i==0 ){
            $i++;
            continue;
          }
          echo "<th>".$thdata[$i]['pertanyaan']."</th>";
          $i++;
        }
      ?>
      <th width='150'>
        Kontrol
      </th>
    </tr>
  </thead>
  <tbody id="dataHolder">
    <?php
      $row = ($_GET['nh'] - 1) * rows;
      $tddata = $data->select("*",$obj,$colorder[$obj],$row);
      $i = 0;
      while($i < COUNT ($tddata)){
        $c=0;
        echo "<tr>";
        while($c < COUNT($thdata)){
          $col = $thdata[$c]['formName'];
          if($c==0){
            if($_GET['obj']=='obat'){
              $c++;
              continue;
            }
            echo "
              <td width='100'>".$tddata[$i][$col]."</td>
            ";
          }else{
            $tdtype = gettype($tddata[$i][$col]);
            if($tdtype == "integer" || $tdtype == "double"){
              $tda = "align='right'";
            }else{
              $tda ="";
            }
            $isn = isNominal($obj,$col);
            if($isn == true){
              echo "
                <td $tda>".number_format($tddata[$i][$col],0,',','.')."</td>
              ";
            }else{
              echo "
                <td $tda>".$tddata[$i][$col]."</td>
              ";
            }

          }
          $c++;
        }
        $ckey = $colkey[$obj];
        echo "
          <td>
            <a class='btn btn-primary' href='./?show=form&obj=".$obj."&mod=u&id=".$tddata[$i][$ckey]."'>
              <img src='ikonz/Edt16.png' />
            </a>
            <a class='btn btn-danger' onClick = delco('".$obj."','".$tddata[$i][$ckey]."')>
              <img src='ikonz/Del16.png' />
            </a>
          </td>
        ";
        echo "</tr>";
        $i++;
      }

    ?>
  </tbody>
  </table>
</div>

<?php
  function isNominal($obj,$col){
    $nominal = array("obat"=>array("harga_beli","harga_hv","harga_resep"));
    $array = $nominal[$obj];
    $res = array_search($col,$array);
    if( $res == NULL ){ return false; }else{ return true; }
  }
?>
