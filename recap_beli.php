<div class='page-banner'>
  <p>REKAPITULASI PEMBELIAN</p>
</div>
<?php
  require_once("./lib/class.crud.inc.php");
  $recap = new dbcrud();
 ?>
<div class="row">
 <div class="form-inline">
   <div class="form-group col-sm-3">
     <label>Tanggal</label>
     <input type="date" id="tg0" class="form-control" />
   </div>
   <div class="form-group col-sm-3">
     <label>Hingga Tanggal</label>
     <input type="date" id="tg1" class="form-control" />
   </div>
   <div class="form-group col-sm-3">
     <label>Nama PBF</label>
     <select id="pbf" class="form-control">
       <option value="">Semua PBF</option>
       <?php
       $sql = "SELECT DISTINCT(namaPBF) pbf FROM fabelData ORDER BY namaPBF limit 100";
       $qry = $recap->transact($sql);
       while($res = $qry->fetch()){
         echo "<option>".$res['pbf']."</option>";
       }
       ?>
     </select>
   </div>
   <div class="form-group col-sm-3">
     <button id="fakturseek" class='btn btn-primary'>Buka Faktur</button>
     <button id="fakturPrnt" class="btn btn-info" onclick=fakturCetak()>Cetak</button>
     <button id="fakturPrnt" class="btn btn-info" onclick=fakturExport()>Export</button>
   </div>
 </div>
</div><br/>

</br/>

<div class="row">
  <div id="fakRecap">

  </div>
  <!--
   <table class='table table-striped table-sm'>
     <thead>
       <tr>
         <th>NAMA OBAT</th>
         <th>JUMLAH OBAT</th>
         <th>SATUAN</th>
         <th>HARGA BELI</th>
         <th>HARGA HV</th>
         <th>HARGA RESEP</th>
       </tr>
     </thead>
     <tbody id="fakRecap">

     </tbody>
   </table>
 -->
</div>

<script>
function fakturCetak(){
  localStorage.setItem("tg0", $("#tg0").val());
  localStorage.setItem("tg1", $("#tg1").val());
  localStorage.setItem("pbf", $("#pbf").val());
  window.open('print.php?obj=fakturtgl');
}

function fakturExport(){
  $.post("setSess.php",{
    tg0 : $("#tg0").val(),
    tg1 : $("#tg1").val(),
    pbf : $("#pbf").val()
  },function(){
    $.ajax({
      url:'./ajax/fakturByTglPbf-Cetak.php',
      success : function(response){
        $("#fakRecap").html(response);
      }
    });
  });

}
</script>
