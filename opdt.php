<?php
  switch($_GET['obj']){
    case 'jobat'    : $title = "Harga Obat"; $obj = "optJenisHarga"; break;
    case 'embal'    : $title = "Embalase"; $obj = "optEmbalase"; break;
  }
echo "<h3>DAFTAR ".strtoupper($title)."</h3>";

require_once("lib/class.crud.inc.php");
$data = new dbcrud();
$thdata = $data->picksome("pertanyaan,formName","formIface","fgroup='".$obj."'");
$colorder = array('optEmbalase'=>'e_r','optJenisHarga'=>'resep');

$colkey = array('optEmbalase'=>'e_r','optJenisHarga'=>'resep');
?>

<div class='table-responsive'>
  <table class='table table-striped table-sm'>
  <thead>
    <tr>
      <?php
        $i = 0;
        while($i < COUNT($thdata)){
          echo "<th>".$thdata[$i]['pertanyaan']."</th>";
          $i++;
        }
      ?>
      <th width='150'>
        Kontrol
      </th>
    </tr>
  </thead>
  <tbody>
    <?php
      $row = ($_GET['nh'] - 1) * rows;
      $tddata = $data->select("*",$obj,$colorder[$obj],$row);
      $i = 0;
      while($i < COUNT ($tddata)){
        $c=0;
        echo "<tr>";
        while($c < COUNT($thdata)){
          $col = $thdata[$c]['formName'];
          if($c==0){
            echo "
              <td>".$tddata[$i][$col]."</td>
            ";
          }else{
            echo "
              <td>".$tddata[$i][$col]."</td>
            ";
          }
          $c++;
        }
        $ckey = $colkey[$obj];
        switch($obj){
          case 'optEmbalase'    : $obj='embal'; break;
          case 'optJenisHarga'  : $obj='jobat'; break;
        }
        echo "
          <td>
            <a class='btn btn-primary' href='./?show=opts&obj=".$obj."&mod=u&id=".$tddata[$i][$ckey]."'>
              <img src='ikonz/Edt16.png' />
            </a>
          </td>
        ";
        echo "</tr>";
        $i++;
      }

    ?>
  </tbody>
  </table>
</div>
