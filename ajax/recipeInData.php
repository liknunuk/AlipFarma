<?php
  require_once("../lib/class.crud.inc.php");
  $rcp = new dbcrud();
  $sr = $_GET['sr'];
  $sql = "SELECT  recipeRqst.trxId, pasien.nama, COUNT(recipeIn.trxId) item,
                  dokter.nama as namaDokter
          FROM recipeRqst, pasien, recipeIn, dokter
          WHERE   pasien.id = recipeRqst.idPasien &&
                  recipeRqst.statusBayar ='".$sr."' &&
                  recipeIn.trxId = recipeRqst.trxId &&
                  dokter.kode = recipeRqst.idDokter
          GROUP BY recipeIn.trxId";
  $qry = $rcp->transact($sql);
  $data = array();
  while($res = $qry->fetch()){
    array_push($data,$res);
  }

  echo json_encode($data);

?>
