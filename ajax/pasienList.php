<?php
  require_once("../lib/class.crud.inc.php");
  $doc = new dbcrud();

  $docs = $doc->picksome("*","pasien","nama LIKE '%".$_GET['np']."%'");

  $i = 0;
  while($i < COUNT($docs)){
    echo "
    <div>
      <p class='docName' id='pn_".$i."' onClick=setPasData(".$docs[$i]['id'].",".$i.")>".$docs[$i]['nama']."</p>
      <p class='docAddr' id='pa_".$i."' >".$docs[$i]['alamat']."</p>
      <p class='docAddr' id='pk_".$i."' >".$docs[$i]['kota']."</p>
    </div>
    ";
    $i++;
  }
  echo "<div><p onClick=newPasi() style='pointer:cursor;'>Pasian Baru</p></div>";
?>

<script>
  function setPasData(pas_id,idx){

    $("#pasienNama").val($("#pn_"+idx).html());
    $("#pasienAlamat").val($("#pa_"+idx).html());
    $("#pasienKota").val($("#pk_"+idx).html());
    $("#pasienId").val(pas_id);

    $("#pasienList").hide();
    $("#pasienList").html('');
  }

  function newPasi(){
    $("#pasienList").hide();
    $("#pasienList").html('');
    $("#newPasBtn").css("visibility","visible");
    $.ajax({
      url     : 'reference.php?key=pas_id',
      success : function(pas_id){
        $("#pasienId").val(pas_id);
      }
    });
  }
</script>
