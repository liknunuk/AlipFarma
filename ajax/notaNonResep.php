<?php
require_once('../lib/class.crud.inc.php');
$posa = new dbcrud();

$sql = "SELECT trxNonResep.*, obat.nama, obat.satuan FROM trxNonResep,obat
        WHERE trxId = '".$_GET['id']."' && obat.kode = trxNonResep.kodeObat";
$qry = $posa->transact($sql);
$no = 1;
$toha = 0;
//KODE OBAT	NAMA OBAT	HARGA OBAT	JML OBAT	JUMLAH HARGA	DISCOUNT	TOTAL HARGA
?>
<table class='table' width="100%" border=1 cellspacing=0 cellpadding=1>
  <thead>
    <tr>
      <th>No.</th>
      <th>Nama/Kode Obat</th>
      <th>Harga Satuan</th>
      <th>Banyaknya</th>
      <th>Jumlah Jual</th>
      <th>Diskon</th>
      <th style="width:120px;">Jumlah Harga</th>
      <th style="width:120px;">Kontrol</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $nomor = 1;
    $toha  = 0;
    while($r = $qry->fetch()){
      $harga = $r['harga_beli'];
      $jmHarga = $harga * $r['kuantitas'];
      $diskon = $r['diskon']/100 * $jmHarga;
      $subTotal = $jmHarga - $diskon;
      echo "
      <tr>
        <td class='ra'>".$nomor.".</td>
        <td>(".$r['kodeObat'].") ".$r['nama']."</td>
        <td class='ra'>".number_format($harga,0,',','.')."</td>
        <td class='ra'>".$r['kuantitas']." ".$r['satuan']."</td>
        <td class='ra'>".number_format($jmHarga,0,',','.')."</td>
        <td class='ra'>".number_format($diskon,0,',','.')."</td>
        <td class='ra'>".number_format($subTotal,0,',','.')."</td>
        <td><a href='javascript:void(0)' onClick=hapusHV('".$r['itemIndex']."')>Batal</a></td>
      </tr>
      ";
      $nomor++;
      $toha += $subTotal;
    }
    ?>
    <tr>
      <td colspan="6" class="ra" style='border-top: 1px solid #000;'>Sub Total</td>
      <td class="ra" id="subTotal" style='border-top: 1px solid #000;'><?php echo $toha; ?></td>
      <td style='border-top: 1px solid #000;'>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" class="ra">Diskon</td>
      <td><input type='number' id='diskone'  class="form-control" style="width: 100px;"/></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" class="ra">Jumlah Bayar</td>
      <td class="ra" id="jmBayar"></td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td colspan="6" class="ra">Jumlah Uang</td>
      <td><input type='number' id='jmFulus'  class="form-control" style="width: 100px;"/></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" class="ra">Kembali</td>
      <td class="ra" id="susuke"></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<div style="margin-top: 30px; text-align: right; padding-right: 30px;">
  <button class='btn btn-danger' id="notaWurung">Batal</button>
  <button class='btn btn-success' id="notaCetak">Cetak</button>
</div>
<script>
  $("#diskone").keypress( function(e){
    if(e.which == 13 ){
      var subtotal = parseInt($("#subTotal").html());
      var discount = parseInt($("#diskone").val());
      var jmdbayar = subtotal - discount;
      $("#jmBayar").html(jmdbayar);
      $("#jmFulus").focus();
      }
  });

  $("#jmFulus").keypress( function(e){
    if(e.which == 13 ){
      var jmdbayar = parseInt($("#jmBayar").html());
      var jmdhuwit = parseInt($("#jmFulus").val());
      var susuke   = jmdhuwit - jmdbayar;
      $("#susuke").html(susuke);
    }
  });

  $("#notaCetak").click( function(){
    <?php
    echo 'var notaNmrTrx = '.$_GET['id'];
     ?>

    var ntSubTotal = $("#subTotal").html();
    var ntDiscount = $("#diskone").val();
    var ntJmDbayar = $("#jmBayar").html();
    var ntJmDhuwit = $("#jmFulus").val();
    var ntJmSusuke = $("#susuke").html();

    //alert(notaNmrTrx);

    localStorage.setItem('nnrSubTotal',ntSubTotal);
    localStorage.setItem('nnrDiscount',ntDiscount);
    localStorage.setItem('nnrJmDbayar',ntJmDbayar);
    localStorage.setItem('nnrJmDuwite',ntJmDhuwit);
    localStorage.setItem('nnrUKembali',ntJmSusuke);

    window.open('print.php?obj=nonresep&id='+notaNmrTrx);

  });

  $("#notaWurung").click( function(){
    var yakin = confirm("Nota <?php echo $_GET['id']; ?> Dibatalkan ?");
    if( yakin == true ){
      $.post("ajax/hapusTrxObat.php?nt=nhv",{ trxId : <?php echo $_GET['id']; ?> },function(response){ loadNota();})
    }
  });

  function hapusHV(idx){
    var yakin = confirm("Obat Dibatalkan ?");
    if( yakin == true ){
      $.post("ajax/hapusTrxObat.php?nt=nr",{ idx : idx },function(response){ loadNota();})
    }
  }
</script>
