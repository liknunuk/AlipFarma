<?php
require("../priceCalc.php");
require_once('../lib/class.crud.inc.php');
$posa = new dbcrud();

$sql = "SELECT trxResep.*, obat.nama, obat.satuan, obat.harga_resep FROM trxResep,obat
        WHERE trxId = '".$_GET['id']."' && obat.kode = trxResep.kodeObat";
$qry = $posa->transact($sql);
$no = 1;
$toha = 0;


while($r = $qry->fetch()){
  $harga = hargaNotaResep($r['kuantitas'],$r['harga_beli'],$r['diskon'],$r['e_p'],$r['e_r']);
  /*$hargaJual = ($r['kuantitas'] * $r['harga_beli'] );
  // $hargaSubt = $hargaJual + $hargaJual * ( ($r['e_r'] + $r['e_p']) / 100);
  $hardis = (100 - $r['diskon'])/100 * $hargaJual;
  $hargin = (100 + $r['e_p']) / 100 * $hardis;
  $hargaSubt = $hargin +  $r['e_r'];*/
  echo "
  <tr>
    <td>".$no."</td>
    <td>".$r['nama']."<br />".$r['kodeObat']."</td>
    <td class='ra'>".$r['kuantitas']." ".$r['satuan']."</td>
    <td>".number_format($r['harga_beli'],1,',','.')."</td>
    <td class='ra'>".number_format($harga[0],1,',','.')."</td>
    <td class='ra'>".$r['diskon']."</td>
    <td class='ra'>".$r['e_r']."</td>
    <td class='ra'>".$r['e_p']."</td>
    <td class='ra' width='100px'>".number_format($harga[1],1,',','.')."</td>
    <td><a href='javascript:void(0)' onClick=hapusObat('".$r['itemIndex']."','".$r['trxId']."')>Batal</a></td>
  </tr>
  ";
  $no++;
  $toha+=$harga[1];
}

?>
<tr>
  <td colspan='8' class='ra'>Total harga</td>
  <!--td class='ra' id="nota-total"><?php echo number_format($toha,0,',','.'); ?></td-->
  <td class='ra' id="nota-total"><?php echo $toha; ?></td>
</tr>
<tr>
  <td colspan='8' class='ra'>Diskon</td>
  <td class='ra'>
    <input class='ra' id='nota-diskon' value="0" />
  </td>
</tr>
<tr>
  <td colspan='8' class='ra'>L/</td>
  <td class='ra'>
    <input class='ra' id='nota-l' value="0" />
  </td>
</tr>
<tr>
  <td colspan='8' class='ra'>Konsultasi</td>
  <td class='ra'>
    <input class='ra' id='nota-konsul' value="0" />
  </td>
</tr>
<tr>
  <td colspan='8' class='ra'>Jumlah Dibayar</td>
  <td class='ra' id='nota-topaid'></td>
</tr>
</tr>
<tr>
  <td colspan='8' class='ra'>Dibayar</td>
  <td class='ra'>
    <input class='ra' id='nota-paid' value="0" />
  </td>
</tr>
<tr>
  <td colspan='8' class='ra'>Kembali</td>
  <td class='ra' id='nota-susuk'></td>
</tr>
<script>
$(document).ready( function(){
  $("#nota-paid").focus( function(){
    var totalHarga = parseInt($("#nota-total").html());
    var diskon = parseInt($("#nota-diskon").val());
    var nota_l = parseInt($("#nota-l").val());
    var konsul = parseInt($("#nota-konsul").val());
    var totalcost = totalHarga - diskon + nota_l + konsul;
    $("#nota-topaid").html(totalcost);
  });

  $("#nota-paid").keypress(function(e){
    if(e.which == 13 ){
      e.preventDefault();
      var rega = parseInt($("#nota-topaid").html());
      var mbayar = parseInt($("#nota-paid").val());
      var jujul = mbayar - rega;
      $("#nota-susuk").html(jujul);
    }
  });
});
function hapusObat(id,trxId){
  $.post('./ajax/hapusObatResep.php',
    {
      idx: id
    },function(responses){
      $.ajax({
        url     : './ajax/notaResep.php?id='+trxId,
        success : function(nota){
          $("#dataResep").html(nota);
        }
      });
  })
}
</script>
