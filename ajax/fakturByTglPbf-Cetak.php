<?php
session_start();

require_once('../lib/class.crud.inc.php');
$posa = new dbcrud();
//print_r($_POST);
$pbf = $_SESSION['pbf'];
$tg0 = $_SESSION['tg0'];
$tg1 = $_SESSION['tg1'];
$nuf = 0;
$faktur = array();
 // cari nomor faktur
  $sql = "SELECT nomorFaktur, namaPBF, tanggalFaktur , tanggalJatuhTempo, totalDiskon
          FROM fabelData
          WHERE namaPBF LIKE '%".$pbf."' AND
                tanggalFaktur BETWEEN '".$tg0."' AND '".$tg1."'";
  $qry = $posa->transact($sql);
  while($res = $qry->fetch()){
    ++$nuf;
    $dafo = $posa->picksome("obat.nama, fabelList.harga_beli, fabelList.ppn10,
            fabelList.kuantitas, fabelList.disc","obat, fabelList","obat.kode = fabelList.kodeObat
            && fabelList.nomorFaktur ='".$res['nomorFaktur']."'");

    $tobel = 0;
    for($i = 0 ; $i < COUNT($dafo) ; $i++ ){
      $nm = $i + 1;
      if($dafo[$i]['ppn10'] == 1){
        $ho = 10/11 * $dafo[$i]['harga_beli'];
        $tx = 1/11 * $dafo[$i]['harga_beli'];
      }else{
        $ho = $dafo[$i]['harga_beli'];
        $tx = 0.1 * $dafo[$i]['harga_beli'];
      }

      if($i == 0){
        $nuf = $nuf;
        $tfk = $posa->tanggalTerbaca($res['tanggalFaktur']);
        $tjt = $posa->tanggalTerbaca($res['tanggalJatuhTempo']);
        $nmf = $res['nomorFaktur'];
        $pbf = $res['namaPBF'];
      }else{
        $nuf = '';
        $tfk = '';
        $tjt = '';
        $nmf = '';
        $pbf = '';
      }

      $hn = $ho + $tx;
      $dc = ($dafo[$i]['disc']) / 100 * $hn;
      $th = ($hn - $dc) * $dafo[$i]['kuantitas'];
      $tobel+=$th;
      $dataFaktur = array(
        'tanggalFaktur'=>$tfk,
        'tanggalJatuhTempo'=>$tjt,
        'namaPBF'=>$pbf,
        'nomorFaktur'=>$nmf,
        'namaObat'=>$dafo[$i]['nama'],
        'banyaknya'=>$dafo[$i]['kuantitas'],
        'hargaObat'=>$ho,
        'ppn'=>$tx,
        'diskon'=>$dc,
        'totalHarga'=>$th,
        'totalBelanja'=>'',
        'totalDiskon'=>'',
        'totalTagihan'=>''
      );

      array_push($faktur,$dataFaktur);
    }
    $tobay = $tobel-$res['totalDiskon'];
    $resumeFaktur= array(
      'tanggalFaktur'=>'',
      'tanggalJatuhTempo'=>'',
      'namaPBF'=>'',
      'nomorFaktur'=>'',
      'namaObat'=>'',
      'banyaknya'=>'',
      'hargaObat'=>'',
      'ppn'=>'',
      'diskon'=>'',
      'totalHarga'=>'',
      'totalBelanja'=>$tobel,
      'totalDiskon'=>$res['totalDiskon'],
      'totalTagihan'=>$tobay
    );
    //$nuf++;
    array_push($faktur,$resumeFaktur);
  }
  // export to csv
  // open the file "demosaved.csv" for writing
$file = fopen('faktur.csv', 'w');

// save the column headers
  fputcsv($file, array('Tgl. Faktur', 'Tgl. Jatuh Tempo', 'Nama PBF', 'No. Faktur','Nama Barang','Banyaknya','Harga Satuan','PPn 10%','Diskon','Total Harga','Total Belanja','Total Diskon','Total Tagihan'));

  // Sample data. This can be fetched from mysql too


  // save each row of the data
  foreach ($faktur as $row)
  {
  fputcsv($file, $row);
  }

  // Close the file
  fclose($file);


?>

<div style="margin: 10px; padding: 10px 30px; background: #EEEEEE;">
<a href='./ajax/faktur.csv' target='_blank'>Download</a>
</div>
