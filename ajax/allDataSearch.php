<table width="100%">

<?php
require_once('../lib/class.crud.inc.php');
$posa = new dbcrud();
$i = 0 ;

// data obat
if($_GET['obj'] == 'obat'){
  $data = $posa->picksome("*","obat","nama LIKE '%".$_GET['nama']."%'");
  while( $i < COUNT($data) ){
    echo "<tr>";
    foreach($data[$i] AS $k=>$v){
      if($k == 'nomor'){
        continue;
      }
      if( $k == 'harga_beli' || $k == 'harga_hv' || $k == 'harga_resep' || $k == 'isiPerBox'  )
      {
          echo "<td align='right'>". number_format($v,0,',','.') ."</td>";
      }else{
        echo "<td>". $v ."</td>";
      }

    }
    echo "
      <td>
        <a class='btn btn-primary' href='./?show=form&obj=".$_GET['obj']."&mod=u&id=".$data[$i]['kode']."'>
          <img src='ikonz/Edt16.png' />
        </a>
        <a class='btn btn-danger' onClick = delco('".$_GET['obj']."','".$data[$i]['kode']."')>
          <img src='ikonz/Del16.png' />
        </a>
      </td>
    </tr>";
    $i++;
  }

}

// data karyawan
if($_GET['obj'] == 'karyawan'){
  $data = $posa->picksome("*","karyawan","nama LIKE '%".$_GET['nama']."%'");
  while( $i < COUNT($data) ){
    echo "<tr>";
    foreach($data[$i] AS $k=>$v){
      echo "<td>". $v ."</td>";
    }
    echo "
      <td>
        <a class='btn btn-primary' href='./?show=form&obj=".$_GET['obj']."&mod=u&id=".$data[$i]['kode']."'>
          <img src='ikonz/Edt16.png' />
        </a>
        <a class='btn btn-danger' onClick = delco('".$_GET['obj']."','".$data[$i]['kode']."')>
          <img src='ikonz/Del16.png' />
        </a>
      </td>
    </tr>";
    $i++;
  }
}

// data dokter
if($_GET['obj'] == 'dokter'){
  $data = $posa->picksome("*","dokter","nama LIKE '%".$_GET['nama']."%'");
  while( $i < COUNT($data) ){
    echo "<tr>";
    foreach($data[$i] AS $k=>$v){
      echo "<td>". $v ."</td>";
    }
    echo "
      <td>
        <a class='btn btn-primary' href='./?show=form&obj=".$_GET['obj']."&mod=u&id=".$data[$i]['kode']."'>
          <img src='ikonz/Edt16.png' />
        </a>
        <a class='btn btn-danger' onClick = delco('".$_GET['obj']."','".$data[$i]['kode']."')>
          <img src='ikonz/Del16.png' />
        </a>
      </td>
    </tr>";
    $i++;
  }
}

// data pasien
if($_GET['obj'] == 'pasien'){
  $data = $posa->picksome("*","pasien","nama LIKE '%".$_GET['nama']."%'");
  while( $i < COUNT($data) ){
    echo "<tr>";
    foreach($data[$i] AS $k=>$v){
      echo "<td>". $v ."</td>";
    }
    echo "
      <td>
        <a class='btn btn-primary' href='./?show=form&obj=".$_GET['obj']."&mod=u&id=".$data[$i]['id']."'>
          <img src='ikonz/Edt16.png' />
        </a>
        <a class='btn btn-danger' onClick = delco('".$_GET['obj']."','".$data[$i]['id']."')>
          <img src='ikonz/Del16.png' />
        </a>
      </td>
    </tr>";
    $i++;
  }
}
?>

</table>
