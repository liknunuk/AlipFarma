<?php
  require_once("../lib/class.crud.inc.php");
  $do = new dbcrud();
  $tanggal = makeymd($_POST['tanggal']);
  $jatempo = makeymd($_POST['jatempo']);
  if($_GET['modus']=="baru"){

    if($_POST['target']=='fbData'){
      $sets = "nomorFaktur,namaPBF,tanggalFaktur,tanggalJatuhTempo,totalDiskon";
      $data = array($_POST['nomor'],$_POST['namapbf'],$tanggal,$jatempo,
      $_POST['diskon']);
      $do->insert("fabelData",$sets,$data);
      echo "Berhasil menyimpan";
    }

    if($_POST['target']=='fbList'){
      //print_r($_POST);

      $sets = "nomorFaktur,kodeObat,kuantitas,harga_beli,ppn10,disc,isiPerBox,satuanJual";
      $data = array($_POST['faktur'],$_POST['kdObat'],$_POST['banyak'],
              $_POST['regane'],$_POST['pajake'],$_POST['diskon'],
              $_POST['perbox'],$_POST['sajual']);
      $do->insert("fabelList",$sets,$data);
      echo "Data obat ditambahkan";
    }
  }

  if($_GET['modus']=="ubah"){
    if($_POST['target']=='fbData'){
      $sets = "namaPBF,tanggalFaktur,tanggalJatuhTempo,totalDiskon";
      $data = array($_POST['namapbf'],$tanggal,$jatempo,$_POST['diskon'],
      $_POST['nomor']);
      $do->update("fabelData",$sets,$data,"nomorFaktur");
      echo "Berhasil Mengupdate";
    }
    
    if($_POST['target']=='fbList'){
      print_r($_POST);
      $sets = "kodeObat,kuantitas,harga_beli,ppn10,disc,isiPerBox,satuanJual";
      $data = array($_POST['kdObat'],$_POST['banyak'],$_POST['regane'],
      $_POST['pajake'],$_POST['diskon'],$_POST['isibox'],$_POST['sajual'],$_POST['itmidx']);
       $do->update("fabelList",$sets,$data,"itemIndex");
      echo "Data obat diupdate";

    }
  }

  function makeymd($tgl){
    list($d,$m,$y) = explode("/",$tgl);
    return("$y-$m-$d");
  }
?>
