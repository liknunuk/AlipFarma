<table class='table table-striped table-sm' border='1'>
  <thead class="thead-dark">
    <tr>
      <th width="50" rowspan="2" style="vertical-align:middle;">No.</th>
      <th rowspan="2" style="vertical-align:middle;">Nama Obat</th>
      <th rowspan="2" style="vertical-align:middle;">Harga</th>
      <th rowspan="2" style="vertical-align:middle;">PPn 10%</th>
      <th width="100" rowspan="2" style="vertical-align:middle;">Banyaknya</th>
      <th width="100" rowspan="2" style="vertical-align:middle;">Kemasan</th>
      <th width="100" rowspan="2" style="vertical-align:middle;">Diskon</th>
      <th rowspan="2" style="vertical-align:middle;">Jumlah Harga</th>
      <th colspan="4">Harga Jual</th>
  </tr>
  <tr>
      <th>Nett</th>
      <th>Resep</th>
      <th>HV</th>
      <th>Ket</th>
    </tr>
  </thead>
  <tbody>

<?php

  require_once("../lib/class.crud.inc.php");
  $fpb = new dbcrud();
  $jnh = $fpb->select("resep,non_resep","optJenisHarga","resep",0);
  $sql = "SELECT obat.nomor, obat.nama, obat.harga_beli as jualan, fabelList.*
          FROM fabelList, obat
          WHERE   fabelList.nomorFaktur = '".$_GET['nf']."' &&
                  obat.kode = fabelList.kodeObat ";
  $qry = $fpb->transact($sql);
  $nu = 1;
  $tohar = 0;
  while($df = $qry->fetch()){

    if($df['ppn10'] == 1){
      $hargaNet = 10/11 * $df['harga_beli'];
      $pajakppn = 1/11 * $df['harga_beli'];
    }else{
      $hargaNet = 1 * $df['harga_beli'];
      $pajakppn = 1/10 * $df['harga_beli'];
    }
    $jmHarga = ((100 - $df['disc']) / 100) * ($hargaNet + $pajakppn) * $df['kuantitas'];
    $jualNett = ceil( $jmHarga / ( $df['isiPerBox'] * $df['kuantitas']) );
    $jualResep = ceil( $jualNett * (100 + $jnh[0]['resep'])/100 );
    $jualBebas = ceil( $jualNett * (100 + $jnh[0]['non_resep'])/100 );
    if($jualNett == $df['jualan']){
      $tanda = "[=]";
      $ket = "Sama";
    }elseif ($jualNett > $df['jualan']) {
      $tanda = "[+]";
      $ket = "Naik";
    }else{
      $tanda = "[-]";
      $ket = "Turun";
    }

    echo "
    <tr>
      <td class='ra'>".$nu.".</td>
      <td>
      [".sprintf('%04d',$df['nomor'])."]
      ";
      if($_GET['em']==1){
        echo
        "<a href='javascript:void(0)'
          onClick=editFabelList('".$df['itemIndex']."','".$_GET['nf']."')>
          ".$df['nama']."
          </a>";
        }else{
          echo $df['nama']."</a>";
        }
    echo "
      </td>
      <td class='ra'>".number_format($hargaNet,0,',','.')."</td>
      <td class='ra'>".number_format($pajakppn,0,',','.')."</td>
      <td class='ra'>".$df['kuantitas']."</td>
      <td class='ra'>".$df['isiPerBox']." ".$df['satuanJual']."</td>
      <td class='ra'>".$df['disc']."</td>
      <td class='ra'>".number_format($jmHarga,0,',','.')."</td>
      <td class='ra'>".number_format($jualNett,1,',','.')."</td>
      <td class='ra'>".number_format($jualResep,1,',','.')."</td>
      <td class='ra'>".number_format($jualBebas,1,',','.')."</td>
      <td><a href='#'
      onclick=updateHarga('".$jualNett."','".$df['isiPerBox']."','".$jualResep."','".$jualBebas."','".$df['nomor']."','".$_GET['nf']."')>
      ".$ket."</td>
    </tr>
    ";
    $nu++;
    $tohar+=$jmHarga;
  }
  $topot = $fpb->pickone("totalDiskon","fabelData","nomorFaktur",$_GET['nf']);
  $tobay = $tohar - $topot['totalDiskon'];
  echo "
    <tr style='border-top:2px solid #000;'>
      <td class='ra' colspan='7'>Jumlah Keseluruhan</td>
      <td class='ra'>".number_format($tohar,0,',','.')."</td>
      <td colspan='4'>&nbsp;</td>
    </tr>
    <tr>
      <td class='ra' colspan='7'>Total Potongan</td>
      <td class='ra'>".number_format($topot['totalDiskon'],0,',','.')."</td>
      <td colspan='4'>&nbsp;</td>
    </tr>
    <tr>
      <td class='ra' colspan='7'>Total Tagihan</td>
      <td class='ra'>".number_format($tobay,0,',','.')."</td>
      <td colspan='4'>&nbsp;</td>
    </tr>
  ";
 ?>

  </tbody>
</table>
<script>
/*
  function editFabelList(idx,nf){
    $.post('ajax/obatBatal.php',{ itemIndex: idx },function(response){
      console.log(response)
    });
    showFaktur(nf);
  }
*/
  function updateHarga(net,ibox,resep,bebas,nomor,nf){

    $.post('actions.php?obj=gaharo&mod=u',{
      'harga_beli'  : net,
      'isiPerBox'   : ibox,
      'harga_resep' : resep,
      'harga_hv'    : bebas,
      'nomor'       : nomor
    },function(response){
      //console.log(response);
    });
    showFaktur(nf,0)
  }
</script>
