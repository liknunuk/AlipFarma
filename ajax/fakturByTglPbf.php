<?php
if($_GET['mod'] == "cetak"){
  $filename ="laporanFaktur.xls";
  $contents = "testdata1 \t testdata2 \t testdata3 \t \n";
  header('Content-type: application/ms-excel');
  header('Content-Disposition: attachment; filename='.$filename);
  $tbClass = "table-print";
}else{
  $tbClass = "table-sm";
}
require_once('../lib/class.crud.inc.php');
$posa = new dbcrud();
//print_r($_POST);
$pbf = $_POST['pbf'];
$tg0 = $_POST['tg0'];
$tg1 = $_POST['tg1'];
$nuf = 1;
 // cari nomor faktur
  $sql = "SELECT nomorFaktur, namaPBF, tanggalFaktur , tanggalJatuhTempo, totalDiskon
          FROM fabelData
          WHERE namaPBF LIKE '%".$pbf."' AND
                tanggalFaktur BETWEEN '".$tg0."' AND '".$tg1."'";
  $qry = $posa->transact($sql);
  while($res = $qry->fetch()){
    /*
    echo "
      <table class='table ".$tbClass."'>
       <tbody>
        <tr><td width='200'>Nomor Faktur</td><td>".$res['nomorFaktur']."</td></tr>
        <tr><td>Nama PBF</td><td>".$res['namaPBF']."</td></tr>
        <tr><td>Tanggal Faktur</td><td>".$posa->tanggalTerbaca($res['tanggalFaktur'])."</td></tr>
        <tr><td>Tanggal Jatuh Tempo</td><td>".$posa->tanggalTerbaca($res['tanggalJatuhTempo'])."</td></tr>
       </tbody>
      </table>
    ";
    */
    $dafo = $posa->picksome("obat.nama, fabelList.harga_beli, fabelList.ppn10,
            fabelList.kuantitas, fabelList.disc","obat, fabelList","obat.kode = fabelList.kodeObat
            && fabelList.nomorFaktur ='".$res['nomorFaktur']."'");
    echo "
      <table class='table table-bordered ".$tbClass."'>
      <tbody>
        <tr>
          <th width='50px'>Nomor</th>
          <th>Tgl Faktur</th>
          <th>Tg Jatuh Tempo</th>
          <th>Nama PBF</th>
          <th>Nomor Faktur</th>
          <th>Nama Obat</th>
          <th width='75px'>Banyak</th>
          <th width='100px'>Harga</th>
          <th width='100px'>PPn</th>
          <th width='50px'>Disc</th>
          <th width='125px'>Jumlah Harga</th>
        </tr>
    ";
    $tobel = 0;
    for($i = 0 ; $i < COUNT($dafo) ; $i++ ){
      $nm = $i + 1;
      if($dafo[$i]['ppn10'] == 1){
        $ho = 10/11 * $dafo[$i]['harga_beli'];
        $tx = 1/11 * $dafo[$i]['harga_beli'];
      }else{
        $ho = $dafo[$i]['harga_beli'];
        $tx = 0.1 * $dafo[$i]['harga_beli'];
      }

      if($i > 0){
        $style = "style='visibility:hidden;'";
      }else{
        $style = "style='visibility:visible;'";
      }

      $hn = $ho + $tx;
      $dc = ($dafo[$i]['disc']) / 100 * $hn;
      $th = ($hn - $dc) * $dafo[$i]['kuantitas'];
      $tobel+=$th;
      echo "
        <tr>
          <td><span ".$style." >".$nuf."</span></td>
          <td><span ".$style." >".$posa->tanggalTerbaca($res['tanggalFaktur'])."</span></td>
          <td><span ".$style." >".$posa->tanggalTerbaca($res['tanggalJatuhTempo'])."</span></td>
          <td><span ".$style." >".$res['namaPBF']."</span></td>
          <td><span ".$style." >".$res['nomorFaktur']."</td>
          <td>".$dafo[$i]['nama']."</td>
          <td align='right'>".$dafo[$i]['kuantitas']." Box</td>
          <td align='right'>".number_format($ho,0,',','.')."</td>
          <td align='right'>".number_format($tx,0,',','.')."</td>
          <td align='right'>".number_format($dc,0,',','.')."</td>
          <td align='right'>".number_format($th,0,',','.')."</td>
        </tr>
      ";
    }
    $tobay = $tobel-$res['totalDiskon'];
    echo "
          <tr>
            <td class='ra' colspan='10'>Jumlah Total</td>
            <td align='right'>".number_format($tobel,0,',','.')."</td>
          </tr>
          <tr>
            <td class='ra' colspan='10'>Total Diskon</td>
            <td align='right'>".number_format($res['totalDiskon'],0,',','.')."</td>
          </tr>
          <tr>
            <td class='ra' colspan='10'>Total Tagihan</td>
            <td align='right'>".number_format($tobay,0,',','.')."</td>
          </tr>
    ";
    echo "
        </tbody>
      </table>";
    $nuf++;
  }
?>
