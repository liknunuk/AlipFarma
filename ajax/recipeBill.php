<?php
  require_once("../lib/class.crud.inc.php");
  $rcp = new dbcrud();
  $emb = $rcp->select("*","optEmbalase,optJenisHarga","e_r",0);
  $el = $emb[0]['e_l'];
  $er = $emb[0]['e_r'];
  $ep = $emb[0]['e_p'];
  $embalas = array('e_l'=>$el,'e_p'=>$ep,'e_r'=>$er);
  $bill = $rcp->picksome("recipeOut.*,obat.nama","recipeOut, obat",
          "trxId='".$_GET['id']."' && obat.kode = recipeOut.kodeObat ");
  $i=0;
  $billing = array();
  $totalBill=0;
  while($i < COUNT($bill)){
    $jumlah = (100 - $bill[$i]['diskon'])/100 * $bill[$i]['harga_resep'] * $bill[$i]['banyaknya'];
    $tagihan = array('idxdata'=>$bill[$i]['idxdata'],
                     'nama'=>$bill[$i]['nama'],
                     'banyaknya'=>$bill[$i]['banyaknya'],
                     'harga'=>number_format($bill[$i]['harga_resep'],0,',','.'),
                     'diskon'=>$bill[$i]['diskon'],
                     'jumlah'=>number_format($jumlah,0,',','.'),
                   );
    $totalBill += $jumlah;
    array_push($billing,$tagihan);
    $i++;
  }

  $billData = array('rincian'=>$billing,'totalTagihan'=>$totalBill,'fak_l'=>$el,'fak_r'=>$er,'fak_p'=>$ep);
  echo json_encode($billData);
?>
