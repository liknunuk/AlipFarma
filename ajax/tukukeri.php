<?php
  require_once('../lib/class.crud.inc.php');
  $posa = new dbcrud();

  $kodeObat = $_GET['id'];
  $sql = "SELECT harga_beli, ppn10, isiPerBox, resep, non_resep
          FROM fabelList , optJenisHarga
          WHERE kodeObat = '".$kodeObat."'
          ORDER BY itemIndex DESC
          limit 1";
  $qry = $posa->transact($sql);
  $res = $qry->fetch();
  if(!$res){
    $tukonkeri = array(
      'hargaNett'=>0,
      'hargaResep'=>0,
      'hargaBebas'=>0,
      'isiPerBox'=>0
    );
  }else{

    if($res['ppn10']== '1' ){
      $harga_beli = $res['harga_beli'];
      $hargaNet = $res['harga_beli'] / $res['isiPerBox'];
    }else{
      $harga_beli = $res['harga_beli'] * 1.1;
      $hargaNet = 1.1 * $res['harga_beli'] / $res['isiPerBox'];
    }
    $hargaRsp = (100 + $res['resep'])/100 * $hargaNet;
    $hargaBbs = (100 + $res['non_resep'])/100 * $hargaNet;

    $tukonkeri = array(
      'hargaNett'=>ceil($hargaNet),
      'hargaResep'=>ceil($hargaRsp),
      'hargaBebas'=>ceil($hargaBbs),
      'isiPerBox'=>$res['isiPerBox'],
      'harga_beli'=>$harga_beli
    );

  }


  echo json_encode($tukonkeri);

?>
