<?php
/*
  echo "<pre>";
  print_r($_POST);
  echo "</pre>";
*/
  require_once("./lib/class.crud.inc.php");
  $rcp = new dbcrud();

  if(empty($_POST['kodeObat'])){ $_POST['kodeObat'] = "000" ;}

  if($_POST['mode'] == 'ins'){
    $sets = 'trxId,kodeObat,namaObat,banyaknya';
    $data = array($_POST['trxId'],$_POST['kodeObat'],$_POST['namaObat'],
            $_POST['banyaknya']);
    $rcp->insert("recipeIn",$sets,$data);
    echo '
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Berhasil!</strong> Obat resep tersimpan ..
    </div>
    ';
  }

  if($_POST['mode'] == 'rmv'){
    $rcp->delete("recipeIn","idxdata",$_POST['id']);
    echo '
    <div class="alert alert-warning alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Berhasil!</strong> Obat resep terhapus ..
    </div>
    ';
  }
?>
