<?php
/*
  echo "<pre>";
  print_r($_POST);
  echo "</pre>";
*/
  require_once("./lib/class.crud.inc.php");
  $rcp = new dbcrud();
  $rep = $rcp->select("*","optEmbalase","e_r","0");

  if($_POST['mode'] == 'ins'){
    $sets = 'trxid,tanggal,shift,idPasien,idDokter,faktor_r, faktor_e, faktor_p';
    $data = array($_POST['trxId'],$_POST['tanggal'],$_POST['shift'],
            $_POST['idPasien'],$_POST['idDokter'],$rep[0]['e_r'],$rep[0]['e_l'],$rep[0]['e_p']);
    $rcp->insert("recipeRqst",$sets,$data);
    echo '
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Berhasil!</strong> Permintaan resep tersimpan ..
    </div>
    ';
  }

  if($_POST['mode'] == 'chg'){
    $sets = "faktor_r,faktor_e,faktor_p,konsultasi,statusBayar";
    $data = array($_POST['fakr'],$_POST['fake'],$_POST['fakp'],$_POST['cnsl'],
            $_POST['stby'],$_POST['trxId']);
    $rcp->update('recipeRqst',$sets,$data,'trxId');
  }
?>
